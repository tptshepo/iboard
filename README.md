# iBoard #

iBoard delivers board packs and documents to iPads, providing a uniquely simple app for reading and annotating documents, approving board resolutions using secure electronic signature on iPads and allows users to vote on items in real time.

## Technologies used ##

* Xcode
* Objective-c