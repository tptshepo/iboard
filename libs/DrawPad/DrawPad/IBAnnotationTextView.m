//
//  IBAnnotationTextView.m
//  DrawPad
//
//  Created by Tshepo Mgaga on 2015/06/14.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import "IBAnnotationTextView.h"


#define OFFSET_X 5
#define OFFSET_Y 30


@implementation IBAnnotationTextView
{
    NSString *guid;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        guid = [[NSUUID UUID] UUIDString];
        
        [self viewDidLoad];
        
    }
    return self;
}


- (void)viewDidLoad
{
    
    UIImageView *background = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon10"]];
    background.frame = CGRectMake(- OFFSET_X, - OFFSET_Y, 38,32);
        //background.frame = CGRectMake(0, 0, 38,32);
    [self addSubview:background];

}




-(BOOL)hitTest:(CGPoint)point
{
    CGRect frame =  CGRectMake(self.frame.origin.x - OFFSET_X, self.frame.origin.y - OFFSET_Y, 38,32);
    return CGRectContainsPoint(frame, point);
}

-(NSString *)description
{
    return guid;
}


@end
