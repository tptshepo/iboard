//
//  IBDrawingSurfaceView.h
//  DrawPad
//
//  Created by Tshepo Mgaga on 2015/06/16.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IBDrawingSurfaceView : UIView

@end
