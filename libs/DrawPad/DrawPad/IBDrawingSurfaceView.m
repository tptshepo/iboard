//
//  IBDrawingSurfaceView.m
//  DrawPad
//
//  Created by Tshepo Mgaga on 2015/06/16.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import "IBDrawingSurfaceView.h"

#import "IBAnnotationTextView.h"


typedef enum {
    AnnotationTypeNone,
    AnnotationTypePen,
    AnnotationTypeHighlighter,
    AnnotationTypeEraser,
    AnnotationTypeText
} AnnotationType;

@implementation IBDrawingSurfaceView
{
    CGPoint lastPoint;
    CGFloat red;
    CGFloat green;
    CGFloat blue;
    CGFloat brush;
    CGFloat opacity;
    BOOL mouseSwiped;
    
    AnnotationType drawMode;
    
    IBAnnotationTextView * lastText;
    CGPoint textLastPoint;
    
    NSMutableArray *textViews;
    
    UIImageView *tempDrawImage;
    UIImageView *mainImage;
    
}


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self viewDidLoad];
        
    }
    return self;
}


- (void)viewDidLoad {
    
    [self setDrawingSettings:AnnotationTypeText];
    
    self.userInteractionEnabled = NO;
    
    textViews = [[NSMutableArray alloc] init];
    
    
    tempDrawImage = [[UIImageView alloc] initWithFrame:self.frame];
    [self addSubview:tempDrawImage];
    
    mainImage = [[UIImageView alloc] initWithFrame:self.frame];
    [self addSubview:mainImage];
    
}

-(void)setDrawingSettings:(AnnotationType)annotationType
{
    drawMode = annotationType;
    
    switch(annotationType)
    {
        case AnnotationTypeNone:
            break;
        case AnnotationTypePen:
            red = 0.0/255.0;
            green = 0.0/255.0;
            blue = 0.0/255.0;
            brush = 3.0;
            opacity = 1.0;
            break;
        case AnnotationTypeHighlighter:
            red = 0.0/255.0;
            green = 0.0/255.0;
            blue = 0.0/255.0;
            brush = 10.0;
            opacity = 0.5;
            break;
        case AnnotationTypeText:
            red = 0.0/255.0;
            green = 0.0/255.0;
            blue = 0.0/255.0;
            brush = 10.0;
            opacity = 1.0;
            break;
        case AnnotationTypeEraser:
            red = 255.0/255.0;
            green = 255.0/255.0;
            blue = 255.0/255.0;
            opacity = 0.0;
            brush = 10.0;
            break;
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [touches anyObject];
    lastPoint = [touch locationInView:self];
    
    if (drawMode == AnnotationTypeText){
        
        
        for (IBAnnotationTextView *textView in textViews)
        {
            if ([textView hitTest:lastPoint])
            {
                NSLog(@"[HITTEST] %@", textView);
                return;
            }
        }
        
        
        if (lastText != nil){
            
            
            
            //CGRect lastRect =  CGRectMake(textLastPoint.x - 5, textLastPoint.y - 30, 38,32);
            
            //            if ([lastText hitTest:lastPoint]){
            //                NSLog(@"show text");
            //
            //                return;
            //            }
            //
            // check for editing mode
            //            BOOL isActive = [lastText isFirstResponder];
            //            if (isActive){
            //                // hide border
            //                [[lastText layer] setBorderWidth:0.0    ];
            //
            //                // hide keyboard
            //                [[self view] endEditing: YES];
            //                return;
            //            }
            
            
        }
        
        // add new textbox
        //        UITextView * text = [[UITextView alloc]initWithFrame:CGRectMake(lastPoint.x - 5,lastPoint.y - 5,100,30)];
        //        lastText = text;
        //        [text setBackgroundColor:[UIColor clearColor]];
        //        [[text layer] setBorderColor:[[UIColor grayColor] CGColor]];
        //        [[text layer] setBorderWidth:0.5];
        //        [[text layer] setCornerRadius:5];
        //        [self.view addSubview:text];
        //        [text becomeFirstResponder];
        
        IBAnnotationTextView * text = [[IBAnnotationTextView alloc]initWithFrame:CGRectMake(lastPoint.x, lastPoint.y, 38,32)];
        lastText = text;
        [self addSubview:text];
        [textViews addObject:text];
        
        NSLog(@"[ADD] %@", text);
        
    }
}

- (void)drawPen:(CGPoint)currentPoint {
    UIGraphicsBeginImageContext(self.frame.size);
    [tempDrawImage.image drawInRect:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    CGContextMoveToPoint(UIGraphicsGetCurrentContext(), lastPoint.x, lastPoint.y);
    CGContextAddLineToPoint(UIGraphicsGetCurrentContext(), currentPoint.x, currentPoint.y);
    CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
    CGContextSetLineWidth(UIGraphicsGetCurrentContext(), brush );
    CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), red, green, blue, 1.0);
    CGContextSetBlendMode(UIGraphicsGetCurrentContext(),kCGBlendModeNormal);
    
    CGContextStrokePath(UIGraphicsGetCurrentContext());
    tempDrawImage.image = UIGraphicsGetImageFromCurrentImageContext();
    [tempDrawImage setAlpha:opacity];
    UIGraphicsEndImageContext();
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [touches anyObject];
    CGPoint currentPoint = [touch locationInView:self];
    
    if (drawMode == AnnotationTypePen || drawMode == AnnotationTypeHighlighter || drawMode == AnnotationTypeEraser)
    {
        [self drawPen:currentPoint];
    }
    
    lastPoint = currentPoint;
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UIGraphicsBeginImageContext(mainImage.frame.size);
    [mainImage.image drawInRect:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)
                      blendMode:kCGBlendModeNormal
                          alpha:1.0];
    
    [tempDrawImage.image drawInRect:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)
                          blendMode:kCGBlendModeNormal
                              alpha:opacity];
    
    mainImage.image = UIGraphicsGetImageFromCurrentImageContext();
    tempDrawImage.image = nil;
    UIGraphicsEndImageContext();
}




-(UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    UIView* view = [super hitTest:point withEvent:event];
    return view;
}


@end
