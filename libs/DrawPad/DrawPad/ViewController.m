//
//  ViewController.m
//  DrawPad
//
//  Created by Tshepo Mgaga on 2015/06/14.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import "ViewController.h"
#import "IBDrawingSurfaceView.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    IBDrawingSurfaceView *drawView = [[IBDrawingSurfaceView alloc] initWithFrame:self.view.frame];
    [self.view addSubview:drawView];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pencilPressed:(id)sender {
    

}

- (IBAction)eraserPressed:(id)sender {

    
    
}

- (IBAction)resetPressed:(id)sender {
    

    
}

- (IBAction)textPressed:(id)sender {
    

    
    
}
@end
