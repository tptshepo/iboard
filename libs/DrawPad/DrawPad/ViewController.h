//
//  ViewController.h
//  DrawPad
//
//  Created by Tshepo Mgaga on 2015/06/14.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController


- (IBAction)pencilPressed:(id)sender;

- (IBAction)eraserPressed:(id)sender;

- (IBAction)resetPressed:(id)sender;

- (IBAction)textPressed:(id)sender;

@end

