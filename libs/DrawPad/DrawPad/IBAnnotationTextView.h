//
//  IBAnnotationTextView.h
//  DrawPad
//
//  Created by Tshepo Mgaga on 2015/06/14.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IBAnnotationTextView : UIView


-(BOOL)hitTest:(CGPoint)point;


@end
