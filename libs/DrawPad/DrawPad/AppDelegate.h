//
//  AppDelegate.h
//  DrawPad
//
//  Created by Tshepo Mgaga on 2015/06/14.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

