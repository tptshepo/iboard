//
//  IBFAQViewController.m
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/07/13.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import "IBFAQViewController.h"
#import "IBDocumentViewerView.h"

@interface IBFAQViewController ()

@end

@implementation IBFAQViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    

        self.documentContainer.bounds = CGRectMake(0, 0, 768, 871);
        
        IBDocumentViewerView * documentView = [[IBDocumentViewerView alloc] initWithFrame:self.documentContainer.bounds];
        [self.documentContainer addSubview:documentView];
        [documentView loadDocument:@"faq" canCallServer:NO];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)tapBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
@end
