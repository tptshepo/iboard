//
//  IBProfile.m
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/04/11.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import "IBProfile.h"
#import "IBCommonKey.h"


@interface IBProfile ()

@property (nonatomic, copy, readwrite) NSString *firstName;
@property (nonatomic, copy, readwrite) NSString *lastName;
@property (nonatomic, copy, readwrite) NSString *userID;
@property (nonatomic, copy, readwrite) NSString *serverExpiryDate;
@property (nonatomic, copy, readwrite) NSString *imageIDThumbnail;

@end

@implementation IBProfile

- (instancetype)initWithFirstName:(NSString *)firstName
                         lastName:(NSString *)lastName
                 serverExpiryDate:(NSString *)serverExpirtDate
                           userID:(NSString *)userID
                           APIKey:(NSString *)APIKey
                 ImageIDThumbnail:(NSString *)ImageIDThumbnail;
{
    if ((self = [super init])) {
        self.firstName = firstName;
        self.lastName = lastName;
        self.serverExpiryDate = serverExpirtDate;
        self.userID = userID;
        self.APIKey = APIKey;
        self.imageIDThumbnail = ImageIDThumbnail;
    }
    
    return self;
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    return [self initWithFirstName:dict[IBProfileFirstNameKey]
                          lastName:dict[IBProfileLastNameKey]
                  serverExpiryDate:dict[IBProfileServerExpiryDateKey]
                            userID:dict[IBProfileUserIDKey]
                            APIKey:dict[IBProfileAPIKeyKey]
                  ImageIDThumbnail:dict[IBImageIDThumbnailKey]
            ];
}

- (NSDictionary *)dictionaryRepresentation
{
    return @{
             IBProfileUserIDKey: self.userID,
             IBProfileFirstNameKey: self.firstName,
             IBProfileLastNameKey: self.lastName,
             IBProfileServerExpiryDateKey: self.serverExpiryDate,
             IBImageIDThumbnailKey: self.imageIDThumbnail
             };
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"<%@: 0x%x UserID=%@ FirstName=%@ LastName=%@ ServerExpiryDate=%@ ImageIDThumbnail=%@>",
            NSStringFromClass([self class]),
            (unsigned int)self,
            self.userID,
            self.firstName,
            self.lastName,
            self.serverExpiryDate,
            self.imageIDThumbnail];
}

@end
