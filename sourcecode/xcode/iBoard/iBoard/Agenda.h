//
//  Agenda.h
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/06/28.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Document, Meeting;

@interface Agenda : NSManagedObject

@property (nonatomic, retain) NSNumber * meetingAgendaItemID;
@property (nonatomic, retain) NSString * itemNumber;
@property (nonatomic, retain) NSString * itemName;
@property (nonatomic, retain) NSNumber * itemOrder;
@property (nonatomic, retain) NSNumber * isDeletedState;
@property (nonatomic, retain) Meeting *meeting;
@property (nonatomic, retain) NSSet *documents;
@end

@interface Agenda (CoreDataGeneratedAccessors)

- (void)addDocumentsObject:(Document *)value;
- (void)removeDocumentsObject:(Document *)value;
- (void)addDocuments:(NSSet *)values;
- (void)removeDocuments:(NSSet *)values;

@end
