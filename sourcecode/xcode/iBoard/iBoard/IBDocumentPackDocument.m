//
//  IBDocumentPackDocument.m
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/04/28.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import "IBDocumentPackDocument.h"
#import "IBCommonKey.h"


@interface IBDocumentPackDocument ()


@property (nonatomic, copy, readwrite) NSString *documentPackDocumentID;
@property (nonatomic, copy, readwrite) NSString *documentID;
@property (nonatomic, copy, readwrite) NSString *documentOrder;
@property (nonatomic, copy, readwrite) NSString *documentName;
@property (nonatomic, copy, readwrite) NSString *canEmail;
@property (nonatomic, copy, readwrite) NSString *canPrint;
@property (nonatomic, copy, readwrite) NSString *fileName;
@property (nonatomic, copy, readwrite) NSString *isDeleted;


@end


@implementation IBDocumentPackDocument

@synthesize coverStyleID = _coverStyleID;

-(instancetype)initWithDocumentPackDocumentID:(NSString *)documentPackDocumentID
                                   documentID:(NSString *)documentID
                                documentOrder:(NSString *)documentOrder
                                 documentName:(NSString *)documentName
                                     canEmail:(NSString *)canEmail
                                     canPrint:(NSString *)canPrint
                                     fileName:(NSString *)fileName
                                    isDeleted:(NSString *)isDeleted
{
    
    
    if ((self = [super init])) {
        self.documentPackDocumentID = documentPackDocumentID;
        self.documentID = documentID;
        self.documentOrder = documentOrder;
        self.documentName = documentName;
        self.canEmail = canEmail;
        self.canPrint = canPrint;
        self.fileName = fileName;
        self.isDeleted = isDeleted;
    }
    
    return self;
    
    
}


- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    return [self initWithDocumentPackDocumentID:dict[IBDocumentPackDocumentIDKey]
                                     documentID:dict[IBDocumentIDKey]
                                  documentOrder:dict[IBDocumentOrderKey]
                                   documentName:dict[IBDocumentNameKey]
                                       canEmail:dict[IBCanEmailKey]
                                       canPrint:dict[IBCanPrintKey]
                                       fileName:dict[IBFileNameKey]
                                      isDeleted:dict[IBIsDeletedKey]];
}

- (NSDictionary *)dictionaryRepresentation
{
    return @{
             IBDocumentPackDocumentIDKey: self.documentPackDocumentID,
             IBDocumentIDKey: self.documentID,
             IBDocumentOrderKey: self.documentOrder,
             IBDocumentNameKey: self.documentName,
             IBCanEmailKey: self.canEmail,
             IBCanPrintKey: self.canPrint,
             IBFileNameKey: self.fileName,
             IBIsDeletedKey: self.isDeleted
             };
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"<%@: 0x%x \
            DocumentPackDocumentID=%@ \
            DocumentID=%@ \
            DocumentOrder=%@ \
            DocumentName=%@ \
            CanEmail=%@ \
            CanPrint=%@ \
            FileName=%@ \
            IsDeleted=%@>\
            ",
            NSStringFromClass([self class]),
            (unsigned int)self,
            self.documentPackDocumentID,
            self.documentID,
            self.documentOrder,
            self.documentName,
            self.canEmail,
            self.canPrint,
            self.fileName,
            self.isDeleted
            ];
}


@end
