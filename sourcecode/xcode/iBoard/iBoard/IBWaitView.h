//
//  IBWaitView.h
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/04/11.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//


#import <UIKit/UIKit.h>

/**
 * A HUD-like modal wait spinner for blocking UI operations
 */
@interface IBWaitView : UIView

/**
 * Display the wait view in center of the parent view with the
 * given status text
 */
- (void)showWithText:(NSString *)text;

/**
 * Hide the wait view
 */
- (void)hide;

@end
