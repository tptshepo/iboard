//
//  IBDocumentListViewControlerViewController.h
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/05/10.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IBDocumentPack.h"
#import "IBDocumentListView.h"

#import "Company.h"

@interface IBDocumentListViewControler : UIViewController <IBDocumentListViewDelegate>

@property (nonatomic) Company *company;
@property (nonatomic) DocumentPack *documentPack;
@property (weak, nonatomic) IBOutlet UILabel *viewTitle;
@property (weak, nonatomic) IBOutlet UIView *viewDocumentList;

- (IBAction)backToDocumentList:(id)sender;


@end
