//
//  Utils.h
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/06/27.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utils : NSObject

+ (NSNumber*)stringToNSnumberIntFromDictionary:(NSString*)string;
+ (NSNumber*)stringToNSNumberBoolFromDictionary:(NSString*)string;

@end
