//
//  IBMeetingAgendaViewController.h
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/05/30.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Meeting.h"


@interface IBMeetingAgendaViewController : UIViewController

@property (nonatomic) Meeting *companyMeeting;

@property (weak, nonatomic) IBOutlet UIView *documentContainer;

@property (weak, nonatomic) IBOutlet UILabel *titleView;

@property (weak, nonatomic) IBOutlet UIView *waitContainer;

@property (weak, nonatomic) IBOutlet UIView *agendaViewContainer;

- (IBAction)tapAgenda:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *infoButton;

- (IBAction)backToMeetingList:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *documentTitle;

@property (weak, nonatomic) IBOutlet UIView *settingsPanelContainer;

- (IBAction)tapSettings:(id)sender;
- (IBAction)tapHighlighter:(id)sender;
- (IBAction)tapPen:(id)sender;
- (IBAction)tapComment:(id)sender;


@property (weak, nonatomic) IBOutlet UIButton *settingsButton;

@property (weak, nonatomic) IBOutlet UIButton *commentButton;

@property (weak, nonatomic) IBOutlet UIButton *penButton;

@property (weak, nonatomic) IBOutlet UIButton *highlighterButton;

@end



