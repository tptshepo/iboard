//
//  IBAnnotationTextView.m
//  DrawPad
//
//  Created by Tshepo Mgaga on 2015/06/14.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import "IBAnnotationCommentView.h"


#define COMMENT_OFFSET_X 5
#define COMMENT_OFFSET_Y 30

#define COMMENT_WIDTH 182
#define COMMENT_HEIGHT 134



@implementation IBAnnotationCommentView
{
    NSString *guid;
    UITextView * commentText;
    CGRect parentRect;
    UIButton *bin;
}

@synthesize delegate = _delegate;
@synthesize isDeleted = _isDeleted;

- (instancetype)initWithFrame:(CGRect)frame restoreMode:(BOOL)restoreMode pRect:(CGRect)pRect
{
    self = [super initWithFrame:frame];
    if (self) {
        
        parentRect = pRect;
        
        guid = [[NSUUID UUID] UUIDString];
        
        [self viewDidLoad:restoreMode];
        
    }
    return self;
}


- (void)viewDidLoad:(BOOL)restoreMode
{
    _isDeleted = NO;
    
    UIImageView *background = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon10"]];
    background.frame = CGRectMake(- COMMENT_OFFSET_X, - COMMENT_OFFSET_Y, 38,32);
    [self addSubview:background];
    
    commentText = [[UITextView alloc]initWithFrame:CGRectMake(0,0,0,0)];
    
    [self commentTextRect];
    
    [commentText setBackgroundColor:[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.9]];
    [[commentText layer] setBorderColor:[[UIColor grayColor] CGColor]];
    [[commentText layer] setBorderWidth:1.0];
    [[commentText layer] setCornerRadius:5];
    [self addSubview:commentText];
    
    
    bin = [[UIButton alloc] init];
    bin.frame = CGRectMake(commentText.frame.origin.x + commentText.frame.size.width - 10,
                           commentText.frame.origin.y + commentText.frame.size.height - 10, 18,24);
    [bin setBackgroundImage:[UIImage imageNamed:@"bin"] forState:normal];
    [self addSubview:bin];
    
    if (restoreMode)
    {
        [bin setHidden:YES];
        [commentText setHidden:YES];
    }
    else
    {
        [commentText becomeFirstResponder];
    }
    
}

- (void)commentTextRect
{
    CGRect iconFrame =  CGRectMake(self.frame.origin.x - COMMENT_OFFSET_X, self.frame.origin.y - COMMENT_OFFSET_Y, 38,32);
    
    CGFloat width = COMMENT_WIDTH;
    CGFloat height = COMMENT_HEIGHT;
    CGFloat x = - ((width * 0.5) - 10);
    CGFloat y = - 180;
    
    if (iconFrame.origin.x + width > parentRect.size.width)
    {
        x -= 30;
    }
    
    if (iconFrame.origin.y < height)
    {
        y += 200;
    }
    
    if (iconFrame.origin.x < width)
    {
        x += 40;
    }
    
    commentText.frame = CGRectMake(x, y, width, height);
}

-(void)showText
{
    
    [self commentTextRect];
    
    [bin setHidden:NO];
    // add new textbox
    [commentText setHidden:NO];
    [commentText becomeFirstResponder];
    
}

-(void)hideText
{
    [bin setHidden:YES];
    
    // add new textbox
    [commentText setHidden:YES];
    
}

-(NSString *)getText
{
    return [commentText text];
}

-(void)setText:(NSString *)text
{
    commentText.text = text;
}

-(BOOL)hitTest:(CGPoint)point
{
    CGRect frame =  CGRectMake(self.frame.origin.x - COMMENT_OFFSET_X, self.frame.origin.y - COMMENT_OFFSET_Y, 38,32);
    BOOL hitTest =  CGRectContainsPoint(frame, point);
    
    if (hitTest)
        return YES;
    
    if (!commentText.isHidden)
    {
        CGRect binFrame = CGRectMake(self.frame.origin.x + (commentText.frame.origin.x + commentText.frame.size.width - 10) ,
                                     self.frame.origin.y + (commentText.frame.origin.y + commentText.frame.size.height - 10), 18,24);
        
        
        hitTest =  CGRectContainsPoint(binFrame, point);
        
        if (hitTest) {
            // delete myself
            _isDeleted = YES;
            [_delegate deleteAnnotationCommentView:self];
            return NO;
        }
        
        
        
        CGRect textFrame = CGRectMake(self.frame.origin.x + commentText.frame.origin.x,
                                      self.frame.origin.y + commentText.frame.origin.y,
                                      commentText.frame.size.width,
                                      commentText.frame.size.height);
        
        
        hitTest =  CGRectContainsPoint(textFrame, point);
        
        if (hitTest)
            return YES;
        
    }
    
    return NO;
}

-(NSString *)description
{
    return guid;
}


@end
