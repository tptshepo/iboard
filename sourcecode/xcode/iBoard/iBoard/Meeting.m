//
//  Meeting.m
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/06/28.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import "Meeting.h"
#import "Agenda.h"
#import "Company.h"


@implementation Meeting

@dynamic companyMeetingID;
@dynamic startDateTime;
@dynamic endDateTime;
@dynamic location;
@dynamic apologies;
@dynamic showAgendaItemNumberInd;
@dynamic packType;
@dynamic documentPackID;
@dynamic packName;
@dynamic packDescription;
@dynamic coverNote;
@dynamic coverStyleID;
@dynamic allowEmail;
@dynamic allowPrint;
@dynamic isDeletedState;
@dynamic userID;
@dynamic company;
@dynamic agendas;

@end
