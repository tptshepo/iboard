//
//  IBService_NSURLSession.m
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/04/11.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import "IBService_NSURLSession.h"


#import "IBService_NSURLSessionRequest.h"
#import "IBService_SubclassMethods.h"
#import "IBProfile.h"
#import "NSArray+Enumerable.h"

static NSString * const IBAuthHeader = @"AuthToken";

@interface IBService_NSURLSession () <IBService_NSURLSessionRequestDelegate>

@property (nonatomic, strong) NSURLSession *session;
@property (nonatomic, strong) NSMutableDictionary *requests;
@property (nonatomic, strong) NSMutableArray *requestsPendingAuthentication;

@end

@implementation IBService_NSURLSession

- (id)init
{
    if ((self = [super init])) {
        self.requests = [NSMutableDictionary dictionary];
        self.requestsPendingAuthentication = [NSMutableArray array];
        
        NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
        sessionConfig.HTTPCookieStorage = NULL;
        sessionConfig.HTTPShouldSetCookies = NO;
        sessionConfig.HTTPCookieAcceptPolicy = NSHTTPCookieAcceptPolicyNever;
        sessionConfig.URLCredentialStorage = NULL;
        
        self.session = [NSURLSession sessionWithConfiguration:sessionConfig];
    }
    
    return self;
}

# pragma mark - Subclass methods

- (NSString *)submitRequestWithURL:(NSURL *)URL
                            method:(NSString *)httpMethod
                              body:(NSDictionary *)bodyDict
                    expectedStatus:(NSInteger)expectedStatus
                           success:(IBServiceSuccess)success
                           failure:(IBServiceFailure)failure
{
    NSMutableURLRequest *request = [self requestForURL:URL
                                                method:httpMethod
                                              bodyDict:bodyDict];
    
    IBService_NSURLSessionRequest *sessionRequest;
    sessionRequest = [[IBService_NSURLSessionRequest alloc] initWithRequest:request
                                                                        usingSession:self.session
                                                                      expectedStatus:expectedStatus
                                                                             success:success
                                                                             failure:failure
                                                                            delegate:self];
    
    self.requests[sessionRequest.requestIdentifier] = sessionRequest;
    return sessionRequest.requestIdentifier;
}

- (void)cancelRequestWithIdentifier:(NSString *)identifier
{
    IBService_NSURLSessionRequest *request = self.requests[identifier];
    if (request) {
        [request cancel];
        [self.requests removeObjectForKey:identifier];
    }
}

- (void)resendRequestsPendingAuthentication
{
    for (IBService_NSURLSessionRequest *request in self.requestsPendingAuthentication) {
        [request restart];
    }
}

#pragma mark - Overrides

- (NSMutableURLRequest *)requestForURL:(NSURL *)URL method:(NSString *)httpMethod bodyDict:(NSDictionary *)bodyDict
{
    //NSLog(@"%@", [NSString stringWithFormat:@"JSON: %@", bodyDict]);
    //NSLog(@"URL: %@", URL);
    
    NSMutableURLRequest *request = [super requestForURL:URL method:httpMethod bodyDict:bodyDict];
    
    if (self.currentUser.APIKey) {
        [request setValue:self.currentUser.APIKey forHTTPHeaderField:IBAuthHeader];
    }
    
    return request;
}

#pragma mark - IBService_NSURLSessionRequestDelegate

- (void)sessionRequestDidComplete:(IBService_NSURLSessionRequest *)request
{
    [self.requests removeObjectForKey:request.requestIdentifier];
    [self.requestsPendingAuthentication removeObject:request];
}

- (void)sessionRequestFailed:(IBService_NSURLSessionRequest *)request error:(NSError *)error
{
    [self.requests removeObjectForKey:request.requestIdentifier];
    [self.requestsPendingAuthentication removeObject:request];
}

- (void)sessionRequestRequiresAuthentication:(IBService_NSURLSessionRequest *)request
{
    [self.requestsPendingAuthentication addObject:request];
    [[NSNotificationCenter defaultCenter] postNotificationName:IBServiceAuthRequiredNotification object:nil];
}

@end