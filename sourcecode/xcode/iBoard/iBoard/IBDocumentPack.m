//
//  IBDocumentPack.m
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/04/28.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import "IBDocumentPack.h"
#import "IBCommonKey.h"
#import "NSArray+Enumerable.h"
#import "IBDocumentPackDocument.h"

@interface IBDocumentPack ()

@property (nonatomic, copy, readwrite) NSString *packType;
@property (nonatomic, copy, readwrite) NSArray *documentList;
@property (nonatomic, copy, readwrite) NSString *documentPackID;
@property (nonatomic, copy, readwrite) NSString *packName;
@property (nonatomic, copy, readwrite) NSString *packDescription;
@property (nonatomic, copy, readwrite) NSString *coverNote;
@property (nonatomic, copy, readwrite) NSString *coverStyleID;
@property (nonatomic, copy, readwrite) NSString *allowEmail;
@property (nonatomic, copy, readwrite) NSString *allowPrint;
@property (nonatomic, copy, readwrite) NSString *isDeleted;

@end


@implementation IBDocumentPack


-(instancetype)initWithPackType:(NSString *)packType
                   documentList:(NSArray *)documentList
                 documentPackID:(NSString *)documentPackID
                       packName:(NSString *)packName
                packDescription:(NSString *)packDescription
                      coverNote:(NSString *)coverNote
                   coverStyleID:(NSString *)coverStyleID
                     allowEmail:(NSString *)allowEmail
                     allowPrint:(NSString *)allowPrint
                      isDeleted:(NSString *)isDeleted
{
    
    
    if ((self = [super init])) {
        
        self.packType = packType;
        self.documentList = documentList;
        self.documentPackID = documentPackID;
        self.packName = packName;
        self.packDescription = packDescription;
        self.coverNote = coverNote;
        self.coverStyleID = coverStyleID;
        self.allowEmail = allowEmail;
        self.allowPrint = allowPrint;
        self.isDeleted = isDeleted;
        
    }
    
    return self;
    
    
}


- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    
    NSArray *documentListFromDict = [dict[IBDocumentListKey] mappedArrayWithBlock:^id(id obj) {
        return [[IBDocumentPackDocument alloc] initWithDictionary:obj];
    }];
    
    return [self initWithPackType:dict[IBPackTypeKey]
                     documentList:documentListFromDict
                   documentPackID:dict[IBDocumentPackIDKey]
                         packName:dict[IBPackNameKey]
                  packDescription:dict[IBPackDescriptionKey]
                        coverNote:dict[IBCoverNoteKey]
                     coverStyleID:dict[IBCoverStyleIDKey]
                       allowEmail:dict[IBAllowEmailKey]
                       allowPrint:dict[IBAllowPrintKey]
                        isDeleted:dict[IBIsDeletedKey]];
    
}

- (NSDictionary *)dictionaryRepresentation
{
    
    NSArray *documentListListFromDict = [self.documentList mappedArrayWithBlock:^id(id obj) {
        return [[IBDocumentPackDocument alloc] initWithDictionary:obj];
    }];
    
    return @{
             IBPackTypeKey: self.packType,
             IBDocumentListKey: documentListListFromDict,
             IBDocumentPackIDKey: self.documentPackID,
             IBPackNameKey: self.packName,
             IBPackDescriptionKey: self.packDescription,
             IBCoverNoteKey: self.coverNote,
             IBCoverStyleIDKey: self.coverStyleID,
             IBAllowEmailKey: self.allowEmail,
             IBAllowPrintKey: self.allowPrint,
             IBIsDeletedKey: self.isDeleted
             };
}

- (NSString *)description
{
    
    return [NSString stringWithFormat:@"<%@: 0x%x \
            PackType=%@ \
            DocumentPackID=%@ \
            PackName=%@ \
            PackDescription=%@ \
            CoverNote=%@ \
            CoverStyleID=%@ \
            AllowEmail=%@ \
            AllowPrint=%@ \
            IsDeleted=%@ \
            ",
            NSStringFromClass([self class]),
            (unsigned int)self,
            self.packType,
            self.documentPackID,
            self.packName,
            self.packDescription,
            self.coverNote,
            self.coverStyleID,
            self.allowEmail,
            self.allowPrint,
            self.isDeleted
            ];
}


@end
