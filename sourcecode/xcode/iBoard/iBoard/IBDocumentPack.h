//
//  IBDocumentPack.h
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/04/28.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "IBSerializable.h"


@interface IBDocumentPack : NSObject <IBSerializable>

@property (nonatomic, copy, readonly) NSString *packType;
@property (nonatomic, copy, readonly) NSArray *documentList;
@property (nonatomic, copy, readonly) NSString *documentPackID;
@property (nonatomic, copy, readonly) NSString *packName;
@property (nonatomic, copy, readonly) NSString *packDescription;
@property (nonatomic, copy, readonly) NSString *coverNote;
@property (nonatomic, copy, readonly) NSString *coverStyleID;
@property (nonatomic, copy, readonly) NSString *allowEmail;
@property (nonatomic, copy, readonly) NSString *allowPrint;
@property (nonatomic, copy, readonly) NSString *isDeleted;

/**
 * Initialize a new instance
 */
- (instancetype)initWithPackType:(NSString *)packType
                    documentList:(NSArray *)documentList
                  documentPackID:(NSString *)documentPackID
                        packName:(NSString *)packName
                 packDescription:(NSString *)packDescription
                       coverNote:(NSString *)coverNote
                    coverStyleID:(NSString *)coverStyleID
                      allowEmail:(NSString *)allowEmail
                      allowPrint:(NSString *)allowPrint
                       isDeleted:(NSString *)isDeleted;


/**
 * Create a new instance from a dictionary
 */
- (instancetype)initWithDictionary:(NSDictionary *)dict;

/**
 * Returns a dictionary representation of the model suitable
 * for JSON serialization
 */
- (NSDictionary *)dictionaryRepresentation;


@end