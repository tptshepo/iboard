//
//  IBService_NSURLSessionRequest.h
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/04/11.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>


#import "IBService.h"

@protocol IBService_NSURLSessionRequestDelegate;

/**
 * An instance of this class models a request encapsulated in a
 * NSURLSessionDataTask. It also tracks its unique identifier as well
 * as the expected HTTP status codes, and the appropriate dispatch blocks
 * for the success and failure cases.
 */
@interface IBService_NSURLSessionRequest : NSObject

@property (nonatomic, weak) id<IBService_NSURLSessionRequestDelegate> delegate;
@property (nonatomic, strong) NSURLRequest *URLRequest;
@property (nonatomic, assign) NSInteger expectedStatus;
@property (nonatomic, copy) IBServiceSuccess successBlock;
@property (nonatomic, copy) IBServiceFailure failureBlock;

/**
 * Initialize a new instance which will immediately schedule the request. Delegate
 * methods will be invoked depending on the final response.
 */
- (instancetype)initWithRequest:(NSURLRequest *)request
                   usingSession:(NSURLSession *)session
                 expectedStatus:(NSInteger)expectedStatus
                        success:(IBServiceSuccess)success
                        failure:(IBServiceFailure)failure
                       delegate:(id<IBService_NSURLSessionRequestDelegate>)delegate;

/**
 * Cancel this request.
 */
- (void)cancel;

/**
 * Restart this request. Delegate methods should be invoked depending on final response
 */
- (void)restart;

/**
 * The unique identifier of the request
 */
- (NSString *)requestIdentifier;

@end

@protocol IBService_NSURLSessionRequestDelegate <NSObject>

/**
 * Indicates that the request completed successfully with the response
 * returned the expected status code.
 */
- (void)sessionRequestDidComplete:(IBService_NSURLSessionRequest *)request;

/**
 * Indicates that the request failed for some reason, described in the given error
 */
- (void)sessionRequestFailed:(IBService_NSURLSessionRequest *)request error:(NSError *)error;

/**
 * Indicates that the request failed authentication (401 response) and requires
 * authentication before proceeding.
 */
- (void)sessionRequestRequiresAuthentication:(IBService_NSURLSessionRequest *)request;

@end