//
//  IBAgendaView.h
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/05/24.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Meeting.h"
#import "Agenda.h"

@protocol IBAgendaViewDelegate;

@interface IBAgendaView : UIView


@property (weak, nonatomic) IBOutlet UIView *view;

@property (weak, nonatomic) IBOutlet UIView *gridContainer;

-(void)setMeetingAgendaItemList:(NSArray *)meetingAgendaList companyMeeting:(Meeting*)companyMeeting;

@property (nonatomic, weak) id<IBAgendaViewDelegate> delegate;

@end


@protocol IBAgendaViewDelegate

- (void)agendaItemSelected:(Agenda *)meetingAgendaItem;

@end
