//
//  IBService.m
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/04/11.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import "IBService.h"


#import "IBProfile.h"
#import "IBCompany.h"
#import "IBKeychain.h"
#import "NSArray+Enumerable.h"
#import "IBService_NSURLSession.h"
#import "IBFileManager.h"
#import "IBDocumentPack.h"
#import "IBCompanyMeeting.h"
#import "IBDocumentPackData.h"

//TODO Fix caching for multiple companies
static BOOL const CACHE_RESULTS = NO;

NSString * const IBServiceAuthRequiredNotification = @"IBServiceAuthRequiredNotification";

/*
 The user-defaults key for the GetCompanies server timestamp
 */
static NSString * const IBGetCompaniesServerTimestampKey = @"GetCompaniesServerTimestamp_v3";
static NSString * const IBGetDocumentPacksServerTimestampKey = @"GetDocumentPacksServerTimestamp_v3";


/**
 * The user-defaults key for the URL of the last server the user authenticated
 * with
 */
static NSString * const IBLastServerURLKey = @"LastServerURL";

static NSString * const IBSavePasswordKey = @"SavePassword";

/**
 * The user-defaults key for the user identifier
 */
static NSString * const IBUserIdentifierKey = @"UserIdentifier";

/**
 * The user-defaults key for the current user
 */
static NSString * const IBCurrentUserKey = @"CurrentUser";

/**
 * The keychain key for the API key
 */
static NSString * const IBUserAPIKeyKey = @"APIKey";

static IBService *SharedInstance;

@interface IBService ()

@property (nonatomic, strong) IBProfile *currentUser;
@property (nonatomic, strong) NSURL *tempServerRoot;
@property (nonatomic, strong) NSURL *serverRoot;
@property (nonatomic, strong) NSMutableDictionary *requests;

@property (nonatomic, strong) NSURLCache *cache;

@end

@implementation IBService

+ (IBService *)sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        SharedInstance = [[IBService_NSURLSession alloc] init];
    });
    
    return SharedInstance;
}

- (instancetype)init
{
    if ((self = [super init])) {
        
        self.cache = [[NSURLCache alloc] initWithMemoryCapacity:4*1024*1024 // 4MB
                                                   diskCapacity:32*1024*1024 // 32MB
                                                       diskPath:NSStringFromClass([self class])];
        [NSURLCache setSharedURLCache:self.cache];
        
        self.requests = [NSMutableDictionary dictionary];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *serverRootString = [defaults stringForKey:IBLastServerURLKey];
        if (serverRootString) {
            self.serverRoot = [NSURL URLWithString:serverRootString];
        }
        
        NSDictionary *userDict = [defaults objectForKey:IBCurrentUserKey];
        if (userDict) {
            self.currentUser = [[IBProfile alloc] initWithDictionary:userDict];
        }
        
        NSError *error = nil;
        NSData *APIKeyData = [IBKeychain secretForKey:IBUserAPIKeyKey error:&error];
        if (APIKeyData) {
            self.currentUser.APIKey = [[NSString alloc] initWithData:APIKeyData encoding:NSUTF8StringEncoding];
        }
    }
    return self;
}

#pragma mark - REST API methods

-(BOOL)isPasswordSaved
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL savePassword = [defaults boolForKey:IBSavePasswordKey];
    return savePassword;
}

#pragma mark - User Authentication

- (NSString *)signInWithUserName:(NSString *)userName
                        password:(NSString *)password
                       deviceUID:(NSString *)deviceUID
                      deviceType:(NSString *)deviceType
                   deviceVersion:(NSString *)deviceVersion
                       otherInfo:(NSString *)otherInfo
                       serverURL:(NSURL *)serverURL
                    savePassword:(BOOL)savePassword
                         success:(void(^)(IBProfile *profile))success
                         failure:(IBServiceFailure)failure;
{
    self.tempServerRoot = serverURL;
    
    [self persistSavePasswordStatus:savePassword];
    
    NSDictionary *params = @{
                             @"UserName":userName,
                             @"Password":password,
                             @"DeviceUID":deviceUID,
                             @"DeviceType":deviceType,
                             @"DeviceVersion":deviceVersion,
                             @"OtherInfo":otherInfo
                             };
    
    return [self submitPOSTPath:@"/Api/Login"
                           body:params
                 expectedStatus:200
                        success:^(NSData *data, NSURLResponse *response) {
                            NSError *error = nil;
                            NSDictionary *userDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                            if (userDict && [userDict isKindOfClass:[NSDictionary class]]) {
                                
                                self.currentUser = [[IBProfile alloc] initWithDictionary:userDict];
                                self.tempServerRoot = nil;
                                self.serverRoot = serverURL;
                                
                                [self persistServerRootAndUserIdentifier];
                                
                                if (success != NULL) {
                                    success(self.currentUser);
                                }
                                
                                [self resendRequestsPendingAuthentication];
                            }
                            else {
                                if (failure != NULL) {
                                    failure(error,0);
                                }
                            }
                        }
                        failure:^(NSError *error, NSInteger statusCode) {
                            self.tempServerRoot = nil;
                            if (failure != NULL) {
                                failure(error, statusCode);
                            }
                        }];
}

- (NSString *)signoutUserWithSuccess:(void (^)())success
                             failure:(IBServiceFailure)failure
{
    
    self.serverRoot = nil;
    self.currentUser = nil;
    
    [self persistServerRootAndUserIdentifier];
    [self persistSavePasswordStatus:NO];
    
    if (success != NULL) {
        success();
    }
    return @"";
    
    /*return [self submitDELETEPath:@"/api/logout"
     success:^(NSData *data) {
     self.serverRoot = nil;
     self.currentUser = nil;
     
     [self persistServerRootAndUserIdentifier];
     
     if (success != NULL) {
     success();
     }
     }
     failure:failure];*/
}

- (BOOL)isUserSignedIn
{
    return self.serverRoot != nil;
}


#pragma mark - Cache

-(void)clearCache
{
    
}

#pragma mark - Save Document Annotation

-(NSString *)getDocumentAnnotations:(NSString *)documentID
                       success:(void (^)(NSData *))success
                       failure:(IBServiceFailure)failure
{
    
    NSString *path = @"/Api/GetDocumentAnnotations";
    path = [NSString stringWithFormat:@"%@?DocumentID=%@", path, documentID];
    
    NSLog(@"%@", path);
    
    return [self submitGETPath:path
                       success:^(NSData *data, NSURLResponse *response) {
                           success(data);
                       }
                       failure:failure];
}

-(NSString *)saveDocumentAnnotationWithDictionary:(NSDictionary *)bodyDict
                                    success:(void (^)(NSData *))success
                                    failure:(IBServiceFailure)failure
{
    NSString *path = @"/Api/SaveDocumentAnnotation";
    path = [NSString stringWithFormat:@"%@", path];
    
    NSLog(@"%@", path);
    
    return [self submitPOSTPath:path
                           body:bodyDict
                 expectedStatus:200
                        success:^(NSData *data, NSURLResponse *response) {
                            
                            success(data);
                        
                        }
                        failure:^(NSError *error, NSInteger statusCode) {
                            self.tempServerRoot = nil;
                            if (failure != NULL) {
                                failure(error, statusCode);
                            }
                        }];
    
}


- (NSString *)deleteDocumentAnnotation:(NSString *)documentAnnotationID
                       success:(void (^)(NSData *))success
                       failure:(IBServiceFailure)failure
{
    
    NSString *path = @"/Api/DeleteDocumentAnnotation";
    path = [NSString stringWithFormat:@"%@?DocumentAnnotationID=%@", path, documentAnnotationID];
    
    NSLog(@"%@", path);
    
    NSDictionary *bodyDict = @{@"DocumentAnnotationID": documentAnnotationID};
    
    return [self submitPOSTPath:path
                           body:bodyDict
                 expectedStatus:200
                        success:^(NSData *data, NSURLResponse *response) {
                            success(data);
                        }
                        failure:^(NSError *error, NSInteger statusCode) {
                            self.tempServerRoot = nil;
                            if (failure != NULL) {
                                failure(error, statusCode);
                            }
                        }];
}

#pragma mark - Get Document

-(NSString *)getDocumentWithID:(NSString *)documentID
                       success:(void (^)(NSData *))success
                       failure:(IBServiceFailure)failure
{
    
    //Check cache first
    if ( [[IBFileManager sharedInstance] hasDocumentID:documentID] )
    {
        NSData* data = [[IBFileManager sharedInstance] getDocumentWithDocumentID:documentID];
        success(data);
        return @"";
    }
 
    NSString *path = @"/Api/GetDocument";
    path = [NSString stringWithFormat:@"%@?DocumentID=%@", path, documentID];
    
    NSLog(@"%@", path);
    
    return [self submitGETPath:path
                       success:^(NSData *data, NSURLResponse *response) {
                           dispatch_async(dispatch_get_main_queue(), ^{
                               [[IBFileManager sharedInstance] saveDocumentWithDocumentID:documentID content:data];
                               success(data);
                           });
                       }
                       failure:failure];
}

#pragma mark - Get Image

-(NSString *)getImageWithID:(NSString *)imageID
                    success:(void (^)(UIImage *))success
                    failure:(IBServiceFailure)failure
{
        //Check cache first
    if ( [[IBFileManager sharedInstance] hasImageID:imageID] )
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSData* data = [[IBFileManager sharedInstance] getImageWithImageID:imageID];
            UIImage *image = [UIImage imageWithData:data];
            success(image);
        });
        return @"";
    }
    
    NSString *path = @"/Api/GetImage";
    path = [NSString stringWithFormat:@"%@?ImageID=%@", path, imageID];
    
    // look up image from cahce
    NSURL *url = [self URLWithPath:path];
    
    NSLog(@"%@", url);
    
    return [self submitGETPath:path
                       success:^(NSData *data, NSURLResponse *response) {
                           
                           dispatch_async(dispatch_get_main_queue(), ^{
                               [[IBFileManager sharedInstance] saveImageWithImageID:imageID content:data];
                               UIImage *image = [UIImage imageWithData:data];
                               success(image);
                           });
                           
                       }
                       failure:failure];
}

#pragma mark - Get Companies

-(NSString *)getCompaniesSuccess:(void (^)(NSArray*))success
                         failure:(IBServiceFailure)failure
{
    NSString *path = @"/Api/GetCompanies";
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *serverTimeStamp = [defaults stringForKey:IBGetCompaniesServerTimestampKey];
    if (serverTimeStamp) {
        if (CACHE_RESULTS)
        {
            path = [NSString stringWithFormat:@"%@?LastQueryDate=%@", path, serverTimeStamp];
        }
    }
    
    return [self submitGETPath:path
                       success:^(NSData *data, NSURLResponse *response) {
                           
                           NSError *error = nil;
                           
                           
                           //get the data object
                           
                           NSDictionary *resultsRoot = [NSJSONSerialization JSONObjectWithData:data
                                                                                       options:0
                                                                                         error:&error];
                           
                           
                           
                           if (resultsRoot && [resultsRoot isKindOfClass:[NSDictionary class]]) {
                               // validate response
                           } else {
                               // did not get the expected JSON format
                               if (failure != NULL) {
                                   failure(error,0);
                               }
                               
                               return;
                           }
                           
                           NSDictionary *dictData = [resultsRoot valueForKeyPath:@"Data"];
                           NSString *serverTimeStamp = [resultsRoot valueForKeyPath:@"ServerTimestamp"];
                           [self persistGetCompaniesServerTimeStamp:serverTimeStamp];
                           
                           
                           error = nil;
                           NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictData
                                                                              options:NSJSONWritingPrettyPrinted
                                                                                error:&error];
                           
                           if (error){
                               // did not get the expected JSON format
                               if (failure != NULL) {
                                   failure(error,0);
                               }
                               return;
                           }
                           
                           
                           // if ([companies count] > 0) {
                           //cache the response to file
                           //   [[IBFileManager sharedInstance] saveCompanies:[self jsonData:dictData]];
                           //}
                           
                           
                           error = nil;
                           NSArray *results = [NSJSONSerialization JSONObjectWithData:jsonData
                                                                              options:0
                                                                                error:&error];
                           
                           
                           
                           if (results && [results isKindOfClass:[NSArray class]]) {
                               
                               NSArray *companies = [results mappedArrayWithBlock:^id(id obj) {
                                   return [[IBCompany alloc] initWithDictionary:obj];
                               }];
                               
                               if (success != NULL) {
                                   success(companies);
                               }
                           }
                           else {
                               if (failure != NULL) {
                                   failure(error,0);
                               }
                           }
                           
                       }
                       failure:failure];
}

#pragma mark - Get Documents Pack

-(NSString *)getDocumentsPacksWithComapnyID:(int)companyID
                                    success:(void (^)(IBDocumentPackData *))success
                                    failure:(IBServiceFailure)failure
{
    NSString *path = @"/Api/GetDocumentPacks";
    
    path = [NSString stringWithFormat:@"%@?CompanyID=%d", path, companyID];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *serverTimeStamp = [defaults stringForKey:IBGetDocumentPacksServerTimestampKey];
    if (serverTimeStamp) {
        if (CACHE_RESULTS)
        {
            path = [NSString stringWithFormat:@"%@&LastQueryDate=%@", path, serverTimeStamp];
        }
    }
    
    return [self submitGETPath:path
                       success:^(NSData *data, NSURLResponse *response) {
                           
                           NSError *error = nil;
                           
                           //get the data object
                           NSDictionary *resultsRoot = [NSJSONSerialization JSONObjectWithData:data
                                                                                       options:0
                                                                                         error:&error];
                           
                           if (resultsRoot && [resultsRoot isKindOfClass:[NSDictionary class]]) {
                               // validate response
                           } else {
                               // did not get the expected JSON format
                               if (failure != NULL) {
                                   failure(error,0);
                               }
                               
                               return;
                           }
                           
                           
                           
                           NSDictionary *dictData = [resultsRoot valueForKeyPath:@"Data"];
                           NSDictionary *dictDocumentPackList = [dictData valueForKeyPath:@"DocumentPackList"];
                           NSDictionary *dictMeetingList = [dictData valueForKeyPath:@"MeetingList"];
                           NSString *serverTimeStamp = [resultsRoot valueForKeyPath:@"ServerTimestamp"];
                           [self persistGetDocumentPacksServerTimeStamp:serverTimeStamp];
                           
                           
                           /***********************************/
                           // Read the Document List Array
                           /***********************************/
                           
                           error = nil;
                           NSData *nsDataDocumentPackList = [NSJSONSerialization dataWithJSONObject:dictDocumentPackList
                                                                                            options:NSJSONWritingPrettyPrinted
                                                                                              error:&error];
                           
                           if (error){
                               // did not get the expected JSON format
                               if (failure != NULL) {
                                   failure(error,0);
                               }
                               return;
                           }
                           
                           
                           error = nil;
                           NSArray *nsArrayDocumentPackList = [NSJSONSerialization JSONObjectWithData:nsDataDocumentPackList
                                                                                              options:0
                                                                                                error:&error];
                           
                           
                           if (error){
                               // did not get the expected JSON format
                               if (failure != NULL) {
                                   failure(error,0);
                               }
                               return;
                           }
                           
                           /***********************************/
                           // Read the Meeting List Array
                           /***********************************/
                           
                           
                           error = nil;
                           NSData *nsDataMeetingList = [NSJSONSerialization dataWithJSONObject:dictMeetingList
                                                                                       options:NSJSONWritingPrettyPrinted
                                                                                         error:&error];
                           
                           if (error){
                               // did not get the expected JSON format
                               if (failure != NULL) {
                                   failure(error,0);
                               }
                               return;
                           }
                           
                           
                           error = nil;
                           NSArray *nsArrayMeetingList = [NSJSONSerialization JSONObjectWithData:nsDataMeetingList
                                                                                         options:0
                                                                                           error:&error];
                           
                           
                           if (error){
                               // did not get the expected JSON format
                               if (failure != NULL) {
                                   failure(error,0);
                               }
                               return;
                           }
                           
                           IBDocumentPackData *documentPackData = [[IBDocumentPackData alloc] initWithDictionary:nsArrayDocumentPackList
                                                                                              nsArrayMeetingList:nsArrayMeetingList
                                                                                                 serverTimestamp:serverTimeStamp];
                           
                           
                           if (success != NULL) {
                               success(documentPackData);
                           }
                           
                           
                       }
                       failure:failure];
}



#pragma mark - Abstract methods

- (NSString *)submitRequestWithURL:(NSURL *)URL
                            method:(NSString *)httpMethod
                              body:(NSDictionary *)bodyDict
                    expectedStatus:(NSInteger)expectedStatus
                           success:(IBServiceSuccess)success
                           failure:(IBServiceFailure)failure
{
    NSAssert(NO, @"%s must be implemented in a sub-class!", __PRETTY_FUNCTION__);
    return nil;
}

- (void)cancelRequestWithIdentifier:(NSString *)identifier
{
    NSAssert(NO, @"%s must be implemented in a sub-class!", __PRETTY_FUNCTION__);
}

- (void)resendRequestsPendingAuthentication
{
    NSAssert(NO, @"%s must be implemented in a sub-class!", __PRETTY_FUNCTION__);
}

#pragma mark - Request Helpers

-(void)persistSavePasswordStatus:(BOOL)savePassword
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:savePassword forKey:IBSavePasswordKey];
    [defaults synchronize];
}

- (void)persistServerRootAndUserIdentifier
{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:self.serverRoot.absoluteString forKey:IBLastServerURLKey];
    [defaults setObject:self.currentUser.userID forKey:IBUserIdentifierKey];
    [defaults setObject:[self.currentUser dictionaryRepresentation] forKey:IBCurrentUserKey];
    [defaults synchronize];
    
    NSError *error = nil;
    // We're storing the key in the keychain
    if (self.currentUser.APIKey) {
        NSData *APIKeyData = [self.currentUser.APIKey dataUsingEncoding:NSUTF8StringEncoding];
        if (! [IBKeychain storeSecret:APIKeyData forKey:IBUserAPIKeyKey error:&error]) {
            NSLog(@"ERROR: Unable to persist API key to the keychain: %@", error);
        }
    }
    // We're clearing the key out of the keychain
    else {
        if (! [IBKeychain deleteSecretForKey:IBUserAPIKeyKey error:&error]) {
            if (error){
                NSLog(@"ERROR: Unable to remove API key from the keychain: %@", error);
            }
        }
    }
}


- (void)persistGetDocumentPacksServerTimeStamp:(NSString *)serverTimeStamp
{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:serverTimeStamp forKey:IBGetDocumentPacksServerTimestampKey];
    [defaults synchronize];
}

- (void)persistGetCompaniesServerTimeStamp:(NSString *)serverTimeStamp
{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:serverTimeStamp forKey:IBGetCompaniesServerTimestampKey];
    [defaults synchronize];
}

- (NSURL *)URLWithPath:(NSString *)path
{
    NSURL *root = self.serverRoot ?: self.tempServerRoot;
    NSAssert(root != nil, @"Cannot make requests if neither serverRoot or tempServerRoot are nil");
    return [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", root, path]];
}

- (NSString *)submitGETPath:(NSString *)path
                    success:(IBServiceSuccess)success
                    failure:(IBServiceFailure)failure
{
    NSURL *URL = [self URLWithPath:path];
    return [self submitRequestWithURL:URL
                               method:@"GET"
                                 body:nil
                       expectedStatus:200
                              success:success
                              failure:failure];
}

- (NSString *)submitDELETEPath:(NSString *)path
                       success:(IBServiceSuccess)success
                       failure:(IBServiceFailure)failure
{
    NSURL *URL = [self URLWithPath:path];
    return [self submitRequestWithURL:URL
                               method:@"DELETE"
                                 body:nil
                       expectedStatus:200
                              success:success
                              failure:failure];
}

- (NSString *)submitPOSTPath:(NSString *)path
                        body:(NSDictionary *)bodyDict
              expectedStatus:(NSInteger)expectedStatus
                     success:(IBServiceSuccess)success
                     failure:(IBServiceFailure)failure
{
    NSURL *URL = [self URLWithPath:path];
    return [self submitRequestWithURL:URL
                               method:@"POST"
                                 body:bodyDict
                       expectedStatus:expectedStatus
                              success:success
                              failure:failure];
}

- (NSString *)submitPUTPath:(NSString *)path
                       body:(NSDictionary *)bodyDict
             expectedStatus:(NSInteger)expectedStatus
                    success:(IBServiceSuccess)success
                    failure:(IBServiceFailure)failure
{
    NSURL *URL = [self URLWithPath:path];
    return [self submitRequestWithURL:URL
                               method:@"PUT"
                                 body:bodyDict
                       expectedStatus:expectedStatus
                              success:success
                              failure:failure];
}

-(NSString*)jsonData:(NSDictionary*) dict
{
    NSError *error;
    NSString *jsonString = @"";
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    return jsonString;
}


- (NSData *)formEncodedParameters:(NSDictionary *)parameters
{
    NSArray *pairs = [parameters.allKeys mappedArrayWithBlock:^id(id obj) {
        return [NSString stringWithFormat:@"%@=%@",
                [obj stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],
                [parameters[obj] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    }];
    
    NSString *formBody = [pairs componentsJoinedByString:@"&"];
    
    return [formBody dataUsingEncoding:NSUTF8StringEncoding];
}

- (NSMutableURLRequest *)requestForURL:(NSURL *)URL
                                method:(NSString *)httpMethod
                              bodyDict:(NSDictionary *)bodyDict
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    [request setHTTPMethod:httpMethod];
    
    // For now, assume body content is always form-urlencoded
    if (bodyDict) {
        //[request setHTTPBody:[self formEncodedParameters:bodyDict]];
        
        
        NSError *error;
        NSString *jsonString = @"";
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:bodyDict
                                                           options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                             error:&error];
        
        if (! jsonData) {
            NSLog(@"Got an error: %@", error);
        } else {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        
        
        [request setHTTPBody:jsonData];
        [request setValue:[NSString stringWithFormat:@"%d", (int)[jsonData length]] forHTTPHeaderField:@"Content-Length"];
        //[request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    }
    
    //[request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    return request;
}

@end