//
//  IBDrawingSurfaceView.m
//  DrawPad
//
//  Created by Tshepo Mgaga on 2015/06/16.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import "IBDrawingSurfaceView.h"

#import "IBAnnotationCommentView.h"
#import "IBFileManager.h"
#import "IBAnnotation.h"
#import "IBAnnotationComment.h"
#import "IBService.h"

typedef enum {
    AnnotationTypeNone,
    AnnotationTypePen,
    AnnotationTypeHighlighter,
    AnnotationTypeComment
} AnnotationType;

@implementation IBDrawingSurfaceView
{
    CGPoint lastPoint;
    
    
    BOOL mouseSwiped;
    BOOL _canCallServer;
    
    
    IBAnnotationCommentView * lastCommentIcon;
    UITextView * lastCommentText;
    CGPoint textLastPoint;
    
    NSMutableArray *commentIcons;
    
    UIImageView *tempDrawImage;
    UIImageView *mainImage;
    
    NSInteger documentAnnotationID;
    
    BOOL savingAnnotation;
}

static CGFloat penBrush = 3.0;
static CGFloat penOpacity = 1.0;

static CGFloat highlighterBrush = 15.0;
static CGFloat highlighterOpacity = 0.5;


static CGFloat penRed = 0.0/255.0;
static CGFloat penGreen = 0.0/255.0;
static CGFloat penBlue = 0.0/255.0;

static CGFloat highlighterRed = 0.0/255.0;
static CGFloat highlighterGreen = 0.0/255.0;
static CGFloat highlighterBlue = 0.0/255.0;

static AnnotationType drawMode;

@synthesize documentId = _documentId;
@synthesize pageNumber = _pageNumber;

#pragma mark - Initialise View Methods

- (instancetype)initWithFrame:(CGRect)frame page:(NSUInteger)page documentId:(NSString*)documentId canCallServer:(BOOL)canCallServer;
{
    self = [super initWithFrame:frame];
    if (self) {
        
        _canCallServer = canCallServer;
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(receiveColorNotification:)
                                                     name:@"ColorNotification"
                                                   object:nil];
        
        _documentId = documentId;
        _pageNumber = page;
        
        
        
        [self viewDidLoad];
        
    }
    return self;
}


- (void)viewDidLoad {
    
    commentIcons = [[NSMutableArray alloc] init];
    
    savingAnnotation = NO;
    
    //documentAnnotationID = [self getDocumentAnnotationID];

    [self downloadAnnotations];
    
    
    //    [[IBService sharedInstance] deleteDocumentAnnotation:@"13"
    //                                               success:^(NSData *data) {
    //                                                   NSLog(@"deleted annotation");
    //                                               }
    //                                               failure:^(NSError *error, NSInteger statusCode) {
    //                                                   NSLog(@"delete annotion failed");
    //
    //                                               }];
    
    
    self.userInteractionEnabled = YES;
    
    tempDrawImage = [[UIImageView alloc] initWithFrame:self.frame];
    [self addSubview:tempDrawImage];
    
    mainImage = [[UIImageView alloc] initWithFrame:self.frame];
    [self addSubview:mainImage];
    
    [self setBackgroundColor:[UIColor clearColor]];
    

    [self restoreAnnotationFromJSON];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark - Notifications

- (void) receiveColorNotification:(NSNotification *) notification
{
    //NSLog(@"notification: %@", notification.userInfo.description);
    
    NSString *action = [notification.userInfo valueForKey:@"action"];
    
    if ([action isEqualToString:@"color"]) {
        NSString *color = [notification.userInfo valueForKey:@"color"];
        NSNumber *r = [notification.userInfo valueForKey:@"red"];
        NSNumber *g = [notification.userInfo valueForKey:@"green"];
        NSNumber *b = [notification.userInfo valueForKey:@"blue"];
        
        
        if ([color isEqualToString:@"pen"]){
            penRed = [r floatValue] ;
            penGreen = [g floatValue];
            penBlue = [b floatValue];
        }
        
        if ([color isEqualToString:@"highlighter"]){
            highlighterRed = [r floatValue] ;
            highlighterGreen = [g floatValue];
            highlighterBlue = [b floatValue];
        }
    }
    
    
    
    if ([action isEqualToString:@"annotationType"]){
        
        NSString *type = [notification.userInfo valueForKey:@"type"];
        
        if ([type isEqualToString:@"highlighter"]){
            [self setDrawingType:AnnotationTypeHighlighter];
        }
        
        else if ([type isEqualToString:@"comment"]){
            [self setDrawingType:AnnotationTypeComment];
        }
        
        else if ([type isEqualToString:@"pen"]){
            [self setDrawingType:AnnotationTypePen];
        }
        else {
            [self setDrawingType:AnnotationTypeNone];
        }
        
    }
    
}

#pragma mark - Annotation Helpers

-(NSInteger)getDocumentAnnotationID
{
    //save the document annotation ID
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString* key = [NSString stringWithFormat:@"Annotation_%@_%@", _documentId, @(_pageNumber)];
    NSNumber *number = [defaults objectForKey:key];
    return [number integerValue];
}

-(void)downloadAnnotations
{
    if (!_canCallServer)
        return;
    
    //__weak typeof(self) weakSelf = self;
    [[IBService sharedInstance] getDocumentAnnotations:_documentId
                                               success:^(NSData *data) {
                                                   NSLog(@"get annotations");
                                                   
                                                   if (documentAnnotationID == 0)
                                                   {
                                                       //get the data object
//                                                       NSError *error = nil;
//                                                       NSDictionary *resultsRoot = [NSJSONSerialization JSONObjectWithData:data
//                                                                                                                   options:0
//                                                                                                                     error:&error];
                                                       
                                                       
                                                       //NSLog(@"%@", resultsRoot);
                                                       
                                                       
                                                   }
                                                   
                                               }
                                               failure:^(NSError *error, NSInteger statusCode) {
                                                   NSLog(@"get annotions failed");
                                                   
                                               }];
    
}

-(void)saveAnnotations
{
    if (savingAnnotation)
        return;
    
    if (!_canCallServer)
        return;
    
    savingAnnotation = YES;
    
    NSMutableArray* ibComments = [[NSMutableArray alloc] init];
    
    for (IBAnnotationCommentView *commentIcon in commentIcons)
    {
        if (commentIcon.isDeleted)
            continue;
        
        IBAnnotationComment *commentObj = [[IBAnnotationComment alloc] initWithPointX:[NSString stringWithFormat:@"%f", commentIcon.frame.origin.x]
                                                                               pointY:[NSString stringWithFormat:@"%f", commentIcon.frame.origin.y]
                                                                                 text:[commentIcon getText]];
        [ibComments addObject:commentObj];
    }
    
    
    
    NSString *encodedString = [UIImagePNGRepresentation(mainImage.image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
    if (encodedString == nil)
        encodedString = @"";
    
    IBAnnotationJSONData* ibAnnotationJSONData = [[IBAnnotationJSONData alloc] initWithGraphics:encodedString
                                                                                       comments:[ibComments copy]];
    
    
    IBAnnotation* ibAnnotation = [[IBAnnotation alloc]initWithDocumentAnnotationID:documentAnnotationID
                                                                        documentID:[_documentId integerValue]
                                                                            pageNo:_pageNumber
                                                                          jSONData:ibAnnotationJSONData];
    
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:ibAnnotation.dictionaryRepresentation
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        //NSLog(@"%@", jsonString);
        
        
        //save to file
        [[IBFileManager sharedInstance] saveAnnotationJSONWithDocumentID:_documentId page:[NSString stringWithFormat:@"%@",  @(_pageNumber)] data:jsonString];
        
        
        //__weak typeof(self) weakSelf = self;
        [[IBService sharedInstance] saveDocumentAnnotationWithDictionary:ibAnnotation.dictionaryRepresentation
                                                                 success:^(NSData *data) {
                                                                     NSLog(@"annotation saved");
                                                                     
                                                                     
                                                                     //get the data object
                                                                     NSError *error = nil;
                                                                     NSDictionary *resultsRoot = [NSJSONSerialization JSONObjectWithData:data
                                                                                                                                 options:0
                                                                                                                                   error:&error];
                                                                     
                                                                     //save the document annotation ID
                                                                     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                                                                     NSString* key = [NSString stringWithFormat:@"Annotation_%@_%@", _documentId, @(_pageNumber)];
                                                                     [defaults setObject:resultsRoot[@"DocumentAnnotationID"] forKey:key];
                                                                     [defaults synchronize];
                                                                     
                                                                     NSNumber *number = [defaults objectForKey:key];
                                                                     documentAnnotationID = [number integerValue];
                                                                     
                                                                     
                                                                     savingAnnotation = NO;
                                                                 }
                                                                 failure:^(NSError *error, NSInteger statusCode) {
                                                                     NSLog(@"annotion failed");
                                                                     
                                                                     savingAnnotation = NO;
                                                                 }];
        
    }
    
}


-(void)restoreAnnotationFromJSON
{
    if (!_canCallServer)
        return;
    
    NSString *annotationJSON = [[IBFileManager sharedInstance] getAnnotationJSONWithDocumentID:_documentId page:[NSString stringWithFormat:@"%@",  @(_pageNumber)]];
    
    if (annotationJSON == nil)
    {
        NSLog(@"No saved annotations");
        return;
    }
    
    //NSLog(@"%@", annotationJSON);
    
    NSLog(@"Restoring saved annotations");
    
    NSData* data = [annotationJSON dataUsingEncoding:NSUTF8StringEncoding];
    
    NSError *error = nil;
    NSDictionary *dict = [NSJSONSerialization  JSONObjectWithData:data options:0 error:&error];
    
    IBAnnotation* ibAnnotation = [[IBAnnotation alloc]initWithDictionary:dict];
    
    //NSLog(@"%@", ibAnnotation);
    
    
    //restore graphics
    
    if (![ibAnnotation.jSONData.graphics isEqualToString:@""])
    {
        NSData* imageData = [[NSData alloc]initWithBase64EncodedString:ibAnnotation.jSONData.graphics options:NSDataBase64DecodingIgnoreUnknownCharacters];
        mainImage.image = [UIImage imageWithData:imageData];
        
        // Plot comments
        for (IBAnnotationComment *comment in ibAnnotation.jSONData.comments) {
            CGRect commentRect = CGRectMake([comment.pointX floatValue], [comment.pointY floatValue], 38,32);
            [self addCommentView:commentRect text:comment.text restoreMode:YES];
        }
        
    }
    
    
    
}


#pragma mark - Drawing Helpers

-(void)setDrawingType:(AnnotationType)annotationType
{
    drawMode = annotationType;
    
    // hide keyboard
    if (lastCommentIcon != nil){
        [self endEditing: YES];
        [lastCommentIcon hideText];
        lastCommentIcon = nil;
    }
}

- (void)drawOnMove:(CGPoint)currentPoint {
    
    UIGraphicsBeginImageContext(self.frame.size);
    [tempDrawImage.image drawInRect:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    CGContextMoveToPoint(UIGraphicsGetCurrentContext(), lastPoint.x, lastPoint.y);
    CGContextAddLineToPoint(UIGraphicsGetCurrentContext(), currentPoint.x, currentPoint.y);
    CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
    
    if (drawMode == AnnotationTypePen){
        CGContextSetLineWidth(UIGraphicsGetCurrentContext(), penBrush );
        CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), penRed, penGreen, penBlue, 1.0);
    }
    
    if (drawMode == AnnotationTypeHighlighter) {
        CGContextSetLineWidth(UIGraphicsGetCurrentContext(), highlighterBrush );
        CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), highlighterRed, highlighterGreen, highlighterBlue, 1.0);
    }
    
    CGContextSetBlendMode(UIGraphicsGetCurrentContext(),kCGBlendModeNormal);
    
    CGContextStrokePath(UIGraphicsGetCurrentContext());
    tempDrawImage.image = UIGraphicsGetImageFromCurrentImageContext();
    
    
    if (drawMode == AnnotationTypePen)
        [tempDrawImage setAlpha:penOpacity];
    
    if (drawMode == AnnotationTypeHighlighter)
        [tempDrawImage setAlpha:highlighterOpacity];
    
    
    UIGraphicsEndImageContext();
}

-(void)addCommentView:(CGRect)rect text:(NSString*)text restoreMode:(BOOL)restoreMode
{
    IBAnnotationCommentView * commentIcon = [[IBAnnotationCommentView alloc]initWithFrame:rect restoreMode:restoreMode pRect:self.frame];
    commentIcon.delegate = self;
    lastCommentIcon = commentIcon;
    [commentIcon setText:text];
    [self addSubview:commentIcon];
    [commentIcons addObject:commentIcon];
}

#pragma mark - IBAnnotationCommentViewDelegate Methods

-(void)deleteAnnotationCommentView:(IBAnnotationCommentView *)me
{
    [me removeFromSuperview];
}


#pragma mark - UIResponder Events

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [touches anyObject];
    lastPoint = [touch locationInView:self];
    
    if (drawMode == AnnotationTypeComment){
        
        //NSLog(@"%f", self.frame.origin.x);
        //NSLog(@"%f", self.frame.size.width);
        
        
        for (IBAnnotationCommentView *commentIcon in commentIcons)
        {
            if (commentIcon.isDeleted)
                continue;
            
            if ([commentIcon hitTest:lastPoint])
            {
                // hide keyboard
                if (lastCommentIcon != nil){
                    [lastCommentIcon hideText];
                    lastCommentIcon = nil;
                }
                
                lastCommentIcon = commentIcon;
                //NSLog(@"[HITTEST] %@", commentIcon);
                [commentIcon showText];
                return;
            }
        }
        
        
        // hide keyboard
        if (lastCommentIcon != nil){
            [self endEditing: YES];
            [lastCommentIcon hideText];
            lastCommentIcon = nil;
            
            [self saveAnnotations];
            return;
        }
        
        
        // validate touch point
        
        CGRect commentRect = CGRectMake(lastPoint.x, lastPoint.y, 38,32);
        CGRect actualRect = CGRectMake(lastPoint.x - 5, lastPoint.y - 30, commentRect.size.width,commentRect.size.height);
        
        if (actualRect.origin.y < 0)
        {
            return;
        }
        
        // check if the touch is not outside the frame starting x
        if (actualRect.origin.x < 0)
        {
            return;
        }
        
        // check if the touch is not outside the frame width
        if (actualRect.origin.x + actualRect.size.width > self.frame.size.width)
        {
            return;
        }
        
        // Plot new comment
        
        [self addCommentView:commentRect text:@"" restoreMode:NO];
        
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [touches anyObject];
    CGPoint currentPoint = [touch locationInView:self];
    
    if (drawMode == AnnotationTypePen || drawMode == AnnotationTypeHighlighter )
    {
        [self drawOnMove:currentPoint];
    }
    
    lastPoint = currentPoint;
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UIGraphicsBeginImageContext(mainImage.frame.size);
    [mainImage.image drawInRect:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)
                      blendMode:kCGBlendModeNormal
                          alpha:1.0];
    
    if (drawMode == AnnotationTypePen)
        [tempDrawImage.image drawInRect:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)
                              blendMode:kCGBlendModeNormal
                                  alpha:penOpacity];
    
    if (drawMode == AnnotationTypeHighlighter)
        [tempDrawImage.image drawInRect:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)
                              blendMode:kCGBlendModeNormal
                                  alpha:highlighterOpacity];
    
    mainImage.image = UIGraphicsGetImageFromCurrentImageContext();
    
    tempDrawImage.image = nil;
    UIGraphicsEndImageContext();
    
    [self saveAnnotations];
}


@end
