//
//  ARFFileManager.m
//  ARF Interface
//
//  Created by Tshepo Mgaga on 2014/02/26.
//
//

#import "IBFileManager.h"

#import <MobileCoreServices/MobileCoreServices.h>

#import "RNDecryptor.h"
#import "RNEncryptor.h"


static IBFileManager *SharedInstance;


@implementation IBFileManager
{
    NSString *documentdir;
    NSString *dataDirectory;
    NSString *annotationDirectory;
    NSString *documentsDirectory;
    NSString *imagesDirectory;
    
}

+ (IBFileManager *)sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        SharedInstance = [[IBFileManager alloc] init];
    });
    
    return SharedInstance;
}

- (id)init
{
    self = [super init];
    if (self) {
        documentdir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
        
        dataDirectory = [documentdir stringByAppendingPathComponent:@"data"];
        documentsDirectory = [documentdir stringByAppendingPathComponent:@"data/documents"];
        annotationDirectory = [documentdir stringByAppendingPathComponent:@"data/annotation"];
        imagesDirectory = [documentdir stringByAppendingPathComponent:@"data/images"];
        
        NSLog(@"%@", dataDirectory);
    }
    return self;
}


#pragma mark - Images


-(void)saveImageWithImageID:(NSString*)imageID content:(NSData*)content
{
    NSString *file = [[NSString alloc] initWithFormat:@"%@/image_%@.png", imagesDirectory, imageID];
    [self writeToFileWithData:content fileName:file];
}


-(BOOL)hasImageID:(NSString*)imageID
{
    NSString *file = [[NSString alloc] initWithFormat:@"%@/image_%@.png", imagesDirectory, imageID];
    return [self fileNameExisits:file];
}

-(NSData*)getImageWithImageID:(NSString*)imageID
{
    NSString *file = [[NSString alloc] initWithFormat:@"%@/image_%@.png", imagesDirectory, imageID];
    NSData *data = [NSData dataWithContentsOfFile:file];
    return data;
}

-(void)removeImageWithImageID:(NSString *)imageID
{
    if ([self hasImageID:imageID]){
        NSString *file = [[NSString alloc] initWithFormat:@"%@/image_%@.png", imagesDirectory, imageID];
        [[NSFileManager defaultManager] removeItemAtPath:file error:nil];
    }
}

#pragma mark - PDF Documents


-(void)saveDocumentWithDocumentID:(NSString*)documentID content:(NSData*)content
{
    NSString *file = [[NSString alloc] initWithFormat:@"%@/%@.pdf", documentsDirectory, documentID];
    
    //encrypt data
    NSData *encryptedData = [RNEncryptor encryptData:content
                                        withSettings:kRNCryptorAES256Settings
                                            password:@"IB&(@)#)#^^#&#**#" error:nil];
    
    [self writeToFileWithData:encryptedData fileName:file];
}


-(BOOL)hasDocumentID:(NSString*)documentID
{
    NSString *file = [[NSString alloc] initWithFormat:@"%@/%@.pdf", documentsDirectory, documentID];
    return [self fileNameExisits:file];
}

-(NSString*)getDocumentFilePathWithID:(NSString*)documentID
{
    NSString *file = [[NSString alloc] initWithFormat:@"%@/%@.pdf", documentsDirectory, documentID];
    return file;
}

-(NSData*)getDocumentWithDocumentID:(NSString*)documentID
{
    NSString *file = [[NSString alloc] initWithFormat:@"%@/%@.pdf", documentsDirectory, documentID];
    NSData *data = [NSData dataWithContentsOfFile:file];
    
    
    // faq doc is not encrypted
    if ([documentID  isEqualToString:@"faq"]){
        return data;
    }
    
//    // decrypt data
    NSData *decryptedData = [RNDecryptor decryptData:data
                                        withSettings:kRNCryptorAES256Settings
                                            password:@"IB&(@)#)#^^#&#**#" error:nil];
    
    return decryptedData;
}

#pragma mark - Annotation Image

-(void)saveAnnotationWithDocumentID:(NSString *)documentID image:(UIImage *)image page:(NSString*)page
{
    NSString *file = [[NSString alloc] initWithFormat:@"%@/%@_%@.png", annotationDirectory, documentID, page];
    [UIImagePNGRepresentation(image) writeToFile:file atomically:YES];
}

-(UIImage *)getAnnotationWithDocumentID:(NSString *)documentID page:(NSString*)page
{
    
    NSString *file = [[NSString alloc] initWithFormat:@"%@/%@_%@.png", annotationDirectory, documentID, page];
    
    if ([self fileNameExisits:file])
    {
        UIImage* image = [UIImage imageWithContentsOfFile:file];
        return image;
        
    }
    
    return nil;
}

#pragma mark - Annotation JSON

-(void)saveAnnotationJSONWithDocumentID:(NSString *)documentID page:(NSString*)page data:(NSString*)data
{
    NSString *file = [[NSString alloc] initWithFormat:@"%@/%@_%@.json", annotationDirectory, documentID, page];
    [self writeToFileWithFileName:file content:data];
}

-(NSString *)getAnnotationJSONWithDocumentID:(NSString *)documentID page:(NSString*)page
{
    
    NSString *file = [[NSString alloc] initWithFormat:@"%@/%@_%@.json", annotationDirectory, documentID, page];
    return [self getFileContents:file];
}

#pragma mark - File Helpers

-(NSString *)getDataRoot
{
    return dataDirectory;
}

-(void)reset
{
    //remove files
    //[self deleteFile:@"messages.json"];
    
}

-(BOOL)deleteFileName:(NSString *)fileName
{
    NSString *file = [[NSString alloc] initWithFormat:@"%@/%@",dataDirectory, fileName];
    NSError *error = nil;
    [[NSFileManager defaultManager] removeItemAtPath:file error:&error];
    return (error == nil);
}

-(NSString *)getFileContents:(NSString*)filename
{
    NSError *error;
    NSURL *url = [[NSURL alloc] initFileURLWithPath:filename];
    NSString *content = [[NSString alloc] initWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&error];
    if (error != nil)
        return nil;
    
    return content;
}

-(BOOL)writeToFileWithData:(NSData *)data fileName:(NSString *)fileName
{
    //NSURL *url = [[NSURL alloc] initFileURLWithPath:fileName];
    NSError *error;
    BOOL ok = [data  writeToFile:fileName options:NSDataWritingFileProtectionNone error:&error];
    //    BOOL ok = [data writeToFile:url atomically:YES encoding:NSUTF8StringEncoding error:&error];
    if (!ok)
    {
        // an error occurred
        NSLog(@"Error writing file at %@\n%@",
              fileName, error);
    }
    return ok;
}

-(BOOL)writeToFileWithFileName:(NSString *)fileName content:(NSString *)content
{
    NSURL *url = [[NSURL alloc] initFileURLWithPath:fileName];
    NSError *error;
    BOOL ok = [content writeToURL:url atomically:YES encoding:NSUTF8StringEncoding error:&error];
    if (!ok)
    {
        // an error occurred
        NSLog(@"Error writing file at %@\n%@",
              fileName, error);
    }
    return ok;
}


-(void)prepareFileSystem
{
    
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    BOOL isDir;
    
    
    // Check data directory
    BOOL dataOK = [fileManager fileExistsAtPath:dataDirectory isDirectory:&isDir];
    if (!dataOK) {
        NSError *error;
        if (![[NSFileManager defaultManager] createDirectoryAtPath:dataDirectory
                                       withIntermediateDirectories:NO
                                                        attributes:nil
                                                             error:&error])
        {
            NSLog(@"Create data directory error: %@", error);
        }
    }
    
    // Create documents directory
    BOOL docuemntOK = [fileManager fileExistsAtPath:documentsDirectory isDirectory:&isDir];
    if (!docuemntOK) {
        NSError *error;
        if (![[NSFileManager defaultManager] createDirectoryAtPath:documentsDirectory
                                       withIntermediateDirectories:NO
                                                        attributes:nil
                                                             error:&error])
        {
            NSLog(@"Create documents directory error: %@", error);
        }
    }
    
    
    // Create annotation directory
    BOOL annotationOK = [fileManager fileExistsAtPath:annotationDirectory isDirectory:&isDir];
    if (!annotationOK) {
        NSError *error;
        if (![[NSFileManager defaultManager] createDirectoryAtPath:annotationDirectory
                                       withIntermediateDirectories:NO
                                                        attributes:nil
                                                             error:&error])
        {
            NSLog(@"Create annotation directory error: %@", error);
        }
    }
    
    // Create images directory
    BOOL imagesOK = [fileManager fileExistsAtPath:imagesDirectory isDirectory:&isDir];
    if (!imagesOK) {
        NSError *error;
        if (![[NSFileManager defaultManager] createDirectoryAtPath:imagesDirectory
                                       withIntermediateDirectories:NO
                                                        attributes:nil
                                                             error:&error])
        {
            NSLog(@"Create images directory error: %@", error);
        }
    }
    
}


-(BOOL)fileNameExisits:(NSString *)fileName
{
    BOOL found = [[NSFileManager defaultManager] fileExistsAtPath:fileName];
    return found;
}


+(NSURL *)pathForDocumentsFile:(NSString *)filename {
    
    // create a URL-based path to the passed in filename located in the Documents directory
    NSFileManager *fm = [NSFileManager defaultManager];
    NSArray *directories = [fm URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
    NSURL *documentPath = [directories objectAtIndex:0];
    
    return [documentPath URLByAppendingPathComponent:filename];
    
}

@end
