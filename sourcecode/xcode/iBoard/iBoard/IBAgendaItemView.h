//
//  IBAgendaItemView.h
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/05/30.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IBAgendaItemView : UIView

@property (strong, nonatomic) IBOutlet UIView *view;

@property (weak, nonatomic) IBOutlet UILabel *title;

@property (weak, nonatomic) IBOutlet UILabel *pageTitle;

-(void)setTitle:(NSString*)title pageTitle:(NSString*)pageTitle;

@end
