//
//  IBDrawingSurfaceView.h
//  DrawPad
//
//  Created by Tshepo Mgaga on 2015/06/16.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "IBAnnotationCommentView.h"

@interface IBDrawingSurfaceView : UIView <IBAnnotationCommentViewDelegate>


- (instancetype)initWithFrame:(CGRect)frame page:(NSUInteger)page documentId:(NSString*)documentId canCallServer:(BOOL)canCallServer;


@property (nonatomic, readwrite) NSString *documentId;
@property (nonatomic, readwrite) NSUInteger pageNumber;

@end


