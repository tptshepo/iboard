//
//  DocumentPackData.h
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/06/28.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DocumentPackData : NSObject

@property (nonatomic, retain) NSArray *documentPacks;
@property (nonatomic, retain) NSArray *meetings;

@end
