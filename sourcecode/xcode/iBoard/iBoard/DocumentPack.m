//
//  DocumentPack.m
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/06/28.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import "DocumentPack.h"
#import "Company.h"
#import "DocumentPackDocument.h"


@implementation DocumentPack

@dynamic documentPackID;
@dynamic packType;
@dynamic packName;
@dynamic packDescription;
@dynamic coverNote;
@dynamic coverStyleID;
@dynamic allowEmail;
@dynamic allowPrint;
@dynamic isDeletedState;
@dynamic userID;
@dynamic documentPackDocuments;
@dynamic company;

@end
