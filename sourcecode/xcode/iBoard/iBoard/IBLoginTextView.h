//
//  IBLoginTextView.h
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/04/02.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(int, TEXT_MODE_TYPE)
{
    TEXT_MODE_TEXT,
    TEXT_MODE_PASSWORD
};

@interface IBLoginTextView : UIView

@property (weak, nonatomic) IBOutlet UITextField *textView;
@property (weak, nonatomic) IBOutlet UIImageView *textImageView;
@property (strong, nonatomic) IBOutlet UIView *view;

@property (nonatomic) NSString *getText;

-(void)setKeyboardType:(UIKeyboardType)type;

-(void)setText:(NSString*)text;
-(void)setTextMode:(TEXT_MODE_TYPE) mode;


@end
