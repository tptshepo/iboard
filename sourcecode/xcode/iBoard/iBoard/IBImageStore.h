//
//  IBImageStore.h
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/05/07.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface IBImageStore : NSObject

+ (instancetype)sharedInstance;

/**
 * Return the UIImage from the given request. If a previous response is in
 * the cache, return that image, otherwise return the placeholder and issue
 * the request. The callback block is invoked after the network request
 * has completed.
 */
- (UIImage *)imageForURLRequest:(NSURLRequest *)request
                    placeholder:(UIImage *)placeholderImage
                    whenFetched:(void(^)(NSURLRequest *request, UIImage *image))callback;

@end
