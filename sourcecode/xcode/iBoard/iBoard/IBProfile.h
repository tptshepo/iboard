//
//  IBProfile.h
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/04/11.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IBSerializable.h"


@interface IBProfile : NSObject <IBSerializable>


@property (nonatomic, copy, readonly) NSString *firstName;
@property (nonatomic, copy, readonly) NSString *lastName;
@property (nonatomic, copy, readonly) NSString *serverExpiryDate;
@property (nonatomic, copy, readonly) NSString *userID;
@property (nonatomic, copy) NSString *APIKey;
@property (nonatomic, copy, readonly) NSString *imageIDThumbnail;

/**
 * Create a new instance
 */
- (instancetype)initWithFirstName:(NSString *)firstName
                         lastName:(NSString *)lastName
                 serverExpiryDate:(NSString *)serverExpirtDate
                           userID:(NSString *)userID
                           APIKey:(NSString *)APIKey
                 ImageIDThumbnail:(NSString *)ImageIDThumbnail;

/**
 * Create a new instance from a dictionary 
 */
- (instancetype)initWithDictionary:(NSDictionary *)dict;

/**
 * Returns a dictionary representation of the model suitable
 * for JSON serialization
 */
- (NSDictionary *)dictionaryRepresentation;

@end
