//
//  IBMeetingAgendaItem.h
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/04/28.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "IBSerializable.h"


@interface IBMeetingAgendaItem : NSObject <IBSerializable>

@property (nonatomic, copy, readonly) NSString *meetingAgendaItemID;
@property (nonatomic, copy, readonly) NSString *itemNumber;
@property (nonatomic, copy, readonly) NSString *itemName;
@property (nonatomic, copy, readonly) NSString *itemOrder;
@property (nonatomic, copy, readonly) NSString *isDeleted;
@property (nonatomic, copy, readonly) NSArray *documentList;

/**
 * Initialize a new instance
 */
- (instancetype)initWithMeetingAgendaItemID:(NSString *)meetingAgendaItemID
                                 itemNumber:(NSString *)itemNumber
                                   itemName:(NSString *)itemName
                                  itemOrder:(NSString *)itemOrder
                                  isDeleted:(NSString *)isDeleted
                               documentList:(NSArray *)documentList
;


/**
 * Create a new instance from a dictionary
 */
- (instancetype)initWithDictionary:(NSDictionary *)dict;

/**
 * Returns a dictionary representation of the model suitable
 * for JSON serialization
 */
- (NSDictionary *)dictionaryRepresentation;


@end