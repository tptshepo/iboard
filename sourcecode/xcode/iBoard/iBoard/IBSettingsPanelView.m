//
//  IBSettingsPanelView.m
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/06/05.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import "IBSettingsPanelView.h"

@implementation IBSettingsPanelView
{
    CGPoint lastPoint;
    CGFloat red;
    CGFloat green;
    CGFloat blue;
    CGFloat brush;
    CGFloat opacity;
}

@synthesize delegate = _delegate;

#pragma mark - View Methods


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        [[NSBundle mainBundle] loadNibNamed:@"IBSettingsPanelView" owner:self options:nil];
        
        [self addSubview:self.view];
        
        [self viewDidLoad];
        
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [[NSBundle mainBundle] loadNibNamed:@"IBSettingsPanelView" owner:self options:nil];
        
        self.bounds = self.view.bounds;
        
        [self addSubview:self.view];
        
        [self viewDidLoad];
        
    }
    return self;
}


- (void)viewDidLoad
{

    [self.view setBackgroundColor:[UIColor clearColor]];
}


- (IBAction)tapPenColor:(id)sender {
    
    UIButton * PressedButton = (UIButton*)sender;
    
    switch(PressedButton.tag)
    {
        case 1:
            red = 3.0/255.0;
            green = 7.0/255.0;
            blue = 7.0/255.0;
            break;
        case 2:
            red = 55.0/255.0;
            green = 107.0/255.0;
            blue = 120.0/255.0;
            break;
        case 3:
            red = 60.0/255.0;
            green = 29.0/255.0;
            blue = 122.0/255.0;
            break;
        case 4:
            red = 85.0/255.0;
            green = 165.0/255.0;
            blue = 53.0/255.0;
            break;
        case 5:
            red = 130.0/255.0;
            green = 13.0/255.0;
            blue = 22.0/255.0;
            break;
        case 6:
            red = 235.0/255.0;
            green = 73.0/255.0;
            blue = 27.0/255.0;
            break;
    }
    
    [self.delegate onPenColorChange:red green:green blue:blue];
}

- (IBAction)taphighlighterColor:(id)sender {
    
    UIButton * PressedButton = (UIButton*)sender;
    
    switch(PressedButton.tag)
    {
        case 1:
            red = 3.0/255.0;
            green = 7.0/255.0;
            blue = 7.0/255.0;
            break;
        case 2:
            red = 55.0/255.0;
            green = 107.0/255.0;
            blue = 120.0/255.0;
            break;
        case 3:
            red = 60.0/255.0;
            green = 29.0/255.0;
            blue = 122.0/255.0;
            break;
        case 4:
            red = 85.0/255.0;
            green = 165.0/255.0;
            blue = 53.0/255.0;
            break;
        case 5:
            red = 130.0/255.0;
            green = 13.0/255.0;
            blue = 22.0/255.0;
            break;
        case 6:
            red = 235.0/255.0;
            green = 73.0/255.0;
            blue = 27.0/255.0;
            break;
    }
    
    [self.delegate onHighlighterColorChange:red green:green blue:blue];
    
}
@end
