//
//  IBCompanyMeeting.h
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/04/28.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "IBSerializable.h"


@interface IBCompanyMeeting : NSObject <IBSerializable>

@property (nonatomic, copy, readonly) NSString *companyMeetingID;
@property (nonatomic, copy, readonly) NSString *startDateTime;
@property (nonatomic, copy, readonly) NSString *endDateTime;
@property (nonatomic, copy, readonly) NSString *location;
@property (nonatomic, copy, readonly) NSString *apologies;
@property (nonatomic, copy, readonly) NSString *showAgendaItemNumberInd;
@property (nonatomic, copy, readonly) NSString *packType;
@property (nonatomic, copy, readonly) NSArray *meetingAgendaItemList;
@property (nonatomic, copy, readonly) NSString *documentPackID;
@property (nonatomic, copy, readonly) NSString *packName;
@property (nonatomic, copy, readonly) NSString *packDescription;
@property (nonatomic, copy, readonly) NSString *coverNote;
@property (nonatomic) NSString *coverStyleID;
@property (nonatomic, copy, readonly) NSString *allowEmail;
@property (nonatomic, copy, readonly) NSString *allowPrint;
@property (nonatomic, copy, readonly) NSString *isDeleted;

/**
 * Initialize a new instance
 */
- (instancetype)initWithCompanyMeetingID:(NSString *)companyMeetingID
                           startDateTime:(NSString *)startDateTime
                             endDateTime:(NSString *)endDateTime
                                location:(NSString *)location
                               apologies:(NSString *)apologies
                 showAgendaItemNumberInd:(NSString *)showAgendaItemNumberInd
                                packType:(NSString *)packType
                   meetingAgendaItemList:(NSArray *)meetingAgendaItemList
                          documentPackID:(NSString *)documentPackID
                                packName:(NSString *)packName
                         packDescription:(NSString *)packDescription
                               coverNote:(NSString *)coverNote
                            coverStyleID:(NSString *)coverStyleID
                              allowEmail:(NSString *)allowEmail
                              allowPrint:(NSString *)allowPrint
                               isDeleted:(NSString *)isDeleted;


/**
 * Create a new instance from a dictionary
 */
- (instancetype)initWithDictionary:(NSDictionary *)dict;

/**
 * Returns a dictionary representation of the model suitable
 * for JSON serialization
 */
- (NSDictionary *)dictionaryRepresentation;


@end