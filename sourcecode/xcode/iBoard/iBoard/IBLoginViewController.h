//
//  ViewController.h
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/03/31.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IBLoginTextView.h"
#import "IBIndicatorLabel.h"

@protocol IBLoginViewControllerDelegate;

@interface IBLoginViewController : UIViewController

@property (weak, nonatomic) IBOutlet IBLoginTextView *userNameView;
@property (weak, nonatomic) IBOutlet IBLoginTextView *passwordView;
@property (nonatomic, weak) id<IBLoginViewControllerDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIView *waitContainer;

@property (weak, nonatomic) IBOutlet UIButton *loginButtonView;


- (IBAction)buttonRemeber:(id)sender;
- (IBAction)loginButton:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *rememberCheckImgae;


- (IBAction)tapFAQ:(id)sender;


@end


@protocol IBLoginViewControllerDelegate

- (void)authenticationViewControllerSucceeded:(IBLoginViewController *)authVC;

@end