//
//  IBAnnotationJSONData.h
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/04/28.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "IBSerializable.h"


@interface IBAnnotationJSONData : NSObject <IBSerializable>

@property (nonatomic, copy, readonly) NSString *graphics;
@property (nonatomic, copy, readonly) NSArray *comments;

/**
 * Initialize a new instance
 */
- (instancetype)initWithGraphics:(NSString *)graphics
                        comments:(NSArray *)comments;


/**
 * Create a new instance from a dictionary
 */
- (instancetype)initWithDictionary:(NSDictionary *)dict;

/**
 * Returns a dictionary representation of the model suitable
 * for JSON serialization
 */
- (NSDictionary *)dictionaryRepresentation;


@end