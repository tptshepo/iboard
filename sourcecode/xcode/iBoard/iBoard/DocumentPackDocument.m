//
//  DocumentPackDocument.m
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/06/28.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import "DocumentPackDocument.h"
#import "DocumentPack.h"


@implementation DocumentPackDocument

@dynamic documentPackDocumentID;
@dynamic documentID;
@dynamic documentOrder;
@dynamic documentName;
@dynamic canEmail;
@dynamic canPrint;
@dynamic fileName;
@dynamic isDeletedState;
@dynamic documentPack;

@synthesize coverStyleID = _coverStyleID;

@end
