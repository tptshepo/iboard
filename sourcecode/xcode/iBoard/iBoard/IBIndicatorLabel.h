
#import <UIKit/UIKit.h>

@interface IBIndicatorLabel : UIView

/**
 Returns the label used for the main textual content of the view. (read-only)
 */
@property (nonatomic, retain, readonly) UILabel *textLabel;

/**
 A view that indicates loading activity to the user. (read-only)
 */
@property (nonatomic, retain, readonly) UIActivityIndicatorView *activityIndicatorView;

/**
 A Boolean value that determines whether the view is loading.
 
 The default value is `NO`.
 */
@property (nonatomic, assign, getter=isLoading) BOOL loading;

/**
 Starts loading and updates the text of the text label.
 
 @param text String to update the `textLabel`'s `text` property to.
 */
- (void)startWithText:(NSString *)text;

/**
 Completes loading and updates the text of the text label.
 
 @param text String to update the `textLabel`'s `text` property to.
 */
- (void)completeWithText:(NSString *)text;

@end
