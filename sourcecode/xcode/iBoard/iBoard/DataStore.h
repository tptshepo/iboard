//
//  DataStore.h
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/06/27.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface DataStore : NSObject {
    NSManagedObjectModel *model;
}

@property (nonatomic, strong) NSManagedObjectContext *context;

+ (id)sharedDataStore;
- (void) dataSoreClear;
- (BOOL)saveChanges;

@end
