//
//  IBCompany.m
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/04/13.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import "IBCompany.h"
#import "IBCommonKey.h"


@interface IBCompany ()

@property (nonatomic, copy, readwrite) NSString *companyID;
@property (nonatomic, copy, readwrite) NSString *companyName;
@property (nonatomic, copy, readwrite) NSString *requireOnlineSignInEveryXDays;
@property (nonatomic, copy, readwrite) NSString *reLoginInactivityMinutes;
@property (nonatomic, copy, readwrite) NSString *allowEmail;
@property (nonatomic, copy, readwrite) NSString *allowPrint;
@property (nonatomic, copy, readwrite) NSString *companyUserLevelID;
@property (nonatomic, copy, readwrite) NSString *isDeleted;
@property (nonatomic, copy, readwrite) NSString *imageIDLogo;



@end

@implementation IBCompany

-(instancetype)initWithCompanyID:(NSString *)companyID
                     companyName:(NSString *)companyName
   requireOnlineSignInEveryXDays:(NSString *)requireOnlineSignInEveryXDays
        reLoginInactivityMinutes:(NSString *)reLoginInactivityMinutes
                      allowEmail:(NSString *)allowEmail
                      allowPrint:(NSString *)allowPrint
              companyUserLevelID:(NSString *)companyUserLevelID
                       isDeleted:(NSString *)isDeleted
                     imageIDLogo:(NSString *)imageIDLogo
{
    
    if ((self = [super init])) {
        self.companyID = companyID;
        self.companyName = companyName;
        self.requireOnlineSignInEveryXDays = requireOnlineSignInEveryXDays;
        self.reLoginInactivityMinutes = reLoginInactivityMinutes;
        self.allowEmail = allowEmail;
        self.allowPrint = allowPrint;
        self.companyUserLevelID = companyUserLevelID;
        self.isDeleted = isDeleted;
        self.imageIDLogo = imageIDLogo;
    }
    
    return self;
    
    
}


- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    return [self initWithCompanyID:dict[IBCompanyIDKey]
                       companyName:dict[IBCompanyNameKey]
     requireOnlineSignInEveryXDays:dict[IBRequireOnlineSignInEveryXDaysKey]
          reLoginInactivityMinutes:dict[IBReLoginInactivityMinutesKey]
                        allowEmail:dict[IBAllowEmailKey]
                        allowPrint:dict[IBAllowPrintKey]
                companyUserLevelID:dict[IBCompanyUserLevelIDKey]
                         isDeleted:dict[IBIsDeletedKey]
                       imageIDLogo:dict[IBImageIDLogoKey]
            ];
}

- (NSDictionary *)dictionaryRepresentation
{
    return @{
             IBCompanyIDKey: self.companyID,
             IBCompanyNameKey: self.companyName,
             IBRequireOnlineSignInEveryXDaysKey: self.requireOnlineSignInEveryXDays,
             IBReLoginInactivityMinutesKey: self.reLoginInactivityMinutes,
             IBAllowEmailKey: self.allowEmail,
             IBAllowPrintKey: self.allowPrint,
             IBCompanyUserLevelIDKey: self.companyUserLevelID,
             IBIsDeletedKey: self.isDeleted,
             IBImageIDLogoKey: self.imageIDLogo
             };
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"<%@: 0x%x CompanyID=%@ CompanyName=%@ RequireOnlineSignInEveryXDays=%@ ReLoginInactivityMinutes=%@ AllowEmail=%@ AllowPrint=%@ CompanyUserLevelID=%@ IsDeleted=%@ ImageIDLogo=%@>",
            NSStringFromClass([self class]),
            (unsigned int)self,
            self.companyID,
            self.companyName,
            self.requireOnlineSignInEveryXDays,
            self.reLoginInactivityMinutes,
            self.allowEmail,
            self.allowPrint,
            self.companyUserLevelID,
            self.isDeleted,
            self.imageIDLogo
            ];
}


@end
