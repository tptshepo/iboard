//
//  IBMeetingAgendaItem.m
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/04/28.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import "IBMeetingAgendaItem.h"
#import "IBCommonKey.h"
#import "NSArray+Enumerable.h"
#import "IBDocumentPackDocument.h"

@interface IBMeetingAgendaItem ()

@property (nonatomic, copy, readwrite) NSString *meetingAgendaItemID;
@property (nonatomic, copy, readwrite) NSString *itemNumber;
@property (nonatomic, copy, readwrite) NSString *itemName;
@property (nonatomic, copy, readwrite) NSString *itemOrder;
@property (nonatomic, copy, readwrite) NSString *isDeleted;
@property (nonatomic, copy, readwrite) NSArray *documentList;

@end


@implementation IBMeetingAgendaItem


-(instancetype)initWithMeetingAgendaItemID:(NSString *)meetingAgendaItemID
                                itemNumber:(NSString *)itemNumber
                                  itemName:(NSString *)itemName
                                 itemOrder:(NSString *)itemOrder
                                 isDeleted:(NSString *)isDeleted
                              documentList:(NSArray *)documentList
{
    
    
    if ((self = [super init])) {
        
        self.meetingAgendaItemID = meetingAgendaItemID;
        self.itemNumber = itemNumber;
        self.itemName = itemName;
        self.itemOrder = itemOrder;
        self.isDeleted = isDeleted;
        self.documentList = documentList;
        
    }
    
    return self;
    
    
}


- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    
    NSArray *documentListFromDict = [dict[IBDocumentListKey] mappedArrayWithBlock:^id(id obj) {
        return [[IBDocumentPackDocument alloc] initWithDictionary:obj];
    }];
    
    return [self initWithMeetingAgendaItemID:dict[IBMeetingAgendaItemIDKey]
                                  itemNumber:dict[IBItemNumberKey]
                                    itemName:dict[IBItemNameKey]
                                   itemOrder:dict[IBItemOrderKey]
                                   isDeleted:dict[IBIsDeletedKey]
                                documentList:documentListFromDict];
    
}

- (NSDictionary *)dictionaryRepresentation
{
    
    NSArray *documentListFromDict = [self.documentList mappedArrayWithBlock:^id(id obj) {
        return [[IBDocumentPackDocument alloc] initWithDictionary:obj];
    }];
    
    return @{
             IBMeetingAgendaItemIDKey: self.meetingAgendaItemID,
             IBItemNumberKey: self.itemNumber,
             IBItemNameKey: self.itemName,
             IBItemOrderKey: self.itemOrder,
             IBIsDeletedKey: self.isDeleted,
             IBDocumentListKey: documentListFromDict
             };
}

- (NSString *)description
{
    
    return [NSString stringWithFormat:@"<%@: 0x%x \
            MeetingAgendaItemID=%@ \
            ItemNumber=%@ \
            ItemName=%@ \
            ItemOrder=%@ \
            IsDeleted=%@ \
            ",
            NSStringFromClass([self class]),
            (unsigned int)self,
            self.meetingAgendaItemID,
            self.itemNumber,
            self.itemName,
            self.itemOrder,
            self.isDeleted
            ];
}


@end
