//
//  Document.h
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/06/28.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Agenda;

@interface Document : NSManagedObject

@property (nonatomic, retain) NSNumber * documentPackDocumentID;
@property (nonatomic, retain) NSNumber * documentID;
@property (nonatomic, retain) NSNumber * documentOrder;
@property (nonatomic, retain) NSString * documentName;
@property (nonatomic, retain) NSNumber * canEmail;
@property (nonatomic, retain) NSNumber * canPrint;
@property (nonatomic, retain) NSString * fileName;
@property (nonatomic, retain) NSNumber * isDeletedState;
@property (nonatomic, retain) Agenda *agenda;

@end
