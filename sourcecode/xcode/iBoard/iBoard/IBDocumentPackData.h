//
//  IBDocumentPackData.h
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/04/28.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "IBSerializable.h"


/**
 * The dictionary keys
 */


@interface IBDocumentPackData : NSObject <IBSerializable>

@property (nonatomic, copy, readonly) NSArray *documentPackList;
@property (nonatomic, copy, readonly) NSArray *meetingList;
@property (nonatomic, copy, readonly) NSString *serverTimestamp;

/**
 * Initialize a new instance
 */
- (instancetype)initWithDocumentPackList:(NSArray *)documentPackList
                             meetingList:(NSArray *)meetingList
                         serverTimestamp:(NSString *)serverTimestamp;


/**
 * Create a new instance from a dictionary
 */
- (instancetype)initWithDictionary:(NSDictionary *)dict;

- (instancetype)initWithDictionary:(NSArray *)nsArrayDocumentPackList
                nsArrayMeetingList:(NSArray *)nsArrayMeetingList
                   serverTimestamp:(NSString *)serverTimestamp;

/**
 * Returns a dictionary representation of the model suitable
 * for JSON serialization
 */
- (NSDictionary *)dictionaryRepresentation;


@end