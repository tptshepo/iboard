//
//  IBCompanyMeeting.m
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/04/28.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import "IBCompanyMeeting.h"
#import "IBCommonKey.h"
#import "IBMeetingAgendaItem.h"
#import "NSArray+Enumerable.h"


@interface IBCompanyMeeting ()

@property (nonatomic, copy, readwrite) NSString *companyMeetingID;
@property (nonatomic, copy, readwrite) NSString *startDateTime;
@property (nonatomic, copy, readwrite) NSString *endDateTime;
@property (nonatomic, copy, readwrite) NSString *location;
@property (nonatomic, copy, readwrite) NSString *apologies;
@property (nonatomic, copy, readwrite) NSString *showAgendaItemNumberInd;
@property (nonatomic, copy, readwrite) NSString *packType;
@property (nonatomic, copy, readwrite) NSArray *meetingAgendaItemList;
@property (nonatomic, copy, readwrite) NSString *documentPackID;
@property (nonatomic, copy, readwrite) NSString *packName;
@property (nonatomic, copy, readwrite) NSString *packDescription;
@property (nonatomic, copy, readwrite) NSString *coverNote;
//@property (nonatomic, copy, readwrite) NSString *coverStyleID;
@property (nonatomic, copy, readwrite) NSString *allowEmail;
@property (nonatomic, copy, readwrite) NSString *allowPrint;
@property (nonatomic, copy, readwrite) NSString *isDeleted;

@end


@implementation IBCompanyMeeting

@synthesize coverStyleID = _coverStyleID;

-(instancetype)initWithCompanyMeetingID:(NSString *)companyMeetingID
                          startDateTime:(NSString *)startDateTime
                            endDateTime:(NSString *)endDateTime
                               location:(NSString *)location
                              apologies:(NSString *)apologies
                showAgendaItemNumberInd:(NSString *)showAgendaItemNumberInd
                               packType:(NSString *)packType
                  meetingAgendaItemList:(NSArray *)meetingAgendaItemList
                         documentPackID:(NSString *)documentPackID
                               packName:(NSString *)packName
                        packDescription:(NSString *)packDescription
                              coverNote:(NSString *)coverNote
                           coverStyleID:(NSString *)coverStyleID
                             allowEmail:(NSString *)allowEmail
                             allowPrint:(NSString *)allowPrint
                              isDeleted:(NSString *)isDeleted
{
    
    
    if ((self = [super init])) {
        
        self.companyMeetingID = companyMeetingID;
        self.startDateTime = startDateTime;
        self.endDateTime = endDateTime;
        self.location = location;
        self.apologies = apologies;
        self.showAgendaItemNumberInd = showAgendaItemNumberInd;
        self.packType = packType;
        self.meetingAgendaItemList = meetingAgendaItemList;
        self.documentPackID = documentPackID;
        self.packName = packName;
        self.packDescription = packDescription;
        self.coverNote = coverNote;
        self.coverStyleID = coverStyleID;
        self.allowEmail = allowEmail;
        self.allowPrint = allowPrint;
        self.isDeleted = isDeleted;
        
    }
    
    return self;
    
}


- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    
    NSArray *documentListFromDict = [dict[IBMeetingAgendaItemListKey] mappedArrayWithBlock:^id(id obj) {
        return [[IBMeetingAgendaItem alloc] initWithDictionary:obj];
    }];
    
    return [self initWithCompanyMeetingID:dict[IBCompanyMeetingIDKey]
                            startDateTime:dict[IBStartDateTimeKey]
                              endDateTime:dict[IBEndDateTimeKey]
                                 location:dict[IBLocationKey]
                                apologies:dict[IBApologiesKey]
                  showAgendaItemNumberInd:dict[IBShowAgendaItemNumberIndKey]
                                 packType:dict[IBPackTypeKey]
                    meetingAgendaItemList:documentListFromDict
                           documentPackID:dict[IBDocumentPackIDKey]
                                 packName:dict[IBPackNameKey]
                          packDescription:dict[IBPackDescriptionKey]
                                coverNote:dict[IBCoverNoteKey]
                             coverStyleID:dict[IBCoverStyleIDKey]
                               allowEmail:dict[IBAllowEmailKey]
                               allowPrint:dict[IBAllowPrintKey]
                                isDeleted:dict[IBIsDeletedKey]];
    
}

- (NSDictionary *)dictionaryRepresentation
{
    
    NSArray *meetingAgendaItemListFromDict = [self.meetingAgendaItemList mappedArrayWithBlock:^id(id obj) {
        return [[IBMeetingAgendaItem alloc] initWithDictionary:obj];
    }];
    
    return @{
             IBCompanyMeetingIDKey: self.companyMeetingID,
             IBStartDateTimeKey: self.startDateTime,
             IBEndDateTimeKey: self.endDateTime,
             IBLocationKey: self.location,
             IBApologiesKey: self.apologies,
             IBShowAgendaItemNumberIndKey: self.showAgendaItemNumberInd,
             IBPackTypeKey: self.packType,
             IBMeetingAgendaItemListKey: meetingAgendaItemListFromDict,
             IBDocumentPackIDKey: self.documentPackID,
             IBPackNameKey: self.packName,
             IBPackDescriptionKey: self.packDescription,
             IBCoverNoteKey: self.coverNote,
             IBCoverStyleIDKey: self.coverStyleID,
             IBAllowEmailKey: self.allowEmail,
             IBAllowPrintKey: self.allowPrint,
             IBIsDeletedKey: self.isDeleted
             };
}

- (NSString *)description
{
    
    return [NSString stringWithFormat:@"<%@: 0x%x \
            CompanyMeetingID=%@ \
            StartDateTime=%@ \
            EndDateTime=%@ \
            Location=%@ \
            Apologies=%@ \
            ShowAgendaItemNumberInd=%@ \
            PackType=%@ \
            DocumentPackID=%@ \
            PackName=%@ \
            PackDescription=%@ \
            CoverNote=%@ \
            CoverStyleID=%@ \
            AllowEmail=%@ \
            AllowPrint=%@ \
            IsDeleted=%@ \
            ",
            NSStringFromClass([self class]),
            (unsigned int)self,
            self.companyMeetingID,
            self.startDateTime,
            self.endDateTime,
            self.location,
            self.apologies,
            self.showAgendaItemNumberInd,
            self.packType,
            self.documentPackID,
            self.packName,
            self.packDescription,
            self.coverNote,
            self.coverStyleID,
            self.allowEmail,
            self.allowPrint,
            self.isDeleted
            ];
}


@end
