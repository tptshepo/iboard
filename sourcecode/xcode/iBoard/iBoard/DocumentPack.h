//
//  DocumentPack.h
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/06/28.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Company, DocumentPackDocument;

@interface DocumentPack : NSManagedObject

@property (nonatomic, retain) NSNumber * documentPackID;
@property (nonatomic, retain) NSNumber * packType;
@property (nonatomic, retain) NSString * packName;
@property (nonatomic, retain) NSString * packDescription;
@property (nonatomic, retain) NSString * coverNote;
@property (nonatomic, retain) NSNumber * coverStyleID;
@property (nonatomic, retain) NSNumber * allowEmail;
@property (nonatomic, retain) NSNumber * allowPrint;
@property (nonatomic, retain) NSNumber * isDeletedState;
@property (nonatomic, retain) NSNumber * userID;
@property (nonatomic, retain) NSSet *documentPackDocuments;
@property (nonatomic, retain) Company *company;
@end

@interface DocumentPack (CoreDataGeneratedAccessors)

- (void)addDocumentPackDocumentsObject:(DocumentPackDocument *)value;
- (void)removeDocumentPackDocumentsObject:(DocumentPackDocument *)value;
- (void)addDocumentPackDocuments:(NSSet *)values;
- (void)removeDocumentPackDocuments:(NSSet *)values;

@end
