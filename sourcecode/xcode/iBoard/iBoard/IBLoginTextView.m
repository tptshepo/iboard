//
//  IBLoginTextView.m
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/04/02.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import "IBLoginTextView.h"

const int IBTEXTVIEW_MODE_TEXT = 0;
const int IBTEXTVIEW_MODE_PASSWORD = 1;

@implementation IBLoginTextView {
    TEXT_MODE_TYPE _textMode;
}


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        [[NSBundle mainBundle] loadNibNamed:@"IBLoginTextView" owner:self options:nil];
        
        [self.view setBackgroundColor:[UIColor clearColor]];
        [self addSubview:self.view];
        
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [[NSBundle mainBundle] loadNibNamed:@"IBLoginTextView" owner:self options:nil];
        [self.view setBackgroundColor:[UIColor clearColor]];
        
        self.bounds = self.view.bounds;
        
        [self addSubview:self.view];
        
    }
    return self;
}

/*
 UIKeyboardTypeDefault,                // Default type for the current input method.
 UIKeyboardTypeASCIICapable,           // Displays a keyboard which can enter ASCII characters, non-ASCII keyboards remain active
 UIKeyboardTypeNumbersAndPunctuation,  // Numbers and assorted punctuation.
 UIKeyboardTypeURL,                    // A type optimized for URL entry (shows . / .com prominently).
 UIKeyboardTypeNumberPad,              // A number pad (0-9). Suitable for PIN entry.
 UIKeyboardTypePhonePad,               // A phone pad (1-9, *, 0, #, with letters under the numbers).
 UIKeyboardTypeNamePhonePad,           // A type optimized for entering a person's name or phone number.
 UIKeyboardTypeEmailAddress,           // A type optimized for multiple email address entry (shows space @ . prominently).
 UIKeyboardTypeDecimalPad,             // A number pad including a decimal point
 UIKeyboardTypeTwitter,                // Optimized for entering Twitter messages (shows # and @)
 UIKeyboardTypeWebSearch,              // Optimized for URL and search term entry (shows space and .)

 */
-(void)setKeyboardType:(UIKeyboardType)type
{
    [self.textView setKeyboardType:type];
}

-(void)setText:(NSString *)text
{
    [self.textView setText:text];
}


-(NSString*)getText{
    return [self.textView text];
}


-(void)setTextMode:(TEXT_MODE_TYPE)mode{
    _textMode = mode;
    
    if (mode == TEXT_MODE_TEXT){
        
        self.textView.secureTextEntry = NO;
        [self.textImageView setImage:[UIImage imageNamed: @"userimage.png"]];
        
        
    } else if (mode == TEXT_MODE_PASSWORD) {
        
        self.textView.secureTextEntry = YES;
        [self.textImageView setImage:[UIImage imageNamed: @"lockimage.png"]];
        
    } else {
        
    }
}


@end
