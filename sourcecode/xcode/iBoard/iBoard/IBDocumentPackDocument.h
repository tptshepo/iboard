//
//  IBDocumentPackDocument.h
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/04/28.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "IBSerializable.h"


@interface IBDocumentPackDocument : NSObject <IBSerializable>


@property (nonatomic, copy, readonly) NSString *documentPackDocumentID;
@property (nonatomic, copy, readonly) NSString *documentID;
@property (nonatomic, copy, readonly) NSString *documentOrder;
@property (nonatomic, copy, readonly) NSString *documentName;
@property (nonatomic, copy, readonly) NSString *canEmail;
@property (nonatomic, copy, readonly) NSString *canPrint;
@property (nonatomic, copy, readonly) NSString *fileName;
@property (nonatomic, copy, readonly) NSString *isDeleted;

@property (nonatomic) int coverStyleID;


/**
 * Initialize a new instance
 */
- (instancetype)initWithDocumentPackDocumentID:(NSString *)documentPackDocumentID
                                    documentID:(NSString *)documentID
                                 documentOrder:(NSString *)documentOrder
                                  documentName:(NSString *)documentName
                                      canEmail:(NSString *)canEmail
                                      canPrint:(NSString *)canPrint
                                      fileName:(NSString *)fileName
                                     isDeleted:(NSString *)isDeleted;

/**
 * Create a new instance from a dictionary
 */
- (instancetype)initWithDictionary:(NSDictionary *)dict;

/**
 * Returns a dictionary representation of the model suitable
 * for JSON serialization
 */
- (NSDictionary *)dictionaryRepresentation;


@end
