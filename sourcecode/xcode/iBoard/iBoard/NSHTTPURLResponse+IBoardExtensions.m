//
//  NSHTTPURLResponse+IBoardExtensions.m
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/04/11.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import "NSHTTPURLResponse+IBoardExtensions.h"

@implementation NSHTTPURLResponse (IBoardExtensions)

- (NSString *)errorMessageWithData:(NSData *)data
{
    NSString *message = [NSString stringWithFormat:@"Unexpected response code: %li", (long)self.statusCode];
    
    if (data) {
        NSError *jsonError = nil;
        id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
        if (json && [json isKindOfClass:[NSDictionary class]]) {
            NSString *errorMessage = [(NSDictionary *)json valueForKey:@"error"];
            if (errorMessage) {
                message = errorMessage;
            }
        }
    }
    
    return message;
}

@end
