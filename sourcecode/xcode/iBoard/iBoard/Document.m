//
//  Document.m
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/06/28.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import "Document.h"
#import "Agenda.h"


@implementation Document

@dynamic documentPackDocumentID;
@dynamic documentID;
@dynamic documentOrder;
@dynamic documentName;
@dynamic canEmail;
@dynamic canPrint;
@dynamic fileName;
@dynamic isDeletedState;
@dynamic agenda;

@end
