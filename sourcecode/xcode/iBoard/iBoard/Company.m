//
//  Company.m
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/06/28.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import "Company.h"
#import "DocumentPack.h"
#import "Meeting.h"


@implementation Company

@dynamic companyID;
@dynamic companyName;
@dynamic requireOnlineSignInEveryXDays;
@dynamic reLoginInactivityMinutes;
@dynamic allowEmail;
@dynamic allowPrint;
@dynamic companyUserLevelID;
@dynamic isDeletedState;
@dynamic imageIDLogo;
@dynamic userID;
@dynamic meetings;
@dynamic documentPacks;

@end
