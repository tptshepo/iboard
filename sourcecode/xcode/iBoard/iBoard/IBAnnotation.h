//
//  IBAnnotation.h
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/04/28.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "IBSerializable.h"

#import "IBAnnotationJSONData.h"

@interface IBAnnotation : NSObject <IBSerializable>

@property (nonatomic, assign, readonly) NSInteger documentAnnotationID;
@property (nonatomic, assign, readonly) NSInteger documentID;
@property (nonatomic, assign, readonly) NSInteger pageNo;
@property (nonatomic, retain, readonly) IBAnnotationJSONData *jSONData;

/**
 * Initialize a new instance
 */
- (instancetype)initWithDocumentAnnotationID:(NSInteger)documentAnnotationID
                                  documentID:(NSInteger)documentID
                                      pageNo:(NSInteger)pageNo
                                    jSONData:(IBAnnotationJSONData *)jSONData
;


/**
 * Create a new instance from a dictionary
 */
- (instancetype)initWithDictionary:(NSDictionary *)dict;

/**
 * Returns a dictionary representation of the model suitable
 * for JSON serialization
 */
- (NSDictionary *)dictionaryRepresentation;


@end