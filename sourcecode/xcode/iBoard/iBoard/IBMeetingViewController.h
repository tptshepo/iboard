//
//  IBMeetingViewController.h
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/04/19.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IBCompany.h"
#import "IBDocumentTab.h"
#import "IBMeetingTab.h"
#import "DocumentPackData.h"

#import "Company.h"

@interface IBMeetingViewController : UIViewController <IBDocumentTabDelegate, IBMeetingTabDelegate>

@property (nonatomic) Company *company;
@property (nonatomic) DocumentPackData *documentPacksData;

@property (weak, nonatomic) IBOutlet UIView *meetingView;

@property (weak, nonatomic) IBOutlet UILabel *viewTitle;

@property (weak, nonatomic) IBOutlet UIButton *segMeeting;
@property (weak, nonatomic) IBOutlet UIButton *segDocument;
@property (weak, nonatomic) IBOutlet UIButton *segArchive;

@property (weak, nonatomic) IBOutlet UIButton *infoButton;
@property (weak, nonatomic) IBOutlet UIButton *searchButton;
@property (weak, nonatomic) IBOutlet UIButton *reloadButton;

@property (weak, nonatomic) IBOutlet UIView *waitContainer;


- (IBAction)showMeetings:(id)sender;
- (IBAction)showDocuments:(id)sender;
- (IBAction)showArchives:(id)sender;

- (IBAction)backToProfile:(id)sender;
- (IBAction)showInfo:(id)sender;
- (IBAction)showSearch:(id)sender;
- (IBAction)reloadData:(id)sender;


@end
