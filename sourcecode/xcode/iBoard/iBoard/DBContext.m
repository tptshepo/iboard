//
//  DBContext.m
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/06/27.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import "DBContext.h"
#import "DataStore.h"
#import "Utils.h"
#import "IBFileManager.h"


//api
#import "IBCompany.h"
#import "IBDocumentPackData.h"
#import "IBDocumentPack.h"
#import "IBDocumentPackDocument.h"
#import "IBCompanyMeeting.h"
#import "IBMeetingAgendaItem.h"

//database
#import "Company.h"
#import "DocumentPackDocument.h"
#import "DocumentPack.h"
#import "Meeting.h"
#import "Agenda.h"
#import "Document.h"
#import "DocumentPackData.h"

@implementation DBContext


#pragma mark - Document Pack


+(void)addDocumentPack:(IBDocumentPackData*)ibDocumentPackData company:(Company*)company
{
    
    //add document packs
    for (IBDocumentPack *documentPack in ibDocumentPackData.documentPackList)
    {
        [DBContext addOrUpdateDocumentPack:documentPack company:company];
    }
    
    // add company meetings
    for (IBCompanyMeeting *ibCompanyMeeting in ibDocumentPackData.meetingList)
    {
        [DBContext addOrUpdateCompanyMeeting:ibCompanyMeeting company:company];
    }
    

    
}

+(DocumentPackData *)getDocumentPackData:(Company*)company
{
    DataStore *dataStore = [DataStore sharedDataStore];
    NSManagedObjectContext *context = [dataStore context];
    
    DocumentPackData *documentPackData = [[DocumentPackData alloc] init];

    documentPackData.documentPacks = [DBContext getDocumentPacks:company context:context];
    documentPackData.meetings = [DBContext getMeetings:company context:context];

    return documentPackData;
}

+(NSString*)getUserID
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *userID = [defaults stringForKey:@"UserIdentifier"];
    return userID;
}

+(NSArray *)getDocumentPacks:(Company*)company  context:(NSManagedObjectContext *)context
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"DocumentPack"];
    NSString *companyID = [company.companyID stringValue];
    NSString *userID = [DBContext getUserID];
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"(userID == %@) and (company.companyID == %@)",
                         [NSNumber numberWithInt:[userID intValue]],
                         [NSNumber numberWithInt:[companyID intValue]]];
    [request setPredicate:pred];
    
    NSError *error;
    NSArray *items = [context executeFetchRequest:request error:&error];
    
    if (!items) {
        NSLog(@"Error occurred while fetching. Error: %@", [error localizedDescription]);
    }
    
    return items;
}


+(NSArray *)getMeetings:(Company*)company context:(NSManagedObjectContext *)context
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Meeting"];
    
    NSString *companyID = [company.companyID stringValue];
    NSString *userID = [DBContext getUserID];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"(userID == %@) and (company.companyID == %@)",
                         [NSNumber numberWithInt:[userID intValue]],
                         [NSNumber numberWithInt:[companyID intValue]]];
    [request setPredicate:pred];
    
    
    NSError *error;
    NSArray *items = [context executeFetchRequest:request error:&error];
    
    if (!items) {
        NSLog(@"Error occurred while fetching. Error: %@", [error localizedDescription]);
    }
    
    //request = nil;
    
    return items;
}

+(void)addOrUpdateCompanyMeeting:(IBCompanyMeeting*)ibCompanyMeeting company:(Company*)company
{
    DataStore *dataStore = [DataStore sharedDataStore];
    NSManagedObjectContext *context = [dataStore context];
    
    // check if the DocumentPack object already exists in the database
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Meeting"];
    NSString *companyMeetingID = ibCompanyMeeting.companyMeetingID;
    NSString *companyID = [company.companyID stringValue];
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"(companyMeetingID == %@) and (userID == %@) and (company.companyID == %@)", companyMeetingID, [DBContext getUserID], companyID];
    
    [request setPredicate:pred];
    
    NSError *error;
    NSArray *items = [context executeFetchRequest:request error:&error];
    
    if (!items) {
        NSLog(@"Error occurred while fetching. Error: %@", [error localizedDescription]);
    }
    
    Meeting *meeting;
    
    BOOL isNew = !(items.count > 0);
    
    if (isNew)
    {
        NSLog(@"Adding new Meeting to database");
        meeting = [NSEntityDescription insertNewObjectForEntityForName:@"Meeting"
                                                inManagedObjectContext:context];
    }
    
    for (Meeting *meetingItem in items) {
        // found object
        meeting = meetingItem;
    }
    
    // update object
    [meeting setCompany:company];
    
    [meeting setUserID:[Utils stringToNSnumberIntFromDictionary:[DBContext getUserID]]];
    [meeting setCompanyMeetingID:[Utils stringToNSnumberIntFromDictionary:ibCompanyMeeting.companyMeetingID]];
    [meeting setStartDateTime:ibCompanyMeeting.startDateTime];
    [meeting setEndDateTime:ibCompanyMeeting.endDateTime];
    [meeting setLocation:ibCompanyMeeting.location];
    [meeting setApologies:ibCompanyMeeting.apologies];
    [meeting setShowAgendaItemNumberInd:[Utils stringToNSNumberBoolFromDictionary:ibCompanyMeeting.showAgendaItemNumberInd]];
    [meeting setPackType:[Utils stringToNSnumberIntFromDictionary:ibCompanyMeeting.packType]];
    [meeting setDocumentPackID:[Utils stringToNSnumberIntFromDictionary:ibCompanyMeeting.documentPackID]];
    [meeting setPackName:ibCompanyMeeting.packName];
    [meeting setPackDescription:ibCompanyMeeting.packDescription];
    [meeting setCoverNote:ibCompanyMeeting.coverNote];
    [meeting setCoverStyleID:[Utils stringToNSnumberIntFromDictionary:ibCompanyMeeting.coverStyleID]];
    [meeting setAllowEmail:[Utils stringToNSNumberBoolFromDictionary:ibCompanyMeeting.allowEmail]];
    [meeting setAllowPrint:[Utils stringToNSNumberBoolFromDictionary:ibCompanyMeeting.allowPrint]];
    [meeting setIsDeletedState:[Utils stringToNSNumberBoolFromDictionary:ibCompanyMeeting.isDeleted]];
    
    
    // clear agendas
    for (Agenda *agenda in meeting.agendas) {
        [context deleteObject:[context objectWithID:agenda.objectID]];
    }
    
    
    // add agenda items
    for (IBMeetingAgendaItem *ibMeetingAgendaItem in ibCompanyMeeting.meetingAgendaItemList) {
        
        if ([[Utils stringToNSNumberBoolFromDictionary:ibMeetingAgendaItem.isDeleted] boolValue])
        {
            //since the object is marked as deleted, don't add it to the database
            continue;
        }
        
        Agenda *agenda = [NSEntityDescription insertNewObjectForEntityForName:@"Agenda"
                                                       inManagedObjectContext:context];
        
        
        [agenda setMeetingAgendaItemID:[Utils stringToNSnumberIntFromDictionary:ibMeetingAgendaItem.meetingAgendaItemID]];
        [agenda setItemName:ibMeetingAgendaItem.itemName];
        [agenda setItemNumber:ibMeetingAgendaItem.itemNumber];
        [agenda setItemOrder:[Utils stringToNSnumberIntFromDictionary:ibMeetingAgendaItem.itemOrder]];
        [agenda setIsDeletedState:[Utils stringToNSNumberBoolFromDictionary:ibMeetingAgendaItem.isDeleted]];
        
        
        // add document list
        for (IBDocumentPackDocument *ibDocumentPackDocument in ibMeetingAgendaItem.documentList) {
            
            if ([[Utils stringToNSNumberBoolFromDictionary:ibDocumentPackDocument.isDeleted] boolValue])
            {
                //since the object is marked as deleted, don't add it to the database
                continue;
            }
            
            Document *document;
            document = [NSEntityDescription insertNewObjectForEntityForName:@"Document"
                                                     inManagedObjectContext:context];
            
            [document setDocumentPackDocumentID:[Utils stringToNSnumberIntFromDictionary:ibDocumentPackDocument.documentPackDocumentID]];
            [document setDocumentID:[Utils stringToNSnumberIntFromDictionary:ibDocumentPackDocument.documentID]];
            [document setDocumentOrder:[Utils stringToNSnumberIntFromDictionary:ibDocumentPackDocument.documentOrder]];
            [document setDocumentName:ibDocumentPackDocument.documentName];
            [document setCanEmail:[Utils stringToNSNumberBoolFromDictionary:ibDocumentPackDocument.canEmail]];
            [document setCanPrint:[Utils stringToNSNumberBoolFromDictionary:ibDocumentPackDocument.canPrint]];
            [document setFileName:ibDocumentPackDocument.fileName];
            [document setIsDeletedState:[Utils stringToNSNumberBoolFromDictionary:ibDocumentPackDocument.isDeleted]];
            
            // add to relationship
            [agenda addDocumentsObject:document];
            
        }
        
        // add to relationship
        [meeting addAgendasObject:agenda];
        
    }
    
    
    // save changes
    [dataStore saveChanges];
}


+(void)addOrUpdateDocumentPack:(IBDocumentPack *)ibDocumentPack company:(Company*)company
{
    DataStore *dataStore = [DataStore sharedDataStore];
    NSManagedObjectContext *context = [dataStore context];
    
    // check if the DocumentPack object already exists in the database
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"DocumentPack"];
    
    NSString *documentPackID = ibDocumentPack.documentPackID;
    NSString *companyID = [company.companyID stringValue];
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"(documentPackID == %@) and (userID == %@) and (company.companyID == %@)", documentPackID, [DBContext getUserID], companyID];
    
    [request setPredicate:pred];
    
    NSError *error;
    NSArray *items = [context executeFetchRequest:request error:&error];
    
    if (!items) {
        NSLog(@"Error occurred while fetching. Error: %@", [error localizedDescription]);
    }
    
    DocumentPack *documentPack;
    
    BOOL isNew = !(items.count > 0);
    
    if (isNew)
    {
        NSLog(@"Adding new DocumentPack to database");
        documentPack = [NSEntityDescription insertNewObjectForEntityForName:@"DocumentPack"
                                                     inManagedObjectContext:context];
    }
    
    for (DocumentPack *docPack in items) {
        // found object
        documentPack = docPack;
    }
    
    //update document
    
    [documentPack setCompany:company];
    
    [documentPack setUserID:[Utils stringToNSnumberIntFromDictionary:[DBContext getUserID]]];
    [documentPack setDocumentPackID:[Utils stringToNSnumberIntFromDictionary:ibDocumentPack.documentPackID]];
    [documentPack setPackType:[Utils stringToNSnumberIntFromDictionary:ibDocumentPack.packType]];
    [documentPack setPackName:ibDocumentPack.packName];
    [documentPack setPackDescription:ibDocumentPack.packDescription];
    [documentPack setCoverNote:ibDocumentPack.coverNote];
    [documentPack setCoverStyleID:[Utils stringToNSnumberIntFromDictionary:ibDocumentPack.coverStyleID]];
    [documentPack setAllowEmail:[Utils stringToNSNumberBoolFromDictionary:ibDocumentPack.allowEmail]];
    [documentPack setAllowPrint:[Utils stringToNSNumberBoolFromDictionary:ibDocumentPack.allowPrint]];
    [documentPack setIsDeletedState:[Utils stringToNSNumberBoolFromDictionary:ibDocumentPack.isDeleted]];
    
    
    //clear the document pack document
    for (DocumentPackDocument *docPackDoc in documentPack.documentPackDocuments) {
        [context deleteObject:[context objectWithID:docPackDoc.objectID]];
    }
    
    
    // add document pack document from API
    for (IBDocumentPackDocument* ibDocumentPackDocument in ibDocumentPack.documentList)
    {
        
        if ([[Utils stringToNSNumberBoolFromDictionary:ibDocumentPackDocument.isDeleted] boolValue])
        {
            //since the object is marked as deleted, don't add it to the database
            continue;
        }
        
        DocumentPackDocument *documentPackDocument;
        documentPackDocument = [NSEntityDescription insertNewObjectForEntityForName:@"DocumentPackDocument"
                                                             inManagedObjectContext:context];
        
        [documentPackDocument setDocumentPackDocumentID:[Utils stringToNSnumberIntFromDictionary:ibDocumentPackDocument.documentPackDocumentID]];
        [documentPackDocument setDocumentID:[Utils stringToNSnumberIntFromDictionary:ibDocumentPackDocument.documentID]];
        [documentPackDocument setDocumentOrder:[Utils stringToNSnumberIntFromDictionary:ibDocumentPackDocument.documentOrder]];
        [documentPackDocument setDocumentName:ibDocumentPackDocument.documentName];
        [documentPackDocument setCanEmail:[Utils stringToNSNumberBoolFromDictionary:ibDocumentPackDocument.canEmail]];
        [documentPackDocument setCanPrint:[Utils stringToNSNumberBoolFromDictionary:ibDocumentPackDocument.canPrint]];
        [documentPackDocument setFileName:ibDocumentPackDocument.fileName];
        [documentPackDocument setIsDeletedState:[Utils stringToNSNumberBoolFromDictionary:ibDocumentPackDocument.isDeleted]];
        
        // add to relationship
        [documentPack addDocumentPackDocumentsObject:documentPackDocument];
    }
    
    // save changes
    [dataStore saveChanges];
}



#pragma mark - Company


+(void)addCompanies:(NSArray *)ibCompanies
{
    for (IBCompany *com in ibCompanies) {
        [DBContext companyAddOrUpdate:com];
    }
}

+(void)companyAddOrUpdate:(IBCompany *)ibCompany
{
    DataStore *dataStore = [DataStore sharedDataStore];
    NSManagedObjectContext *context = [dataStore context];
    
    // check if the company object already exists in the database
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Company"];
    
    NSString *companyID = ibCompany.companyID;
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"(companyID == %@) and (userID == %@)", companyID, [DBContext getUserID]];
    
    [request setPredicate:pred];
    
    NSError *error;
    NSArray *items = [context executeFetchRequest:request error:&error];
    
    if (!items) {
        NSLog(@"Error occurred while fetching. Error: %@", [error localizedDescription]);
    }
    
    Company *company;
    
    BOOL isNew = !(items.count > 0);
    
    if (isNew)
    {
        NSLog(@"Adding new company to database");
        company = [NSEntityDescription insertNewObjectForEntityForName:@"Company"
                                                inManagedObjectContext:context];
    }
    
    for (Company *com in items) {
        // company found
        company = com;
    }
    
    //update object
    
    [company setUserID:[Utils stringToNSnumberIntFromDictionary:[DBContext getUserID]]];
    [company setCompanyID:[Utils stringToNSnumberIntFromDictionary:ibCompany.companyID]];
    [company setCompanyName:ibCompany.companyName];
    [company setRequireOnlineSignInEveryXDays:[Utils stringToNSnumberIntFromDictionary:ibCompany.requireOnlineSignInEveryXDays]];
    [company setReLoginInactivityMinutes:[Utils stringToNSnumberIntFromDictionary:ibCompany.reLoginInactivityMinutes]];
    [company setAllowEmail:[Utils stringToNSNumberBoolFromDictionary:ibCompany.allowEmail]];
    [company setAllowPrint:[Utils stringToNSNumberBoolFromDictionary:ibCompany.allowPrint]];
    [company setCompanyUserLevelID:[Utils stringToNSnumberIntFromDictionary:ibCompany.companyUserLevelID]];
    [company setIsDeletedState:[Utils stringToNSNumberBoolFromDictionary:ibCompany.isDeleted]];
    [company setImageIDLogo:[Utils stringToNSnumberIntFromDictionary:ibCompany.imageIDLogo]];
    
    //remove the cached image just in case it was updated by the API
    [[IBFileManager sharedInstance] removeImageWithImageID:ibCompany.imageIDLogo];
    
    
    // save changes
    [dataStore saveChanges];
}



+(NSArray *)getCompanies
{
    DataStore *dataStore = [DataStore sharedDataStore];
    NSManagedObjectContext *context = [dataStore context];
    
    // check if the company object already exists in the database
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Company"];
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"userID == %@", [DBContext getUserID]];
    [request setPredicate:pred];
    
    NSError *error;
    NSArray *items = [context executeFetchRequest:request error:&error];
    
    if (!items) {
        NSLog(@"Error occurred while fetching. Error: %@", [error localizedDescription]);
    }
    
    return items;
}



@end
