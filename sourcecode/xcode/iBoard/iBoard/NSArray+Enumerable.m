//
//  NSArray+Enumerable.m
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/04/11.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import "NSArray+Enumerable.h"

@implementation NSArray (Enumerable)

- (NSArray *)mappedArrayWithBlock:(id (^)(id))block
{
    NSMutableArray *temp = [NSMutableArray arrayWithCapacity:self.count];
    for (id obj in self) {
        [temp addObject:block(obj)];
    }
    
    return temp;
}

@end
