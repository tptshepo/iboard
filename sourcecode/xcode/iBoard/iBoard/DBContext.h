//
//  DBContext.h
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/06/27.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IBCompany.h"
#import "IBDocumentPack.h"
#import "IBDocumentPackData.h"
#import "IBCompanyMeeting.h"
#import "DocumentPackData.h"
#import "Company.h"

@interface DBContext : NSObject


+(void)addDocumentPack:(IBDocumentPackData*)ibDocumentPackData company:(Company*)company;
+(void)addOrUpdateDocumentPack:(IBDocumentPack *)ibDocumentPack company:(Company*)company;
+(void)addOrUpdateCompanyMeeting:(IBCompanyMeeting*)ibCompanyMeeting company:(Company*)company;
//+(NSArray *)getDocumentPacks:(Company*)company;
//+(NSArray *)getMeetings:(Company*)company;
+(DocumentPackData *)getDocumentPackData:(Company*)company;

+(void) addCompanies:(NSArray*)ibCompanies;
+(void) companyAddOrUpdate:(IBCompany*)ibCompany;
+(NSArray*) getCompanies;




@end
