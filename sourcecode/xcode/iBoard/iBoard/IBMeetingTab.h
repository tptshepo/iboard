//
//  IBMeetingScreen.h
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/04/27.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Company.h"
#import "Meeting.h"

@protocol IBMeetingTabDelegate;


@interface IBMeetingTab : UIView


@property (weak, nonatomic) IBOutlet UIView *largeCoverView;
@property (strong, nonatomic) IBOutlet UIView *view;
@property (weak, nonatomic) IBOutlet UIView *gridContainer;


@property (weak, nonatomic) IBOutlet UIButton *viewDocumentButton;
@property (weak, nonatomic) IBOutlet UIButton *viewCalenderButton;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UILabel *apologiesLabel;
@property (weak, nonatomic) IBOutlet UIImageView *splitterImage;


- (IBAction)tapViewDocument:(id)sender;

@property (nonatomic, weak) id<IBMeetingTabDelegate> delegate;


-(void)setMeetingList:(NSArray*)meetingList company:(Company*)company;

@end


@protocol IBMeetingTabDelegate

- (void)companyMeetingSelected:(Meeting *)companyMeeting;

@end