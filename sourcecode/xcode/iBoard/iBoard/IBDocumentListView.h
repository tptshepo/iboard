//
//  IBDocumentListView.h
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/05/10.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DocumentPackDocument.h"
#import "DocumentPack.h"
#import "Company.h"

@protocol IBDocumentListViewDelegate;

@interface IBDocumentListView : UIView

@property (strong, nonatomic) IBOutlet UIView *view;

@property (weak, nonatomic) IBOutlet UIView *largeCoverView;
@property (weak, nonatomic) IBOutlet UIView *gridContainer;
@property (weak, nonatomic) IBOutlet UIButton *viewDocumentButton;
@property (weak, nonatomic) IBOutlet UIImageView *splitterImage;

@property (nonatomic, weak) id<IBDocumentListViewDelegate> delegate;

- (IBAction)tapViewDocument:(id)sender;

-(void)setDocumentPackList:(NSArray *)documentList documentPack:(DocumentPack*)documentPack company:(Company*)company;

@end


@protocol IBDocumentListViewDelegate

- (void)documentSelected:(DocumentPackDocument *)documentPackDocument;

@end