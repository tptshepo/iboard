//
//  ARFFileManager.h
//  ARF Interface
//
//  Created by Tshepo Mgaga on 2014/02/26.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface IBFileManager : NSObject


#pragma mark - Singleton access

+ (IBFileManager *)sharedInstance;


#pragma mark - API Files

-(void)saveImageWithImageID:(NSString*)imageID content:(NSData*)content;
-(BOOL)hasImageID:(NSString*)imageID;
-(NSData*)getImageWithImageID:(NSString*)imageID;
-(void)removeImageWithImageID:(NSString*)imageID;


-(void)saveDocumentWithDocumentID:(NSString*)documentID content:(NSData*)content;
-(NSString*)getDocumentFilePathWithID:(NSString*)documentID;
-(BOOL)hasDocumentID:(NSString*)documentID;
-(NSData*)getDocumentWithDocumentID:(NSString*)documentID;

-(void)saveAnnotationWithDocumentID:(NSString*)documentID image:(UIImage*)image page:(NSString*)page;
-(void)saveAnnotationJSONWithDocumentID:(NSString *)documentID page:(NSString*)page data:(NSString*)data;

-(UIImage*)getAnnotationWithDocumentID:(NSString*)documentID page:(NSString*)page;
-(NSString*)getAnnotationJSONWithDocumentID:(NSString*)documentID page:(NSString*)page;

#pragma mark - File Helpers

-(void) prepareFileSystem;
-(BOOL) writeToFileWithFileName:(NSString *)fileName content:(NSString *)content;
-(BOOL) writeToFileWithData:(NSData *)data fileName:(NSString *)fileName;
-(BOOL) deleteFileName:(NSString *)fileName;
-(BOOL) fileNameExisits:(NSString *)fileName;
-(NSString *)getFileContents:(NSString*)filename;
-(NSString *)getDataRoot;
-(void) reset;

+(NSURL *)pathForDocumentsFile:(NSString *)filename;

@end
