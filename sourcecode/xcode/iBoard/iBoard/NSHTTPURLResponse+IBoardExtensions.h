//
//  NSHTTPURLResponse+IBoardExtensions.h
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/04/11.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSHTTPURLResponse (IBoardExtensions)

/**
 * Attempt to extract an error message from the given data object
 * (assumes JSON payload), otherwise return default error message.
 */
- (NSString *)errorMessageWithData:(NSData *)data;

@end
