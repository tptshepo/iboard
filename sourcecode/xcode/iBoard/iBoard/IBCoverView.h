//
//  IBCoverView.h
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/04/26.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IBCoverView : UIView

@property (strong, nonatomic) IBOutlet UIView *view;

@property (weak, nonatomic) IBOutlet UILabel *headerText;
@property (weak, nonatomic) IBOutlet UILabel *subLabel1;
@property (weak, nonatomic) IBOutlet UILabel *subLabel2;
@property (weak, nonatomic) IBOutlet UILabel *footerText;


-(void)hideHeaderText:(BOOL)isHidden;

-(void) initCoverWithHeaderText:(NSString*)headerText
                      subLabel1:(NSString*)subLabel1
                      subLabel2:(NSString*)subLabel2
                     footerText:(NSString*)footerText
                          color:(UIColor*)color;


@end
