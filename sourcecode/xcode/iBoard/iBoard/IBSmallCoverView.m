//
//  IBSmallCoverView.m
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/04/27.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import "IBSmallCoverView.h"

@implementation IBSmallCoverView



#pragma mark - View Methods


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        [[NSBundle mainBundle] loadNibNamed:@"IBSmallCoverView" owner:self options:nil];
        
        //[self.view setBackgroundColor:[UIColor clearColor]];
        [self addSubview:self.view];
        
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [[NSBundle mainBundle] loadNibNamed:@"IBSmallCoverView" owner:self options:nil];
        //[self.view setBackgroundColor:[UIColor clearColor]];
        
        self.bounds = self.view.bounds;
        
        [self addSubview:self.view];
        
    }
    return self;
}


-(void)hideHeaderText:(BOOL)isHidden
{
    [self.headerText setHidden:isHidden];
}


-(void)setCoverBackgroundColor:(UIColor *)color
{
    [self.view setBackgroundColor:color];
}

-(void)initCoverWithHeaderText:(NSString *)headerText
                     subLabel1:(NSString *)subLabel1
                     subLabel2:(NSString *)subLabel2
                    footerText:(NSString *)footerText
                         color:(UIColor *)color
{
    [self.headerText setText:headerText];
    [self.subLabel1 setText:subLabel1];
    [self.subLabel2 setText:subLabel2];
    [self.footerText setText:footerText];
    
    [self.view setBackgroundColor:color];
}

@end
