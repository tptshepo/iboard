//
//  IBDocumentListViewControlerViewController.m
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/05/10.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import "IBDocumentListViewControler.h"
#import "IBDocumentPack.h"
#import "IBDocumentListView.h"
#import "IBDocumentViewController.h"


@interface IBDocumentListViewControler()
{
    IBDocumentListView * _documentListView;
    DocumentPackDocument * _documentPackDocument;
}

@end

@implementation IBDocumentListViewControler

@synthesize documentPack = _documentPack;
@synthesize company = _company;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.viewTitle setText:_documentPack.packName];
    
    self.viewDocumentList.backgroundColor = [UIColor clearColor];
    
    IBDocumentListView *documentListView = [[IBDocumentListView alloc] initWithFrame:CGRectMake(0,0,755,926)];
    _documentListView = documentListView;
    documentListView.delegate = self;
    
    [self.viewDocumentList addSubview:documentListView];
    
    [documentListView setDocumentPackList:[_documentPack.documentPackDocuments allObjects] documentPack:_documentPack company:_company];
}

- (IBAction)backToDocumentList:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"DocumentViewSegue"]) {
        IBDocumentViewController *vc = (IBDocumentViewController *)segue.destinationViewController;
        
        vc.documentPackDocument  = _documentPackDocument;
    }
}


-(void)documentSelected:(DocumentPackDocument *)documentPackDocument
{
    //NSLog(@"documentPackDocument selected: %@", [documentPackDocument dictionaryRepresentation]);
    
    _documentPackDocument = documentPackDocument;
    
    if (self.presentedViewController == nil) {
        [self performSegueWithIdentifier:@"DocumentViewSegue" sender:nil];
    }
    
}

@end
