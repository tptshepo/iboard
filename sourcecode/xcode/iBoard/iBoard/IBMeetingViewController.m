//
//  IBMeetingViewController.m
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/04/19.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import "IBMeetingViewController.h"
#import "IBMeetingTab.h"
#import "IBService.h"
#import "IBDocumentPackData.h"
#import "IBDocumentTab.h"
#import "IBDocumentListViewControler.h"
#import "IBWaitView.h"
#import "IBCompanyMeeting.h"
#import "IBMeetingAgendaViewController.h"

#import "DBContext.h"
#import "DocumentPackData.h"
#import "DocumentPack.h"
#import "Meeting.h"

@interface IBMeetingViewController ()
{
    IBMeetingTab * _meetingTab;
    IBDocumentTab * _documentTab;
    
    DocumentPack * _selectedDocumentPack;
    Meeting * _selectedCompanyMeeting;
    
    int _spinnerCount;
}

@property (nonatomic, strong) IBWaitView *waitView;

@end



@implementation IBMeetingViewController


#pragma mark - Properties

@synthesize company = _company;
@synthesize documentPacksData = _documentPacksData;


#pragma mark UIViewController events

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //[self.infoButton setEnabled:NO];
    //[self.searchButton setEnabled:NO];
    
    self.waitView = [[IBWaitView alloc] initWithFrame:CGRectZero];
    [self.waitView setBounds:[self.waitContainer bounds]];
    [self.waitContainer setBackgroundColor:[UIColor clearColor]];
    [self.waitContainer addSubview:self.waitView];
    [self.waitView showWithText:@""];
    [self hideSpinner];
    
    [self.viewTitle setText:_company.companyName];
    
    self.meetingView.backgroundColor = [UIColor clearColor];
    
    IBMeetingTab *meetingTab = [[IBMeetingTab alloc] initWithFrame:CGRectMake(0,0,768,902)];
    meetingTab.delegate = self;
    _meetingTab = meetingTab;
    
    
    IBDocumentTab *documentTab = [[IBDocumentTab alloc] initWithFrame:CGRectMake(0,0,768,902)];
    documentTab.delegate = self;
    _documentTab = documentTab;
    
    [self getDocumentPacks];
    
    [self changeSegmentStateTo:@"meetings"];
}

#pragma mark IBDocumentTabDelegate

- (void)documentSelected:(DocumentPack *)documentPack
{
    _selectedDocumentPack = documentPack;
    if (self.presentedViewController == nil) {
        [self performSegueWithIdentifier:@"DocumentListSegue" sender:nil];
    }
}


#pragma mark IBMeetingTabDelegate

-(void)companyMeetingSelected:(Meeting *)companyMeeting
{
    _selectedCompanyMeeting = companyMeeting;
    
    if (self.presentedViewController == nil) {
        [self performSegueWithIdentifier:@"MeetingAgendaSegue" sender:nil];
    }
}

#pragma mark - API Methods

-(void)setDocumentPacksData:(DocumentPackData *)documentPacksData
{
    
    _documentPacksData = documentPacksData;
    
    // load meetings
    [_meetingTab setMeetingList:documentPacksData.meetings company:_company];
    
    //load documents
    [_documentTab setDocumentPackList:documentPacksData.documentPacks company:_company];
    
    
}

-(void)getDocumentPacks
{
    __weak typeof(self) weakSelf = self;
    
    [self showSpinner];
    
    IBService *service = [IBService sharedInstance];
    
    [service getDocumentsPacksWithComapnyID:[_company.companyID intValue]
                                    success:^(IBDocumentPackData *documentPackData) {
                                        NSLog(@"Get document Packs");
                                        
                                         //update database
                                        [DBContext addDocumentPack:documentPackData company:_company];
                                        
                                        //get database data
                                        [weakSelf setDocumentPacksData:[DBContext getDocumentPackData:_company]];
                                        
                                        [weakSelf hideSpinner];
                                        
                                    }
                                    failure:^(NSError *error, NSInteger statusCode) {
                                        NSLog(@"Failed to get document packs");
                                        
                                        //get database data
                                        [weakSelf setDocumentPacksData:[DBContext getDocumentPackData:_company]];
                                        
                                        [weakSelf hideSpinner];
                                        
                                    }];
}

#pragma mark - Helpers

-(void)showSpinner {
    _spinnerCount ++;
    [self.waitContainer setHidden:NO];
}

-(void)hideSpinner {
    _spinnerCount--;
    
    if (_spinnerCount < 1)
        [self.waitContainer setHidden:YES];
}

-(void)changeSegmentStateTo:(NSString *)viewName
{
    // clear the loaded subviews
    [[self.meetingView subviews] makeObjectsPerformSelector: @selector(removeFromSuperview)];
    
    if ([viewName isEqualToString:@"meetings"]) {
        
        [self.segMeeting setBackgroundImage:[UIImage imageNamed: @"segment_active.png"] forState:UIControlStateNormal];
        [self.segDocument setBackgroundImage:[UIImage imageNamed: @"segment_inactive.png"] forState:UIControlStateNormal];
        [self.segArchive setBackgroundImage:[UIImage imageNamed: @"segment_inactive.png"] forState:UIControlStateNormal];
        
        
        [self.meetingView addSubview:_meetingTab];
    }
    
    if ([viewName isEqualToString:@"documents"]) {
        
        [self.segMeeting setBackgroundImage:[UIImage imageNamed: @"segment_inactive.png"] forState:UIControlStateNormal];
        [self.segDocument setBackgroundImage:[UIImage imageNamed: @"segment_active.png"] forState:UIControlStateNormal];
        [self.segArchive setBackgroundImage:[UIImage imageNamed: @"segment_inactive.png"] forState:UIControlStateNormal];
        
        [self.meetingView addSubview:_documentTab];
    }
    
    if ([viewName isEqualToString:@"archives"]) {
        
        [self.segMeeting setBackgroundImage:[UIImage imageNamed: @"segment_inactive.png"] forState:UIControlStateNormal];
        [self.segDocument setBackgroundImage:[UIImage imageNamed: @"segment_inactive.png"] forState:UIControlStateNormal];
        [self.segArchive setBackgroundImage:[UIImage imageNamed: @"segment_active.png"] forState:UIControlStateNormal];
    }
}


#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"DocumentListSegue"]) {
        IBDocumentListViewControler *vc = (IBDocumentListViewControler *)segue.destinationViewController;
        
        vc.company  = _company;
        vc.documentPack = _selectedDocumentPack;
    }
    
    if ([segue.identifier isEqualToString:@"MeetingAgendaSegue"]) {
        IBMeetingAgendaViewController *vc = (IBMeetingAgendaViewController *)segue.destinationViewController;
        
        vc.companyMeeting  = _selectedCompanyMeeting;
    }
    
}

- (IBAction)showMeetings:(id)sender {
    [self changeSegmentStateTo:@"meetings"];
}

- (IBAction)showDocuments:(id)sender {
    [self changeSegmentStateTo:@"documents"];
}

- (IBAction)showArchives:(id)sender {
    
    [self changeSegmentStateTo:@"archives"];
}

- (IBAction)backToProfile:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)showInfo:(id)sender {
}

- (IBAction)showSearch:(id)sender {
}

- (IBAction)reloadData:(id)sender {
}

@end
