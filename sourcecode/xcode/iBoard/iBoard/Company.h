//
//  Company.h
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/06/28.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class DocumentPack, Meeting;

@interface Company : NSManagedObject

@property (nonatomic, retain) NSNumber * companyID;
@property (nonatomic, retain) NSString * companyName;
@property (nonatomic, retain) NSNumber * requireOnlineSignInEveryXDays;
@property (nonatomic, retain) NSNumber * reLoginInactivityMinutes;
@property (nonatomic, retain) NSNumber * allowEmail;
@property (nonatomic, retain) NSNumber * allowPrint;
@property (nonatomic, retain) NSNumber * companyUserLevelID;
@property (nonatomic, retain) NSNumber * isDeletedState;
@property (nonatomic, retain) NSNumber * imageIDLogo;
@property (nonatomic, retain) NSNumber * userID;
@property (nonatomic, retain) NSSet *meetings;
@property (nonatomic, retain) NSSet *documentPacks;
@end

@interface Company (CoreDataGeneratedAccessors)

- (void)addMeetingsObject:(Meeting *)value;
- (void)removeMeetingsObject:(Meeting *)value;
- (void)addMeetings:(NSSet *)values;
- (void)removeMeetings:(NSSet *)values;

- (void)addDocumentPacksObject:(DocumentPack *)value;
- (void)removeDocumentPacksObject:(DocumentPack *)value;
- (void)addDocumentPacks:(NSSet *)values;
- (void)removeDocumentPacks:(NSSet *)values;

@end
