//
//  IBSettingsPanelView.h
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/06/05.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol IBSettingsPanelViewDelegate;

@interface IBSettingsPanelView : UIView

@property (strong, nonatomic) IBOutlet UIView *view;

@property (nonatomic, weak) id<IBSettingsPanelViewDelegate> delegate;


- (IBAction)tapPenColor:(id)sender;

- (IBAction)taphighlighterColor:(id)sender;

@end


@protocol IBSettingsPanelViewDelegate

@required

- (void)onPenColorChange:(CGFloat)red green:(CGFloat)green blue:(CGFloat)blue;
- (void)onHighlighterColorChange:(CGFloat)red green:(CGFloat)green blue:(CGFloat)blue;

@end