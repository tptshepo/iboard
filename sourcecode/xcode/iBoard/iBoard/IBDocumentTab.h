//
//  IBDocumentScreen.h
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/05/10.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Company.h"
#import "DocumentPack.h"


@protocol IBDocumentTabDelegate;


@interface IBDocumentTab : UIView

@property (strong, nonatomic) IBOutlet UIView *view;

@property (weak, nonatomic) IBOutlet UIView *largeCoverView;
@property (weak, nonatomic) IBOutlet UIView *gridContainer;
@property (weak, nonatomic) IBOutlet UIButton *viewDocumentPackButton;
@property (weak, nonatomic) IBOutlet UIImageView *splitterImage;

- (IBAction)tapViewPack:(id)sender;


@property (nonatomic, weak) id<IBDocumentTabDelegate> delegate;

-(void)setDocumentPackList:(NSArray *)documentPackList company:(Company*)company;

@end


@protocol IBDocumentTabDelegate

- (void)documentSelected:(DocumentPack *)documentPack;

@end