//
//  Agenda.m
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/06/28.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import "Agenda.h"
#import "Document.h"
#import "Meeting.h"


@implementation Agenda

@dynamic meetingAgendaItemID;
@dynamic itemNumber;
@dynamic itemName;
@dynamic itemOrder;
@dynamic isDeletedState;
@dynamic meeting;
@dynamic documents;

@end
