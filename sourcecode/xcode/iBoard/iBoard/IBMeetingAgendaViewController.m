//
//  IBMeetingAgendaViewController.m
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/05/30.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import "IBMeetingAgendaViewController.h"
#import "IBAgendaView.h"

#import "IBMeetingTab.h"
#import "Document.h"

#import "IBWaitView.h"
#import "IBService.h"

#import "IBDocumentViewerView.h"
#import "IBSettingsPanelView.h"


#define AGENDA_VIEW_HEIGHT 871

#define SETTING_VIEW_WIDTH 287
#define SETTING_VIEW_HEIGHT 221
#define AGENDA_SIDE_BAR_ANIMATION_SPEED 0.2


@interface IBMeetingAgendaViewController () <IBAgendaViewDelegate, IBDocumentViewerViewDelegate, IBSettingsPanelViewDelegate>
{
    int _spinnerCount;
    BOOL _isAgendaVisible;
    BOOL _isSettingPanelVisible;
    IBAgendaView * _agendaView;
    IBDocumentViewerView * _documentView;
    IBSettingsPanelView * _settingsPanelView;
    
    int _downloadCount;
    BOOL _downloadError;
    
    BOOL _penOn;
    BOOL _highlighterOn;
    BOOL _commentOn;
}

@property (nonatomic, strong) IBWaitView *waitView;

@end

@implementation IBMeetingAgendaViewController

@synthesize companyMeeting = _companyMeeting;


#pragma mark - View Controller Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    
    self.documentContainer.bounds = CGRectMake(0, 0, 768, 871);
    
    _spinnerCount = 0;
    _isAgendaVisible = YES;
    _isSettingPanelVisible = NO;
    
    [self.settingsButton setEnabled:NO];
    [self.penButton setEnabled:NO];
    [self.highlighterButton setEnabled:NO];
    [self.commentButton setEnabled:NO];
    
    
    [self.settingsPanelContainer setBackgroundColor:[UIColor clearColor]];
    
    [self.documentTitle setText:@""];
    
    self.waitView = [[IBWaitView alloc] initWithFrame:CGRectZero];
    [self.waitView setBounds:[self.waitContainer bounds]];
    [self.waitContainer setBackgroundColor:[UIColor clearColor]];
    [self.waitContainer addSubview:self.waitView];
    [self.waitView showWithText:@""];
    [self.waitContainer setHidden:YES];
    
    [self.agendaViewContainer setHidden:YES];
    [self.infoButton setEnabled:NO];
    [self.settingsPanelContainer setHidden:YES];
    
    [self.titleView setText:_companyMeeting.packName];
    
    [self downloadAllDocuments];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Spinners

-(void)showSpinner {
    _spinnerCount ++;
    [self.waitContainer setHidden:NO];
    
    //NSLog(@"Show spinner: %d", _spinnerCount);
}

-(void)hideSpinner {
    _spinnerCount--;
    
   // NSLog(@"hide spinner: %d", _spinnerCount);
    
    if (_downloadCount == 0) {
        [self.waitContainer setHidden:YES];
        
        if (!_downloadError){
            [self.agendaViewContainer setHidden:NO];
            [self.infoButton setEnabled:YES];
            [self loadAgendaList];
            [self loadSettingsPanel];
        }
    }
    
    
}


#pragma mark - Agenda

-(void)showAgenda
{
    //NSLog(@"showAgenda");
    
    [_documentView setSwipeEnabled:NO];
    [self.agendaViewContainer setFrame:CGRectMake(-314, 82, 312, AGENDA_VIEW_HEIGHT)];
    
    [UIView animateWithDuration:AGENDA_SIDE_BAR_ANIMATION_SPEED animations:^{
        [self.agendaViewContainer setFrame:CGRectMake(-1, 82, 312, AGENDA_VIEW_HEIGHT)];
    }];
    
    [self.agendaViewContainer setHidden:NO];
    _isAgendaVisible = YES;
}

-(void)hideAgenda:(void (^)(BOOL finished))done
{
    NSLog(@"hideAgenda");
    
    if (!_isAgendaVisible)
    {
        [_documentView setSwipeEnabled:YES];
        done(YES);
        return;
    }
    
    //[self.agendaViewContainer setFrame:CGRectMake(-1, 82, 312, AGENDA_VIEW_HEIGHT)];
    
    [UIView animateWithDuration:AGENDA_SIDE_BAR_ANIMATION_SPEED animations:^{
        [self.agendaViewContainer setFrame:CGRectMake(-314, 82, 312, AGENDA_VIEW_HEIGHT)];
    }
                     completion:^ (BOOL finished)
     {
         if (finished) {
             [self.agendaViewContainer setHidden:YES];
             [_documentView setSwipeEnabled:YES];
             done(YES);
         }
     }
     ];
    
    _isAgendaVisible = NO;
}

-(void)loadAgendaList
{
    [[self.agendaViewContainer subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    IBAgendaView *agendaView = [[IBAgendaView alloc] initWithFrame:CGRectMake(0,0,312,AGENDA_VIEW_HEIGHT)];
    _agendaView = agendaView;
    _agendaView.delegate = self;

    
    [_agendaView setMeetingAgendaItemList:[_companyMeeting.agendas allObjects] companyMeeting:_companyMeeting];
    
    [self.agendaViewContainer addSubview:_agendaView];
}



-(void)agendaItemSelected:(Agenda *)meetingAgendaItem
{
    //NSLog(@"agendaItemSelected");
    
    [[self.documentContainer subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    [self resetAnnotationControls];
    
    [self.documentTitle setText:@""];
    
    if (meetingAgendaItem.documents != nil){
        if (meetingAgendaItem.documents.count > 0){
            
            //[self hideAgenda];
            
            Document * doc = [meetingAgendaItem.documents allObjects][0];
            [self.documentTitle setText:doc.fileName];
            
            IBDocumentViewerView * documentView = [[IBDocumentViewerView alloc] initWithFrame:self.documentContainer.bounds];
            documentView.delegate = self;
            _documentView = documentView;
            [self.documentContainer addSubview:documentView];
            [_documentView loadDocument:[doc.documentID stringValue] canCallServer:YES];
            
            [_documentView setSwipeEnabled:NO];
            
            [self.settingsButton setEnabled:YES];
            [self.penButton setEnabled:YES];
            [self.highlighterButton setEnabled:YES];
            [self.commentButton setEnabled:YES];
            
        } else {
            
            [self resetSettings];
        }
    }
    
}

#pragma mark - Settings

-(void)showSettingsPanel
{
    //NSLog(@"showSettingsPanel");
    
    [self.settingsButton setBackgroundImage:[UIImage imageNamed:@"icon9_active"] forState:normal];
    
    [self.settingsPanelContainer setFrame:CGRectMake(self.view.bounds.size.width - SETTING_VIEW_WIDTH - 2,
                                                     self.view.bounds.size.height - SETTING_VIEW_HEIGHT - 73,
                                                     SETTING_VIEW_WIDTH,
                                                     SETTING_VIEW_HEIGHT)];
    
    
    [self.settingsPanelContainer setHidden:NO];
    _isSettingPanelVisible = YES;
}

-(void)hideSettingsPanel
{
    //NSLog(@"hideSettingsPanel");
    
    [self.settingsButton setBackgroundImage:[UIImage imageNamed:@"icon9"] forState:normal];
    
    [self.settingsPanelContainer setFrame:CGRectMake(self.view.bounds.size.width - SETTING_VIEW_WIDTH - 2,
                                                     self.view.bounds.size.height - SETTING_VIEW_HEIGHT - 73,
                                                     SETTING_VIEW_WIDTH,
                                                     SETTING_VIEW_HEIGHT)];
    
    
    [self.settingsPanelContainer setHidden:YES];
    _isSettingPanelVisible = NO;
}

-(void)loadSettingsPanel
{
    [[self.settingsPanelContainer subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    IBSettingsPanelView *settingsView = [[IBSettingsPanelView alloc] initWithFrame:CGRectMake(0,0,SETTING_VIEW_WIDTH,SETTING_VIEW_HEIGHT)];
    _settingsPanelView = settingsView;
    settingsView.delegate  = self;
    [self.settingsPanelContainer addSubview:_settingsPanelView];
    
    [self hideSettingsPanel];
}

#pragma mark - Downloads

-(int)getDocumentDownloadCount
{
    int count = 0;
    
    NSArray *meetingAgendaItemList = [_companyMeeting.agendas allObjects];
    
    for (int i = 0; i < meetingAgendaItemList.count; i++)
    {
        Agenda * meetingAgendaItem = meetingAgendaItemList[i];
        
        if (meetingAgendaItem.documents != nil) {
            
            NSArray *documentList = [meetingAgendaItem.documents allObjects];
            
            for (int d = 0; d < documentList.count; d++)
            {
                count++;
            }
        }
    }
    
    return count;
}

-(void)downloadAllDocuments
{
    
    _downloadError = NO;
    _downloadCount = [self getDocumentDownloadCount];
    
    NSArray *meetingAgendaItemList = [_companyMeeting.agendas allObjects];
    
    for (int i = 0; i < meetingAgendaItemList.count; i++)
    {
        Agenda * meetingAgendaItem = meetingAgendaItemList[i];
        
        if (meetingAgendaItem.documents != nil) {
            
            NSArray *documentList = [meetingAgendaItem.documents allObjects];
            
            for (int d = 0; d < documentList.count; d++)
            {
                Document * doc = documentList[d];
                
                NSLog(@"Doc: %@", doc.documentID);
                
                [self showSpinner];
                
                __weak typeof(self) weakSelf = self;
                
                IBService *service = [IBService sharedInstance];
                
                [service getDocumentWithID:[doc.documentID stringValue]
                                   success:^(NSData *data) {
                                       NSLog(@"document downloaded");
                                       
                                       _downloadCount--;
                                       
                                       [weakSelf hideSpinner];
                                   }
                                   failure:^(NSError *error, NSInteger statusCode) {
                                       NSLog(@"Failed to get image");
                                       _downloadCount--;
                                       _downloadError = YES;
                                       [weakSelf hideSpinner];
                                       
                                   }];
                
            }
            
        }
        
    }
    
}


#pragma mark - Helpers

-(void)setAnnotationType:(NSString*)type
{
    
    NSDictionary *info = @{
                           @"action": @"annotationType",
                           @"type" : type
                           };
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ColorNotification" object:nil userInfo:info];
}

-(void)resetAnnotationControls
{
    [self highlighterOn:NO];
    [self penOn:NO];
    [self commentOn:NO];
    [self hideSettingsPanel];
}

-(void)resetSettings
{
    [self.settingsButton setEnabled:NO];
    [self.penButton setEnabled:NO];
    [self.highlighterButton setEnabled:NO];
    [self.commentButton setEnabled:NO];
    
    [self highlighterOn:NO];
    [self penOn:NO];
    [self commentOn:NO];
    [self hideSettingsPanel];
    
}

- (void)highlighterOn:(BOOL)on {
    if (!on){
        [self.highlighterButton setBackgroundImage:[UIImage imageNamed:@"icon6"] forState:normal];
        _highlighterOn = NO;
        
    } else {
        _highlighterOn = YES;
        [self.highlighterButton setBackgroundImage:[UIImage imageNamed:@"icon6_active"] forState:normal];
    }
}


- (void)penOn:(BOOL)on {
    
    if (!on){
        [self.penButton setBackgroundImage:[UIImage imageNamed:@"icon7"] forState:normal];
        _penOn = NO;
        
    } else {
        _penOn = YES;
        [self.penButton setBackgroundImage:[UIImage imageNamed:@"icon7_active"] forState:normal];
    }
}


- (void)commentOn:(BOOL)on {
    if (!on){
        [self.commentButton setBackgroundImage:[UIImage imageNamed:@"icon8"] forState:normal];
        _commentOn = NO;
        
    } else {
        _commentOn = YES;
        [self.commentButton setBackgroundImage:[UIImage imageNamed:@"icon8_active"] forState:normal];
    }
}

#pragma mark - IBActions

- (IBAction)tapSettings:(id)sender {
    
    [self hideAgenda:^ (BOOL finished){
        
        if(_isSettingPanelVisible){
            [self hideSettingsPanel];
        } else {
            [self showSettingsPanel];
        }
        
    }];
}

- (IBAction)tapHighlighter:(id)sender {
    
    [self hideAgenda:^ (BOOL finished){
        
        [self highlighterOn:!_highlighterOn];
        [self penOn:NO];
        [self commentOn:NO];
        
        [_documentView setAnnotationMode:_highlighterOn];
        [self setAnnotationType:@"highlighter"];
        
    }];
}

- (IBAction)tapComment:(id)sender {
    
    [self hideAgenda:^ (BOOL finished){
        
        [self commentOn:!_commentOn];
        [self penOn:NO];
        [self highlighterOn:NO];
        
        [_documentView setAnnotationMode:_commentOn];
        [self setAnnotationType:@"comment"];
        
    }];
}

- (IBAction)tapPen:(id)sender {
    
    [self hideAgenda:^ (BOOL finished){
        
        [self penOn:!_penOn];
        [self highlighterOn:NO];
        [self commentOn:NO];
        
        [_documentView setAnnotationMode:_penOn];
        [self setAnnotationType:@"pen"];
        
    }];
}

- (IBAction)backToMeetingList:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)tapAgenda:(id)sender {
    
    if(_isAgendaVisible){
        [self hideAgenda:^ (BOOL finished){}];
    } else {
        [self showAgenda];
    }
    
}

#pragma mark - IBDocumentViewerViewDelegate

-(void)singleTap
{
    if(_isAgendaVisible)
        [self hideAgenda:^ (BOOL finished){}];
    
    if (_isSettingPanelVisible)
        [self hideSettingsPanel];
}


#pragma mark - IBSettingsPanelViewDelegate

-(void)onHighlighterColorChange:(CGFloat)red green:(CGFloat)green blue:(CGFloat)blue
{
    //NSLog(@"[HIGHLIGHTER] %f,%f,%f", red, green, blue);
    
    
    NSDictionary *info = @{
                           @"action": @"color",
                           @"color" :@"highlighter",
                           @"red" : [NSNumber numberWithFloat:red],
                           @"green" : [NSNumber numberWithFloat:green],
                           @"blue" : [NSNumber numberWithFloat:blue]
                           };
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ColorNotification" object:nil userInfo:info];
    
}

-(void)onPenColorChange:(CGFloat)red green:(CGFloat)green blue:(CGFloat)blue
{
    
    //NSLog(@"[PEN] %f,%f,%f", red, green, blue);
    
    NSDictionary *info = @{
                           @"action": @"color",
                           @"color" :@"pen",
                           @"red" : [NSNumber numberWithFloat:red],
                           @"green" : [NSNumber numberWithFloat:green],
                           @"blue" : [NSNumber numberWithFloat:blue]
                           };
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ColorNotification" object:nil userInfo:info];
}


@end
