//
//  IBAnnotationJSONData.m
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/04/28.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import "IBAnnotationJSONData.h"
#import "IBCommonKey.h"
#import "NSArray+Enumerable.h"

#import "IBAnnotationComment.h"

@interface IBAnnotationJSONData ()

@property (nonatomic, copy, readwrite) NSString *graphics;
@property (nonatomic, copy, readwrite) NSArray *comments;

@end


@implementation IBAnnotationJSONData


-(instancetype)initWithGraphics:(NSString *)graphics
                       comments:(NSArray *)comments
{
    
    
    if ((self = [super init])) {
        
        self.graphics = graphics;
        self.comments = comments;
        
    }
    
    return self;
    
    
}


- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    
    NSArray *list = [dict[IBCommentsKey] mappedArrayWithBlock:^id(id obj) {
        return [[IBAnnotationComment alloc] initWithDictionary:obj];
    }];
    
    return [self initWithGraphics:dict[IBGraphicsKey]
                         comments:list
            ];
    
}

- (NSDictionary *)dictionaryRepresentation
{
    
//    NSArray *list = [self.comments mappedArrayWithBlock:^id(id obj) {
//        return [[IBAnnotationComment alloc] initWithDictionary:obj];
//    }];
    
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    for (IBAnnotationComment *comment in self.comments) {
        [array addObject:comment.dictionaryRepresentation];
    }
    
    
    return @{
             IBGraphicsKey: self.graphics,
             IBCommentsKey: array,
             };
}

- (NSString *)description
{
    
    return [NSString stringWithFormat:@"<%@: 0x%x \
            Graphics=%@ \
            ",
            NSStringFromClass([self class]),
            (unsigned int)self,
            self.graphics
            ];
}


@end
