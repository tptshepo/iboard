//
//  IBImageStore.m
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/05/07.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import "IBImageStore.h"

@interface IBImageStore ()

@property (nonatomic, strong) NSURLCache *cache;

@end

@implementation IBImageStore


+ (instancetype)sharedInstance
{
    static IBImageStore *_ib_SharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _ib_SharedInstance = [[IBImageStore alloc] init];
    });
    
    return _ib_SharedInstance;
}

- (instancetype)init
{
    if ((self = [super init])) {
        self.cache = [[NSURLCache alloc] initWithMemoryCapacity:4*1024*1024 // 4MB
                                                   diskCapacity:32*1024*1024 // 32MB
                                                       diskPath:NSStringFromClass([self class])];
    }
    
    return self;
}

- (UIImage *)imageForURLRequest:(NSURLRequest *)request
                    placeholder:(UIImage *)placeholderImage
                    whenFetched:(void (^)(NSURLRequest *, UIImage *))callback
{
    NSCachedURLResponse *cachedResponse = [self.cache cachedResponseForRequest:request];
    if (cachedResponse) {
        return [UIImage imageWithData:cachedResponse.data];
    }
    else {
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if (data != nil && error == nil) {
                                                            NSCachedURLResponse *cacheResponse = [[NSCachedURLResponse alloc] initWithResponse:response data:data];
                                                            [self.cache storeCachedResponse:cacheResponse forRequest:request];
                                                            
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                UIImage *image = [UIImage imageWithData:data];
                                                                callback(request, image);
                                                            });
                                                        }
                                                        else if (error) {
                                                            NSLog(@"ERROR fetching %@: %@", request.URL, error);
                                                        }
                                                    }];
        [dataTask resume];
        
        return placeholderImage;
    }
}

@end
