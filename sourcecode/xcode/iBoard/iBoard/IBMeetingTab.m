//
//  IBMeetingScreen.m
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/04/27.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import "IBMeetingTab.h"
#import "IBCoverView.h"
#import "GMGridView.h"
#import "IBSmallCoverView.h"

#import "Meeting.h"
#import "Company.h"

#define COVER_STYLE_ALPHA 0.3

@interface IBMeetingTab () <GMGridViewDataSource, GMGridViewActionDelegate>

{
    __weak IBCoverView *_largeCoverView;
    
    __gm_weak GMGridView *_gmGridView;
    
    int _colorIndex;
    int _selectedIndex;
    
    NSArray * _meetingList;
    Company * _company;
    int _noOfMeetings;
    
    Meeting * _currentSelectedCompanyMeeting;
    int _currentSelectedCellIndex;
}


@end


@implementation IBMeetingTab



#pragma mark - View Methods


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        [[NSBundle mainBundle] loadNibNamed:@"IBMeetingTab" owner:self options:nil];
        
        [self addSubview:self.view];
        
        [self viewDidLoad];
        
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [[NSBundle mainBundle] loadNibNamed:@"IBMeetingTab" owner:self options:nil];
        
        self.bounds = self.view.bounds;
        
        [self addSubview:self.view];
        
        [self viewDidLoad];
        
    }
    return self;
}



- (void)viewDidLoad
{
    _meetingList = nil;
    _noOfMeetings = 0;
    
    self.view.backgroundColor = [UIColor clearColor];
    self.gridContainer.backgroundColor = [UIColor clearColor];
    
    IBCoverView *coverView = [[IBCoverView alloc] initWithFrame:CGRectMake(0,0,242,313)];
    [self.largeCoverView addSubview:coverView];
    _largeCoverView = coverView;
    
    [self initGridView];
    
    [self showLargeCoverView:YES];
    
}

-(void)initGridView
{
    _colorIndex = 0;
    _selectedIndex = -1;
    _currentSelectedCellIndex = -1;
    _currentSelectedCompanyMeeting = nil;
    
    NSInteger spacing = 1;
    
    GMGridView *gmGridView = [[GMGridView alloc] initWithFrame:self.gridContainer.bounds];
    gmGridView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    gmGridView.backgroundColor = [UIColor clearColor];
    [self.gridContainer addSubview:gmGridView];
    _gmGridView = gmGridView;
    
    _gmGridView.style = GMGridViewStyleSwap;
    _gmGridView.itemSpacing = spacing;
    _gmGridView.minEdgeInsets = UIEdgeInsetsMake(spacing, spacing, spacing, spacing);
    _gmGridView.centerGrid = NO;
    _gmGridView.actionDelegate = self;
    _gmGridView.dataSource = self;
    
    //_gmGridView.mainSuperView = self.navigationController.view;
}

- (void)viewDidUnload
{
    _gmGridView = nil;
}


-(void)showLargeCoverView:(BOOL)hide
{
    [self.viewDocumentButton setHidden:hide];
    [self.viewCalenderButton setHidden:hide];
    [self.dateLabel setHidden:hide];
    [self.locationLabel setHidden:hide];
    [self.apologiesLabel setHidden:hide];
    [self.splitterImage setHidden:hide];
    [self.largeCoverView setBackgroundColor:[UIColor clearColor]];
    [_largeCoverView setHidden:hide];
    [_gmGridView setHidden:hide];
}

- (IBAction)tapViewDocument:(id)sender {
    
    if (self.delegate)
        [self.delegate companyMeetingSelected:_currentSelectedCompanyMeeting];
    
}

-(void)setMeetingList:(NSArray *)meetingList company:(Company*)company
{
    _meetingList = meetingList;
    _company = company;
    _noOfMeetings = (int)_meetingList.count;
    
    
    //set the coverStyleID
    for (Meeting *meeting in _meetingList)
    {
        meeting.coverStyleID = [NSNumber numberWithInt:[self getNextColorStyleID]];
    }
    
    [self reloadGrid];
    
    
    // set default top cover
    if (_noOfMeetings > 0) {
        
        Meeting * meeting = _meetingList[0];
        
        
        _currentSelectedCellIndex = 0;
        _currentSelectedCompanyMeeting = meeting;
        
        [self setGMGridViewCellAlpha:0 coverStyleID:[meeting.coverStyleID intValue] alpha:1];
        [self selectMeeting:meeting colorStyleID:[meeting.coverStyleID intValue]];
        
        
        [self showLargeCoverView:NO];
    
    } else {
        
        [self showLargeCoverView:YES];
    }
    
}


-(void)selectMeeting:(Meeting *)meeting colorStyleID:(int)colorStyleID
{
    [_largeCoverView initCoverWithHeaderText:meeting.packName.uppercaseString
                                   subLabel1:meeting.startDateTime.uppercaseString
                                   subLabel2:meeting.location.uppercaseString
                                  footerText:_company.companyName.uppercaseString
                                       color:[self getNextUIColor:colorStyleID alpha:1]];
    
    [self.dateLabel setText:meeting.startDateTime.uppercaseString];
    [self.locationLabel setText:meeting.location.uppercaseString];
    [self.apologiesLabel setText:[NSString stringWithFormat:@"Apologies: %@", meeting.apologies]];
    
}




#pragma mark - Meeting Grid View Functions

-(void)reloadGrid
{
    _colorIndex = 0;
    [_gmGridView reloadData];
}


//////////////////////////////////////////////////////////////
#pragma mark GMGridViewDataSource
//////////////////////////////////////////////////////////////

- (NSInteger)numberOfItemsInGMGridView:(GMGridView *)gridView
{
    return _noOfMeetings;
}

- (CGSize)GMGridView:(GMGridView *)gridView sizeForItemsInInterfaceOrientation:(UIInterfaceOrientation)orientation
{
    return CGSizeMake(180, 250);
}

- (GMGridViewCell *)GMGridView:(GMGridView *)gridView cellForItemAtIndex:(NSInteger)index
{
    //NSLog(@"Creating view indx %d", index);
    
    CGSize size = [self GMGridView:gridView sizeForItemsInInterfaceOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
    
    GMGridViewCell *cell = [gridView dequeueReusableCell];
    
    if (!cell)
    {
        // create new cell
        
        cell = [[GMGridViewCell alloc] init];
        cell.tag = [self getNextColorStyleID];
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
        view.backgroundColor = [UIColor clearColor];
        view.layer.masksToBounds = NO;
        view.layer.cornerRadius = 8;
        
        cell.contentView = view;
    }
    
    [[cell.contentView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    
    // set content cell
    
    if (_meetingList.count > index){
        
        Meeting * meeting = _meetingList[index];
        
        IBSmallCoverView *coverView = [[IBSmallCoverView alloc] initWithFrame:CGRectMake(0,0,163, 216)];
        [coverView initCoverWithHeaderText:meeting.packName.uppercaseString
                                 subLabel1:meeting.startDateTime.uppercaseString
                                 subLabel2:meeting.location.uppercaseString
                                footerText:_company.companyName.uppercaseString
                                     color:[self getNextUIColor:[meeting.coverStyleID intValue] alpha:COVER_STYLE_ALPHA]];
        
        
        
        [cell.contentView addSubview:coverView];
        
    }
    
    
    //add background
    //UIImageView *background = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"blockimage.png"]];
    //background.frame = CGRectMake(0, 0, size.width, size.height);
    //[cell.contentView addSubview:background];
    //[cell.contentView sendSubviewToBack:background];
    //cell.contentView.contentMode = UIViewContentModeScaleAspectFit;
    
    
    
    return cell;
}



//////////////////////////////////////////////////////////////
#pragma mark GMGridViewActionDelegate
//////////////////////////////////////////////////////////////

-(void)setGMGridViewCellAlpha:(NSInteger)position coverStyleID:(int)coverStyleID alpha:(CGFloat)alpha
{
    GMGridViewCell *cell = [_gmGridView cellForItemAtIndex:position];
    
    for (UIView *v in cell.contentView.subviews)
    {
        if([v isKindOfClass:[IBSmallCoverView class]])
        {
            [((IBSmallCoverView *)v) setCoverBackgroundColor:[self getNextUIColor:coverStyleID alpha:alpha]];
        }
    }
}

- (void)GMGridView:(GMGridView *)gridView didTapOnItemAtIndex:(NSInteger)position
{
    NSLog(@"Did tap at index %d", (int)position);
    
    Meeting * meeting = _meetingList[(int)position];
    
    if (_currentSelectedCellIndex > -1)
    {
        //change the alpha channel for the previous cell
        [self setGMGridViewCellAlpha:_currentSelectedCellIndex coverStyleID:[_currentSelectedCompanyMeeting.coverStyleID intValue] alpha:COVER_STYLE_ALPHA];
    }
    
    //change the alpha channel for the current cell
    [self setGMGridViewCellAlpha:position coverStyleID:[meeting.coverStyleID intValue] alpha:1];

    
    _currentSelectedCellIndex = (int)position;
    _currentSelectedCompanyMeeting = meeting;
    
    [self selectMeeting:meeting colorStyleID:[meeting.coverStyleID intValue]];
}

- (void)GMGridViewDidTapOnEmptySpace:(GMGridView *)gridView
{
    NSLog(@"Tap on empty space");
}



-(int)getNextColorStyleID
{
    if (_colorIndex + 1 > 5)
    {
        _colorIndex = 1;
    }
    else
    {
        _colorIndex++;
    }
    
    return _colorIndex;
}

-(UIColor*)getNextUIColor:(int)colorIndex alpha:(CGFloat)alpha
{
    
    /*
     red = [UIColor colorWithRed:149.0/255.0 green:26.0/255.0  blue:29.0/255.0  alpha:0.4];
     purple = [UIColor colorWithRed:93.0/255.0 green:62.0/255.0  blue:180.0/255.0  alpha:0.4];
     blue = [UIColor colorWithRed:54.0/255.0 green:146.0/255.0  blue:241.0/255.0  alpha:0.4];
     green = [UIColor colorWithRed:103.0/255.0 green:174.0/255.0  blue:31.0/255.0  alpha:0.4];
     */
    
    if (colorIndex == 1) {
        return [UIColor colorWithRed:93.0/255.0 green:62.0/255.0  blue:180.0/255.0  alpha:alpha];
    }
    
    else if (colorIndex == 2) {
        return [UIColor colorWithRed:149.0/255.0 green:26.0/255.0  blue:29.0/255.0  alpha:alpha];
    }
    
    else if (colorIndex == 3) {
        return [UIColor colorWithRed:54.0/255.0 green:146.0/255.0  blue:241.0/255.0  alpha:alpha];
    }
    
    else if (colorIndex == 4) {
        return [UIColor colorWithRed:103.0/255.0 green:174.0/255.0  blue:31.0/255.0  alpha:alpha];
    }
    else {
        return [UIColor colorWithRed:93.0/255.0 green:62.0/255.0  blue:180.0/255.0  alpha:alpha];
    }
}




@end
