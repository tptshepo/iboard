//
//  IBAnnotation.m
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/04/28.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import "IBAnnotation.h"
#import "IBCommonKey.h"
#import "NSArray+Enumerable.h"

@interface IBAnnotation ()

@property (nonatomic, assign, readwrite) NSInteger documentAnnotationID;
@property (nonatomic, assign, readwrite) NSInteger documentID;
@property (nonatomic, assign, readwrite) NSInteger pageNo;
@property (nonatomic, retain, readwrite) IBAnnotationJSONData *jSONData;

@end


@implementation IBAnnotation


-(instancetype)initWithDocumentAnnotationID:(NSInteger)documentAnnotationID
                                 documentID:(NSInteger)documentID
                                     pageNo:(NSInteger)pageNo
                                   jSONData:(IBAnnotationJSONData *)jSONData
{
    
    
    if ((self = [super init])) {
        
        self.documentAnnotationID = documentAnnotationID;
        self.documentID = documentID;
        self.pageNo = pageNo;
        self.jSONData = jSONData;
        
    }
    
    return self;
    
    
}


- (instancetype)initWithDictionary:(NSDictionary *)dict
{
 
   IBAnnotationJSONData *data = [[IBAnnotationJSONData alloc] initWithDictionary:dict[IBJSONDataKey]];
    
    return [self initWithDocumentAnnotationID:[[NSString stringWithFormat:@"%@",dict[IBDocumentAnnotationIDKey]] intValue]
                                   documentID:[[NSString stringWithFormat:@"%@",dict[IBDocumentIDKey]] intValue]
                                       pageNo:[[NSString stringWithFormat:@"%@",dict[IBPageNoKey]] intValue]
                                     jSONData:data
            ];
    
}

- (NSDictionary *)dictionaryRepresentation
{

    return @{
             IBDocumentAnnotationIDKey: [NSNumber numberWithInteger:self.documentAnnotationID],
             IBDocumentIDKey: [NSNumber numberWithInteger:self.documentID],
             IBPageNoKey: [NSNumber numberWithInteger:self.pageNo],
             IBJSONDataKey: self.jSONData.dictionaryRepresentation,
             };
}

- (NSString *)description
{
    
    return [NSString stringWithFormat:@"<%@: 0x%x \
            DocumentAnnotationID=%d \
            DocumentID=%d \
            PageNo=%d \
            JSONData=%@ \
            ",
            NSStringFromClass([self class]),
            (unsigned int)self,
            (int)self.documentAnnotationID,
            (int)self.documentID,
            (int)self.pageNo,
            self.jSONData
            ];
}


@end
