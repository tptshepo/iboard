//
//  Meeting.h
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/06/28.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Agenda, Company;

@interface Meeting : NSManagedObject

@property (nonatomic, retain) NSNumber * companyMeetingID;
@property (nonatomic, retain) NSString * startDateTime;
@property (nonatomic, retain) NSString * endDateTime;
@property (nonatomic, retain) NSString * location;
@property (nonatomic, retain) NSString * apologies;
@property (nonatomic, retain) NSNumber * showAgendaItemNumberInd;
@property (nonatomic, retain) NSNumber * packType;
@property (nonatomic, retain) NSNumber * documentPackID;
@property (nonatomic, retain) NSString * packName;
@property (nonatomic, retain) NSString * packDescription;
@property (nonatomic, retain) NSString * coverNote;
@property (nonatomic, retain) NSNumber * coverStyleID;
@property (nonatomic, retain) NSNumber * allowEmail;
@property (nonatomic, retain) NSNumber * allowPrint;
@property (nonatomic, retain) NSNumber * isDeletedState;
@property (nonatomic, retain) NSNumber * userID;
@property (nonatomic, retain) Company *company;
@property (nonatomic, retain) NSSet *agendas;
@end

@interface Meeting (CoreDataGeneratedAccessors)

- (void)addAgendasObject:(Agenda *)value;
- (void)removeAgendasObject:(Agenda *)value;
- (void)addAgendas:(NSSet *)values;
- (void)removeAgendas:(NSSet *)values;

@end
