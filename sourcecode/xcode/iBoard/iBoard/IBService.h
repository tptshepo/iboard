//
//  IBService.h
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/04/11.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class IBProfile;
@class IBCompany;
@class IBDocumentPackData;

/**
 * The notification name posted when an attempt has been made to retrieve
 * an authenticated resource and either no stored credentials were found,
 * or the current credentials are invalid.
 */
extern NSString * const IBServiceAuthRequiredNotification;

typedef void (^IBServiceSuccess)(NSData *data, NSURLResponse *response);

/**
 * The common callback block signature for remote calls that fail
 */
typedef void (^IBServiceFailure)(NSError *error, NSInteger statusCode);

/**
 * A base class defining a service that encapsulates access to the backend REST
 * server. All methods return immediately and the callback blocks are invoked
 * asynchronously (on the main thread) depending on success or failure of the operation
 *
 * Each operation returns a unique identifier for that operation that can later
 * be canceled with a call to -cancelRequestWithIdentifier:
 */

@interface IBService : NSObject


#pragma mark - Singleton access

+ (IBService *)sharedInstance;

-(BOOL)isPasswordSaved;

#pragma mark - User Authentication

- (NSString *)signInWithUserName:(NSString *)userName
                        password:(NSString *)password
                       deviceUID:(NSString *)deviceUID
                      deviceType:(NSString *)deviceType
                   deviceVersion:(NSString *)deviceVersion
                       otherInfo:(NSString *)otherInfo
                       serverURL:(NSURL *)serverURL
                    savePassword:(BOOL)savePassword
                         success:(void(^)(IBProfile *profile))success
                         failure:(IBServiceFailure)failure;

- (NSString *)signoutUserWithSuccess:(void(^)())success
                             failure:(IBServiceFailure)failure;

/**
 * Indicates if the user is currently signed-in with the service.
 */
- (BOOL)isUserSignedIn;

/**
 * Returns the currently signed-in user or nil of -isUserSignedIn returns NO
 */
- (IBProfile *)currentUser;

/**
 * The current server this service instance is pointed at. This
 * method will be nil if -isUserSignedIn returns NO
 */
- (NSURL *)serverRoot;

#pragma mark - Cache

-(void)clearCache;


#pragma mark - Save Document Annotation


- (NSString *)saveDocumentAnnotationWithDictionary:(NSDictionary *)bodyDict
                        success:(void(^)(NSData*))success
                        failure:(IBServiceFailure)failure;

- (NSString *)getDocumentAnnotations:(NSString *)documentID
                        success:(void(^)(NSData*))success
                        failure:(IBServiceFailure)failure;

- (NSString *)deleteDocumentAnnotation:(NSString *)documentAnnotationID
                             success:(void(^)(NSData*))success
                             failure:(IBServiceFailure)failure;

#pragma mark - Get Document


- (NSString *)getDocumentWithID:(NSString *)documentID
                     success:(void(^)(NSData*))success
                     failure:(IBServiceFailure)failure;


#pragma mark - Get Image


- (NSString *)getImageWithID:(NSString *)imageID
                     success:(void(^)(UIImage*))success
                     failure:(IBServiceFailure)failure;


#pragma mark - Get Companies


- (NSString *)getCompaniesSuccess:(void(^)(NSArray*))success
                          failure:(IBServiceFailure)failure;


#pragma mark - Get Documents Pack


- (NSString *)getDocumentsPacksWithComapnyID:(int)companyID
                                     success:(void(^)(IBDocumentPackData *documentPackData))success
                                     failure:(IBServiceFailure)failure;




/**
 * Cancels the request matching the given identifier. If the operation has already
 * completed, the result is a no-op. If no matching operation can be found, the
 * result is a no-op.
 * @param identifier The ID of the request to cancel
 */
- (void)cancelRequestWithIdentifier:(NSString *)identifier;


@end





@interface IBService (SubclassRequirements)

- (NSString *)submitRequestWithURL:(NSURL *)URL
                            method:(NSString *)httpMethod
                              body:(NSDictionary *)bodyDict
                    expectedStatus:(NSInteger)expectedStatus
                           success:(IBServiceSuccess)success
                           failure:(IBServiceFailure)failure;

- (void)cancelRequestWithIdentifier:(NSString *)identifier;

- (void)resendRequestsPendingAuthentication;

@end
