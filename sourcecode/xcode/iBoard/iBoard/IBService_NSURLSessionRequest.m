//
//  IBService_NSURLSessionRequest.m
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/04/11.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import "IBService_NSURLSessionRequest.h"


#import "NSHTTPURLResponse+IBoardExtensions.h"

@interface IBService_NSURLSessionRequest ()

@property (nonatomic, strong) NSURLSession *session;
@property (nonatomic, strong) NSURLSessionDataTask *task;
@property (nonatomic, strong) NSString *requestIdentifier;

@end

@implementation IBService_NSURLSessionRequest

- (instancetype)initWithRequest:(NSURLRequest *)request
                   usingSession:(NSURLSession *)session
                 expectedStatus:(NSInteger)expectedStatus
                        success:(IBServiceSuccess)success
                        failure:(IBServiceFailure)failure
                       delegate:(id<IBService_NSURLSessionRequestDelegate>)delegate
{
    if ((self = [super init])) {

        self.URLRequest = request;
        self.session = session;
        self.expectedStatus = expectedStatus;
        self.successBlock = success;
        self.failureBlock = failure;
        self.delegate = delegate;
        
        self.requestIdentifier = [[NSUUID UUID] UUIDString];
        self.task = [self createDataTask];
        [self.task resume];
    }
    
    return self;
}

- (void)cancel
{
    [self.task cancel];
    self.task = nil;
}

- (void)restart
{
    [self cancel];
    self.task = [self createDataTask];
    [self.task resume];
}

#pragma mark - Private helpers

- (NSURLSessionDataTask *)createDataTask
{
    __weak typeof(self) weakSelf = self;
    
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:self.URLRequest
                                                 completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                
                                                     NSHTTPURLResponse *HTTPResponse = (NSHTTPURLResponse *)response;
                                                     NSMutableURLRequest *mutableRequest = (NSMutableURLRequest *)weakSelf.URLRequest;
                                                     
                                                     //NSLog(@"%@", mutableRequest.URL.path);
                                                     
                                                     if (HTTPResponse.statusCode == weakSelf.expectedStatus) {
                                                         NSLog(@"%@ %@ %li SUCCESS", [mutableRequest HTTPMethod], [mutableRequest URL], (long)weakSelf.expectedStatus);
                                                         
                                                         dispatch_async(dispatch_get_main_queue(), ^{
                                                             weakSelf.successBlock(data, response);
                                                             [weakSelf.delegate sessionRequestDidComplete:weakSelf];
                                                         });
                                                     }
                                                     else if (HTTPResponse.statusCode == 401) {
                                                         dispatch_async(dispatch_get_main_queue(), ^{
                                                             NSLog(@"sessionRequestRequiresAuthentication");
                                                             
                                                             [weakSelf.delegate sessionRequestRequiresAuthentication:weakSelf];
                                                         });
                                                     }
                                                     else if (HTTPResponse.statusCode == 403 && [mutableRequest.URL.path rangeOfString:@"/Api/Login"].location == NSNotFound) {
                                                         dispatch_async(dispatch_get_main_queue(), ^{
                                                             NSLog(@"sessionRequestRequiresAuthentication");
                                                             
                                                             [weakSelf.delegate sessionRequestRequiresAuthentication:weakSelf];
                                                         });
                                                     }
                                                     else {
                                                         NSLog(@"%@ %@ %li INVALID STATUS CODE", [mutableRequest HTTPMethod], [mutableRequest URL], (long)HTTPResponse.statusCode);
                                                         
                                                         NSString *message = [HTTPResponse errorMessageWithData:data];
                                                         
                                                         if (message == nil){
                                                             message = @"";
                                                         }
                                                         
                                                         NSError *error = [NSError errorWithDomain:@"IBService"
                                                                                              code:HTTPResponse.statusCode
                                                                                          userInfo:@{ NSLocalizedDescriptionKey: message }];
                                                         
                                                         dispatch_async(dispatch_get_main_queue(), ^{
                                                             weakSelf.failureBlock(error, HTTPResponse.statusCode);
                                                             [weakSelf.delegate sessionRequestFailed:weakSelf error:error];
                                                         });
                                                     }
                                                 }];
    
    return task;
}

@end