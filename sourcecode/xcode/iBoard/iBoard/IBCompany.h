//
//  IBCompany.h
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/04/13.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>


#import "IBSerializable.h"


@interface IBCompany : NSObject <IBSerializable>

@property (nonatomic, copy, readonly) NSString *companyID;
@property (nonatomic, copy, readonly) NSString *companyName;
@property (nonatomic, copy, readonly) NSString *requireOnlineSignInEveryXDays;
@property (nonatomic, copy, readonly) NSString *reLoginInactivityMinutes;
@property (nonatomic, copy, readonly) NSString *allowEmail;
@property (nonatomic, copy, readonly) NSString *allowPrint;
@property (nonatomic, copy, readonly) NSString *companyUserLevelID;
@property (nonatomic, copy, readonly) NSString *isDeleted;
@property (nonatomic, copy, readonly) NSString *imageIDLogo;

/**
 * Initialize a new instance
 */
- (instancetype)initWithCompanyID:(NSString *)companyID
                      companyName:(NSString *)companyName
    requireOnlineSignInEveryXDays:(NSString *)requireOnlineSignInEveryXDays
         reLoginInactivityMinutes:(NSString *)reLoginInactivityMinutes
                       allowEmail:(NSString *)allowEmail
                       allowPrint:(NSString *)allowPrint
               companyUserLevelID:(NSString *)companyUserLevelID
                        isDeleted:(NSString *)isDeleted
                      imageIDLogo:(NSString *)imageIDLogo;

/**
 * Create a new instance from a dictionary
 */
- (instancetype)initWithDictionary:(NSDictionary *)dict;

/**
 * Returns a dictionary representation of the model suitable
 * for JSON serialization
 */
- (NSDictionary *)dictionaryRepresentation;

@end
