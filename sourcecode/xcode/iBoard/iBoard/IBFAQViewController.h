//
//  IBFAQViewController.h
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/07/13.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IBFAQViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *documentContainer;


- (IBAction)tapBack:(id)sender;

@end
