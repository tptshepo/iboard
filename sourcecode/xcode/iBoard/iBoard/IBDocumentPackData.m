//
//  IBDocumentPackData.m
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/04/28.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import "IBDocumentPackData.h"
#import "IBCommonKey.h"
#import "NSArray+Enumerable.h"
#import "IBDocumentPack.h"
#import "IBCompanyMeeting.h"


@interface IBDocumentPackData ()

@property (nonatomic, copy, readwrite) NSArray *documentPackList;
@property (nonatomic, copy, readwrite) NSArray *meetingList;
@property (nonatomic, copy, readwrite) NSString *serverTimestamp;

@end


@implementation IBDocumentPackData


-(instancetype)initWithDocumentPackList:(NSArray *)documentPackList
                            meetingList:(NSArray *)meetingList
                        serverTimestamp:(NSString *)serverTimestamp
{
    
    
    if ((self = [super init])) {
        
        self.documentPackList = documentPackList;
        self.meetingList = meetingList;
        self.serverTimestamp = serverTimestamp;
        
    }
    
    return self;
}


- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    
    NSArray *documentPackListFromDict = [dict[IBDocumentPackListKey] mappedArrayWithBlock:^id(id obj) {
        return [[IBDocumentPack alloc] initWithDictionary:obj];
    }];
    
    
    NSArray *meetingListFromDict = [dict[IBMeetingListKey] mappedArrayWithBlock:^id(id obj) {
        return [[IBCompanyMeeting alloc] initWithDictionary:obj];
    }];
    
    return [self initWithDocumentPackList:documentPackListFromDict
                              meetingList:meetingListFromDict
                          serverTimestamp:dict[IBServerTimestampKey]
            ];
    
}

-(instancetype)initWithDictionary:(NSArray *)nsArrayDocumentPackList
               nsArrayMeetingList:(NSArray *)nsArrayMeetingList
                  serverTimestamp:(NSString *)serverTimestamp
{
    
    NSArray *documentPackListFromDict = [nsArrayDocumentPackList mappedArrayWithBlock:^id(id obj) {
        return [[IBDocumentPack alloc] initWithDictionary:obj];
    }];
    
    
    NSArray *meetingListFromDict = [nsArrayMeetingList mappedArrayWithBlock:^id(id obj) {
        return [[IBCompanyMeeting alloc] initWithDictionary:obj];
    }];
    
    return [self initWithDocumentPackList:documentPackListFromDict
                              meetingList:meetingListFromDict
                          serverTimestamp:serverTimestamp
            ];
    
}



- (NSDictionary *)dictionaryRepresentation
{
    NSArray *documentListListFromDict = [self.documentPackList mappedArrayWithBlock:^id(id obj) {
        return [[IBDocumentPack alloc] initWithDictionary:obj];
    }];
    
    NSArray *meetingListFromDict = [self.meetingList mappedArrayWithBlock:^id(id obj) {
        return [[IBCompanyMeeting alloc] initWithDictionary:obj];
    }];
    
    return @{
             IBDocumentPackListKey: documentListListFromDict,
             IBMeetingListKey: meetingListFromDict,
             IBServerTimestampKey: self.serverTimestamp,
             };
}

- (NSString *)description
{
    
    return [NSString stringWithFormat:@"<%@: 0x%x \
            ServerTimestamp=%@ \
            ",
            NSStringFromClass([self class]),
            (unsigned int)self,
            self.serverTimestamp
            ];
}


@end