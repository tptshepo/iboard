//
//  IBAgendaView.m
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/05/24.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import "IBAgendaView.h"
#import "GMGridView.h"
#import "IBAgendaItemView.h"

#import "Meeting.h"
#import "Agenda.h"
#import "Document.h"

#import "IBFileManager.h"
#import "ReaderDocument.h"

@interface IBAgendaView () <GMGridViewDataSource, GMGridViewActionDelegate>

{
    __gm_weak GMGridView *_gmGridView;
    
    int _colorIndex;
    int _selectedIndex;
    
    Agenda * _selectedMeetingAgendaItem;
    Meeting * _comapnyMeeting;
    NSArray * _meetingAgendaList;
    
    int _noOfItems;
    
    int _currentSelectedCellIndex;
}


@end


@implementation IBAgendaView


#pragma mark - View Methods


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        [[NSBundle mainBundle] loadNibNamed:@"IBAgendaView" owner:self options:nil];
        
        [self addSubview:self.view];
        
        [self viewDidLoad];
        
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [[NSBundle mainBundle] loadNibNamed:@"IBAgendaView" owner:self options:nil];
        
        self.bounds = self.view.bounds;
        
        [self addSubview:self.view];
        
        [self viewDidLoad];
        
    }
    return self;
}



- (void)viewDidLoad
{
    _noOfItems = 0;
    
    //self.view.backgroundColor = [UIColor clearColor];
    //self.gridContainer.backgroundColor = [UIColor clearColor];
    
    [self initGridView];
    
}

-(void)initGridView
{
    
    //NSLog(@"Load Agenda View");
    
    _colorIndex = 0;
    _selectedIndex = -1;
    _currentSelectedCellIndex = -1;
    
    NSInteger spacing = 1;
    
    GMGridView *gmGridView = [[GMGridView alloc] initWithFrame:self.gridContainer.bounds];
    //gmGridView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    gmGridView.backgroundColor = [UIColor clearColor];
    [self.gridContainer addSubview:gmGridView];
    _gmGridView = gmGridView;
    
    _gmGridView.style = GMGridViewStyleSwap;
    _gmGridView.itemSpacing = spacing;
    _gmGridView.minEdgeInsets = UIEdgeInsetsMake(spacing, spacing, spacing, spacing);
    _gmGridView.centerGrid = NO;
    _gmGridView.actionDelegate = self;
    _gmGridView.dataSource = self;
    
    //_gmGridView.mainSuperView = self.navigationController.view;
}


- (void)viewDidUnload
{
    _gmGridView = nil;
}


-(void)setMeetingAgendaItemList:(NSArray *)meetingAgendaList companyMeeting:(Meeting*)companyMeeting
{
    _meetingAgendaList = meetingAgendaList;
    _comapnyMeeting = companyMeeting;
    
    _noOfItems = (int)_meetingAgendaList.count;
    
    [self reloadGrid];
}

-(NSNumber*)getPageCount:(Document*)doc
{
    NSString * filePath = [[IBFileManager sharedInstance] getDocumentFilePathWithID:[doc.documentID stringValue]];
    ReaderDocument * document = [ReaderDocument withDocumentFilePath:filePath password:@""];
    assert(document != nil); // Must have a valid ReaderDocument
    return document.pageCount;
}


#pragma mark - Meeting Grid View Functions

-(void)reloadGrid
{
    _colorIndex = 0;
    [_gmGridView reloadData];
}


//////////////////////////////////////////////////////////////
#pragma mark GMGridViewDataSource
//////////////////////////////////////////////////////////////

- (NSInteger)numberOfItemsInGMGridView:(GMGridView *)gridView
{
    return _noOfItems;
}

- (CGSize)GMGridView:(GMGridView *)gridView sizeForItemsInInterfaceOrientation:(UIInterfaceOrientation)orientation
{
    return CGSizeMake(312, 69);
}

- (GMGridViewCell *)GMGridView:(GMGridView *)gridView cellForItemAtIndex:(NSInteger)index
{
    CGSize size = [self GMGridView:gridView sizeForItemsInInterfaceOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
    
    GMGridViewCell *cell = [gridView dequeueReusableCell];
    
    if (!cell)
    {
        cell = [[GMGridViewCell alloc] init];
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
        view.backgroundColor = [UIColor clearColor];
        view.layer.masksToBounds = NO;
        view.layer.cornerRadius = 8;
        
        cell.contentView = view;
    }
    
    [[cell.contentView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    
    // set content cell
    Agenda * aitem = _meetingAgendaList[index];
    IBAgendaItemView *agendaItem = [[IBAgendaItemView alloc] initWithFrame:CGRectMake(0, 0, 312, 69)];
    [agendaItem setTitle:[NSString stringWithFormat:@"%@", aitem.itemName] pageTitle:@"No document attached"];
    
    if (aitem.documents != nil){
        if (aitem.documents.count > 0){
            Document * doc;
            for (Document *d in aitem.documents) {
                doc = d;
            }
            [agendaItem setTitle:[NSString stringWithFormat:@"%@", aitem.itemName]
                       pageTitle:[NSString stringWithFormat:@"%@ pages", [self getPageCount:doc]]];
        }
    }
    
    [cell.contentView addSubview:agendaItem];
    
    return cell;
}



//////////////////////////////////////////////////////////////
#pragma mark GMGridViewActionDelegate
//////////////////////////////////////////////////////////////


- (void)GMGridView:(GMGridView *)gridView didTapOnItemAtIndex:(NSInteger)position
{
  //  NSLog(@"Did tap at index %d", (int)position);
    
    _selectedMeetingAgendaItem = _meetingAgendaList[(int)position];
    _selectedIndex = (int)position;
    
    if (self.delegate)
        [self.delegate agendaItemSelected:_selectedMeetingAgendaItem];
    
}

- (void)GMGridViewDidTapOnEmptySpace:(GMGridView *)gridView
{
    //NSLog(@"Tap on empty space");
}

@end
