//
//  IBAnnotationTextView.h
//  DrawPad
//
//  Created by Tshepo Mgaga on 2015/06/14.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol IBAnnotationCommentViewDelegate;

@interface IBAnnotationCommentView : UIView


- (instancetype)initWithFrame:(CGRect)frame restoreMode:(BOOL)restoreMode pRect:(CGRect)pRect;

-(BOOL)hitTest:(CGPoint)point;

-(void)showText;
-(void)hideText;
-(NSString*)getText;
-(void)setText:(NSString*)text;

@property (nonatomic, weak) id<IBAnnotationCommentViewDelegate> delegate;
@property (nonatomic, readwrite) BOOL isDeleted;


@end



@protocol IBAnnotationCommentViewDelegate

@required

-(void)deleteAnnotationCommentView:(IBAnnotationCommentView*)me;


@end