//
//  ProfileViewController.h
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/04/04.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IBLoginViewController.h"

@interface IBProfileViewController : UIViewController <IBLoginViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIView *gridContainer;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *profileImage;

@property (weak, nonatomic) IBOutlet UIView *waitContainer;


- (IBAction)buttonLogout:(id)sender;
- (IBAction)buttonSupport:(id)sender;
- (IBAction)unwindToProfile:(UIStoryboard *)segue;


@end
