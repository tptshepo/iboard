//
//  IBDocumentViewController.h
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/05/17.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IBDocumentListView.h"
#import "ReaderDocument.h"


@class IBDocumentViewController;


@interface IBDocumentViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *documentContainer;
@property (nonatomic) DocumentPackDocument *documentPackDocument;
@property (weak, nonatomic) IBOutlet UILabel *titleView;

@property (weak, nonatomic) IBOutlet UIView *waitContainer;

- (IBAction)backToDocumentList:(id)sender;

@end
