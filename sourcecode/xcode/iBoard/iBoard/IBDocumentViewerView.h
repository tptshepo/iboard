//
//  IBDocumentViewerView.h
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/05/31.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReaderDocument.h"

@protocol IBDocumentViewerViewDelegate;

@interface IBDocumentViewerView : UIView

- (void)loadDocument:(NSString*)documentID canCallServer:(BOOL)canCallServer;
- (void)cleanUp;
- (void)setAnnotationMode:(BOOL)state;
-(void)setSwipeEnabled:(BOOL)state;

@property (nonatomic, weak) id<IBDocumentViewerViewDelegate> delegate;


//@property (strong, nonatomic) IBOutlet UIView *documentContainer;

@end


@protocol IBDocumentViewerViewDelegate

@required

- (void)singleTap;

@end