//
//  Utils.m
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/06/27.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import "Utils.h"

@implementation Utils

+(NSNumber *)stringToNSnumberIntFromDictionary:(NSString *)string
{
    if ([string isKindOfClass:[NSString class]])
    {
        if ([string length] == 0)
        {
            return [NSNumber numberWithInt:0];
        }
        
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        f.numberStyle = NSNumberFormatterDecimalStyle;
        NSNumber *number = [f numberFromString:string];
        return number;
    }
    
    if ([string isKindOfClass:[NSNumber class]])
    {
        return [NSNumber numberWithInt:[string intValue]];
    }
    
    
    return [NSNumber numberWithInt:0];
}

+(NSNumber *)stringToNSNumberBoolFromDictionary:(NSString *)string
{
    if ([string isKindOfClass:[NSNull class]])
    {
        return [NSNumber numberWithBool:NO];
    }
    
    BOOL b = [string boolValue];
    return [NSNumber numberWithBool:b];
}

@end
