//
//  ViewController.m
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/03/31.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import "IBLoginViewController.h"
#import "IBLoginTextView.h"
#import "IBProfileViewController.h"
#import "IBService.h"
#import "IBProfile.h"
#import "IBWaitView.h"

#import "IBFileManager.h"

NSString * const API_URL = @"https://mail.singular.co.za/DocumentPacks";


@interface IBLoginViewController ()
{
    BOOL _remeberPassword;
}

@property (nonatomic, strong) IBWaitView *waitView;

@end

@implementation IBLoginViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    _remeberPassword = NO;
    
    
    [self.userNameView setBackgroundColor:[UIColor clearColor]];
    [self.userNameView setTextMode:TEXT_MODE_TEXT];
    [self.userNameView setKeyboardType:UIKeyboardTypeEmailAddress];
    
    [self.passwordView setBackgroundColor:[UIColor clearColor]];
    [self.passwordView setTextMode:TEXT_MODE_PASSWORD];
    
    [self.userNameView setText:@"bwilson@singular.co.za"];
    //[self.userNameView setText:@"Rachel@test.co.za"];
    //[self.userNameView setText:@"bwebber@singular.co.za"];
    //[self.userNameView setText:@"Johnny@test.co.za"];
    [self.passwordView setText:@"Passw0rd"];
    
    
    self.waitView = [[IBWaitView alloc] initWithFrame:CGRectZero];
    [self.waitView setBounds:[self.waitContainer bounds]];
    [self.waitContainer setBackgroundColor:[UIColor clearColor]];
    [self.waitContainer addSubview:self.waitView];
    [self.waitView showWithText:@"Please wait..."];
    
    [self.waitContainer setHidden:YES];
    
}

-(void)showSpinner {
    [self.rememberCheckImgae setEnabled:NO];
    [self.loginButtonView setEnabled:NO];
    [self.waitContainer setHidden:NO];
}

-(void)hideSpinner {
    [self.rememberCheckImgae setEnabled:YES];
    [self.loginButtonView setEnabled:YES];
    [self.waitContainer setHidden:YES];
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buttonRemeber:(id)sender {
    
    if (_remeberPassword) {
        _remeberPassword = NO;
        [self.rememberCheckImgae setBackgroundImage:[UIImage imageNamed: @"remember.png"] forState:UIControlStateNormal];
        
    } else {
        _remeberPassword = YES;
        [self.rememberCheckImgae setBackgroundImage:[UIImage imageNamed: @"remember_checked.png"] forState:UIControlStateNormal];
    }
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
}

- (IBAction)loginButton:(id)sender {
    
    [self showSpinner];
    
    NSString * username = [self.userNameView getText];
    NSString * password = [self.passwordView getText];
    
    
    __weak typeof(self) weakSelf = self;
    
    NSURL *URL = [NSURL URLWithString:API_URL];
    
    
    [[IBService sharedInstance] signInWithUserName:username
                                          password:password
                                         deviceUID:@"iPad"
                                        deviceType:@"Generic"
                                     deviceVersion:@"1.0"
                                         otherInfo:@"None"
                                         serverURL:URL
                                      savePassword:_remeberPassword
                                           success:^(IBProfile *profle) {
                                               NSLog(@"Success Login");
                                               
                                               [weakSelf hideSpinner];
                                               [weakSelf.delegate authenticationViewControllerSucceeded:weakSelf];
                                               
                                           }
                                           failure:^(NSError *error, NSInteger statusCode) {
                                               NSLog(@"Fail Login");
                                               
                                               [weakSelf hideSpinner];
                                               
                                               if (statusCode == 403)
                                               {
                                                   UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Unable to Log-in"
                                                                                                   message:@"Invalid username or password."
                                                                                                  delegate:nil
                                                                                         cancelButtonTitle:@"OK"
                                                                                         otherButtonTitles:nil];
                                                   [alert show];
                                               }
                                               else
                                               {
                                                   
                                                   UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Unable to Log-in"
                                                                                                   message:error.localizedDescription
                                                                                                  delegate:nil
                                                                                         cancelButtonTitle:@"OK"
                                                                                         otherButtonTitles:nil];
                                                   [alert show];
                                               }
                                           }];
    
    
    
}

- (IBAction)tapFAQ:(id)sender {
    
    if ([[IBFileManager sharedInstance] hasDocumentID:@"faq"]) {
        [self performSegueWithIdentifier:@"FAQSegue" sender:nil];
    }
    
}
@end
