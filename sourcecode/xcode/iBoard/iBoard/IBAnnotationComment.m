//
//  IBAnnotationComment.m
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/04/28.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import "IBAnnotationComment.h"
#import "IBCommonKey.h"

@interface IBAnnotationComment ()

@property (nonatomic, copy, readwrite) NSString *pointX;
@property (nonatomic, copy, readwrite) NSString *pointY;
@property (nonatomic, copy, readwrite) NSString *text;

@end


@implementation IBAnnotationComment


-(instancetype)initWithPointX:(NSString *)pointX
                       pointY:(NSString *)pointY
                         text:(NSString *)text
{
    
    
    if ((self = [super init])) {
        
        self.pointX = pointX;
        self.pointY = pointY;
        self.text = text;
        
    }
    
    return self;
    
    
}


- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    return [self initWithPointX:dict[IBPointXKey]
                         pointY:dict[IBPointYKey]
                           text:dict[IBTextKey]
            ];
    
}

- (NSDictionary *)dictionaryRepresentation
{
    return @{
             IBPointXKey: self.pointX,
             IBPointYKey: self.pointY,
             IBTextKey: self.text,
             };
}

- (NSString *)description
{
    
    return [NSString stringWithFormat:@"<%@: 0x%x \
            PointX=%@ \
            PointY=%@ \
            Text=%@ \
            ",
            NSStringFromClass([self class]),
            (unsigned int)self,
            self.pointX,
            self.pointY,
            self.text
            ];
}


@end
