//
//  IBAgendaItemView.m
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/05/30.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import "IBAgendaItemView.h"

@implementation IBAgendaItemView


#pragma mark - View Methods


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        [[NSBundle mainBundle] loadNibNamed:@"IBAgendaItemView" owner:self options:nil];
        
        [self addSubview:self.view];
        
        [self viewDidLoad];
        
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [[NSBundle mainBundle] loadNibNamed:@"IBAgendaItemView" owner:self options:nil];
        
        self.bounds = self.view.bounds;
        
        [self addSubview:self.view];
        
        [self viewDidLoad];
        
    }
    return self;
}


- (void)viewDidLoad
{
 
    
}


-(void)setTitle:(NSString *)title pageTitle:(NSString *)pageTitle
{
    [self.title setText:title];
    [self.pageTitle setText:pageTitle];
}


@end
