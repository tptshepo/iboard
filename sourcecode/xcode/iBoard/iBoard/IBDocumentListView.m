//
//  IBDocumentListView.m
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/05/10.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import "IBDocumentListView.h"

#import "GMGridView.h"
#import "IBCoverView.h"
#import "IBSmallCoverView.h"

#import "DocumentPack.h"
#import "DocumentPackDocument.h"
#import "Company.h"

#define COVER_STYLE_ALPHA 0.3

@interface IBDocumentListView () <GMGridViewDataSource, GMGridViewActionDelegate>

{
    __weak IBCoverView *_largeCoverView;
    __gm_weak GMGridView *_gmGridView;
    
    int _colorIndex;
    int _selectedIndex;
    
    NSArray * _documentList;
    Company * _company;
    DocumentPack * _documentPack;
    
    DocumentPackDocument * _selectedDocumentPackDocument;
    int _noOfDocuments;

    DocumentPackDocument * _currentSelectedDocumentPackDocument;
    int _currentSelectedCellIndex;
}


@end


@implementation IBDocumentListView


#pragma mark - View Methods


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        [[NSBundle mainBundle] loadNibNamed:@"IBDocumentListView" owner:self options:nil];
        
        [self addSubview:self.view];
        
        [self viewDidLoad];
        
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [[NSBundle mainBundle] loadNibNamed:@"IBDocumentListView" owner:self options:nil];
        
        self.bounds = self.view.bounds;
        
        [self addSubview:self.view];
        
        [self viewDidLoad];
        
    }
    return self;
}



- (void)viewDidLoad
{
    _noOfDocuments = 0;
    
    self.view.backgroundColor = [UIColor clearColor];
    self.gridContainer.backgroundColor = [UIColor clearColor];
    
    IBCoverView *coverView = [[IBCoverView alloc] initWithFrame:CGRectMake(0,0,242,313)];
    [coverView hideHeaderText:YES];
    [self.largeCoverView addSubview:coverView];
    _largeCoverView = coverView;
    
    [self initGridView];
    
    [self showLargeCoverView:YES];
}

-(void)initGridView
{
    _colorIndex = 0;
    _selectedIndex = -1;
    _currentSelectedCellIndex = -1;
    _currentSelectedDocumentPackDocument = nil;
    
    NSInteger spacing = 1;
    
    GMGridView *gmGridView = [[GMGridView alloc] initWithFrame:self.view.bounds];
    gmGridView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    gmGridView.backgroundColor = [UIColor clearColor];
    [self.gridContainer addSubview:gmGridView];
    _gmGridView = gmGridView;
    
    _gmGridView.style = GMGridViewStyleSwap;
    _gmGridView.itemSpacing = spacing;
    _gmGridView.minEdgeInsets = UIEdgeInsetsMake(spacing, spacing, spacing, spacing);
    _gmGridView.centerGrid = NO;
    _gmGridView.actionDelegate = self;
    _gmGridView.dataSource = self;
    
    //_gmGridView.mainSuperView = self.navigationController.view;
}


- (void)viewDidUnload
{
    _gmGridView = nil;
}


-(void)setDocumentPackList:(NSArray *)documentList documentPack:(DocumentPack*)documentPack company:(Company*)company
{
    _documentList = documentList;
    _company = company;
    _documentPack = documentPack;
    
    _noOfDocuments = (int)_documentList.count;
    
    //set the coverStyleID
    for (DocumentPackDocument *doc in _documentList)
    {
        doc.coverStyleID = [self getNextColorStyleID];
    }
    
    [self reloadGrid];
    
    // set default top cover
    if (_noOfDocuments > 0) {
        DocumentPackDocument * documentPackDocuemnt = _documentList[0];
        
        _currentSelectedCellIndex = 0;
        _currentSelectedDocumentPackDocument = documentPackDocuemnt;
        
        [self setGMGridViewCellAlpha:0 coverStyleID:documentPackDocuemnt.coverStyleID alpha:1];
        [self selectDocument:documentPackDocuemnt colorStyleID:documentPackDocuemnt.coverStyleID];
        
        [self showLargeCoverView:NO];
        
    } else {
        
        [self showLargeCoverView:YES];
    }
}

-(void)selectDocument:(DocumentPackDocument *)documentPackDocument colorStyleID:(int)colorStyleID
{
    _selectedDocumentPackDocument = documentPackDocument;
    
    [_largeCoverView initCoverWithHeaderText:@""
                                   subLabel1:@""
                                   subLabel2:documentPackDocument.fileName
                                  footerText:documentPackDocument.documentName
                                       color:[self getNextUIColor:colorStyleID alpha:1]];
    
}

-(void)showLargeCoverView:(BOOL)hide
{
    [self.viewDocumentButton setHidden:hide];
    [self.largeCoverView setBackgroundColor:[UIColor clearColor]];
    [_largeCoverView setHidden:hide];
    [_gmGridView setHidden:hide];
    [self.splitterImage setHidden:hide];
}



#pragma mark - Meeting Grid View Functions

-(void)reloadGrid
{
    _colorIndex = 0;
    [_gmGridView reloadData];
}


//////////////////////////////////////////////////////////////
#pragma mark GMGridViewDataSource
//////////////////////////////////////////////////////////////

- (NSInteger)numberOfItemsInGMGridView:(GMGridView *)gridView
{
    return _noOfDocuments;
}

- (CGSize)GMGridView:(GMGridView *)gridView sizeForItemsInInterfaceOrientation:(UIInterfaceOrientation)orientation
{
    return CGSizeMake(180, 250);
}

- (GMGridViewCell *)GMGridView:(GMGridView *)gridView cellForItemAtIndex:(NSInteger)index
{
    CGSize size = [self GMGridView:gridView sizeForItemsInInterfaceOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
    
    GMGridViewCell *cell = [gridView dequeueReusableCell];
    
    if (!cell)
    {
        cell = [[GMGridViewCell alloc] init];
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
        view.backgroundColor = [UIColor clearColor];
        view.layer.masksToBounds = NO;
        view.layer.cornerRadius = 8;
        
        cell.contentView = view;
    }
    
    [[cell.contentView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    
    // set content cell
    
    if (_documentList.count > index){
        
        DocumentPackDocument * documentPackDocument = _documentList[index];
        
        IBSmallCoverView *coverView = [[IBSmallCoverView alloc] initWithFrame:CGRectMake(0,0,163, 216)];
        
        
        [coverView initCoverWithHeaderText:@""
                                 subLabel1:@""
                                 subLabel2:documentPackDocument.fileName
                                footerText:documentPackDocument.documentName
                                     color:[self getNextUIColor:documentPackDocument.coverStyleID alpha:COVER_STYLE_ALPHA]];
        
        [coverView hideHeaderText:YES];
        
        [cell.contentView addSubview:coverView];
    }
    
    
    return cell;
}



//////////////////////////////////////////////////////////////
#pragma mark GMGridViewActionDelegate
//////////////////////////////////////////////////////////////

-(void)setGMGridViewCellAlpha:(NSInteger)position coverStyleID:(int)coverStyleID alpha:(CGFloat)alpha
{
    GMGridViewCell *cell = [_gmGridView cellForItemAtIndex:position];
    
    for (UIView *v in cell.contentView.subviews)
    {
        if([v isKindOfClass:[IBSmallCoverView class]])
        {
            [((IBSmallCoverView *)v) setCoverBackgroundColor:[self getNextUIColor:coverStyleID alpha:alpha]];
        }
    }
}

- (void)GMGridView:(GMGridView *)gridView didTapOnItemAtIndex:(NSInteger)position
{
   // NSLog(@"Did tap at index %d", (int)position);
    
    // update the large cover
    DocumentPackDocument * documentPackDocument = _documentList[(int)position];
    
    if (_currentSelectedCellIndex > -1)
    {
        //change the alpha channel for the previous cell
        [self setGMGridViewCellAlpha:_currentSelectedCellIndex coverStyleID:_currentSelectedDocumentPackDocument.coverStyleID alpha:COVER_STYLE_ALPHA];
    }
    
     //change the alpha channel for the current cell
    [self setGMGridViewCellAlpha:position coverStyleID:documentPackDocument.coverStyleID alpha:1];
    
    _currentSelectedCellIndex = (int)position;
    _currentSelectedDocumentPackDocument = documentPackDocument;
    
    [self selectDocument:documentPackDocument colorStyleID:documentPackDocument.coverStyleID];
}

- (void)GMGridViewDidTapOnEmptySpace:(GMGridView *)gridView
{
    //NSLog(@"Tap on empty space");
}

-(int)getNextColorStyleID
{
    if (_colorIndex + 1 > 5)
    {
        _colorIndex = 1;
    }
    else
    {
        _colorIndex++;
    }
    
    return _colorIndex;
}

-(UIColor*)getNextUIColor:(int)colorIndex alpha:(CGFloat)alpha
{
    
    /*
     red = [UIColor colorWithRed:149.0/255.0 green:26.0/255.0  blue:29.0/255.0  alpha:0.4];
     purple = [UIColor colorWithRed:93.0/255.0 green:62.0/255.0  blue:180.0/255.0  alpha:0.4];
     blue = [UIColor colorWithRed:54.0/255.0 green:146.0/255.0  blue:241.0/255.0  alpha:0.4];
     green = [UIColor colorWithRed:103.0/255.0 green:174.0/255.0  blue:31.0/255.0  alpha:0.4];
     */
    
    if (colorIndex == 1) {
        return [UIColor colorWithRed:93.0/255.0 green:62.0/255.0  blue:180.0/255.0  alpha:alpha];
    }
    
    else if (colorIndex == 2) {
        return [UIColor colorWithRed:149.0/255.0 green:26.0/255.0  blue:29.0/255.0  alpha:alpha];
    }
    
    else if (colorIndex == 3) {
        return [UIColor colorWithRed:54.0/255.0 green:146.0/255.0  blue:241.0/255.0  alpha:alpha];
    }
    
    else if (colorIndex == 4) {
        return [UIColor colorWithRed:103.0/255.0 green:174.0/255.0  blue:31.0/255.0  alpha:alpha];
    }
    else {
        return [UIColor colorWithRed:93.0/255.0 green:62.0/255.0  blue:180.0/255.0  alpha:alpha];
    }
}


- (IBAction)tapViewDocument:(id)sender {
    
    if (self.delegate)
        [self.delegate documentSelected:_selectedDocumentPackDocument];
}
@end
