//
//  ViewController.m
//  GMGridView
//
//  Created by Gulam Moledina on 11-10-09.
//  Copyright (c) 2011 GMoledina.ca. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "IBProfileViewController.h"
#import "IBMeetingViewController.h"
#import "GMGridView.h"
#import "IBService.h"
#import "IBProfile.h"
#import "IBImageStore.h"
#import "IBWaitView.h"
#import "DataStore.h"
#import "DBContext.h"
#import "Company.h"

#define NUMBER_ITEMS_ON_LOAD 9


//////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark ViewController (privates methods)
//////////////////////////////////////////////////////////////

@interface IBProfileViewController () <GMGridViewDataSource, GMGridViewActionDelegate>
{
    __gm_weak GMGridView *_gmGridView;
    
    NSMutableArray *_data;
    __gm_weak NSMutableArray *_currentData;
    NSInteger _lastDeleteItemIndexAsked;
    int _bkImageIndex;
    int _selectedIndex;
    
    int _spinnerCount;
}

@property (nonatomic, strong) IBWaitView *waitView;
@property (nonatomic, strong) NSArray *companies;

@end


//////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark ViewController implementation
//////////////////////////////////////////////////////////////

@implementation IBProfileViewController


- (void)awakeFromNib
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(authenticationRequired:)
                                                 name:IBServiceAuthRequiredNotification
                                               object:nil];
}


//////////////////////////////////////////////////////////////
#pragma mark UIViewController events
//////////////////////////////////////////////////////////////

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _spinnerCount = 0;
    
    self.waitView = [[IBWaitView alloc] initWithFrame:CGRectZero];
    [self.waitView setBounds:[self.waitContainer bounds]];
    [self.waitContainer setBackgroundColor:[UIColor clearColor]];
    [self.waitContainer addSubview:self.waitView];
    [self.waitView showWithText:@"Loading profile..."];
    
    [self hideSpinner];
    
    
    [self initGridView];
    
    
    IBService *service = [IBService sharedInstance];
    
    if(!service.isPasswordSaved){
        [self signOut];
    } else {
        [self getCompanies];
    }
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
}

-(void)initGridView
{
    
    _bkImageIndex = 0;
    _selectedIndex = -1;
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self loadDefaultItems:0];
    
    NSInteger spacing = 10;
    
    GMGridView *gmGridView = [[GMGridView alloc] initWithFrame:self.gridContainer.bounds];
    gmGridView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    gmGridView.backgroundColor = [UIColor clearColor];
    [self.gridContainer addSubview:gmGridView];
    _gmGridView = gmGridView;
    
    _gmGridView.style = GMGridViewStyleSwap;
    _gmGridView.itemSpacing = spacing;
    _gmGridView.centerGrid = YES;
    _gmGridView.actionDelegate = self;
    _gmGridView.dataSource = self;
}


-(void)loadDefaultItems:(int)numberOfItems
{
    _data = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < numberOfItems; i ++)
    {
        [_data addObject:[NSString stringWithFormat:@"item %d", i]];
    }
    _currentData = _data;
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    _gmGridView = nil;
}

#pragma mark - IBLoginViewControllerDelegate

/*
 This function is executed when the user login successfully
 */
- (void)authenticationViewControllerSucceeded:(IBLoginViewController *)authVC
{
    [self dismissViewControllerAnimated:YES completion:NULL];
    
    //[self initGridView];
    
    [self getCompanies];
}

#pragma mark - Notifications

/*
 This function is executed when the user needs to be authenticated
 */
- (void)authenticationRequired:(NSNotification *)notification
{
    if (self.presentedViewController == nil) {
        [self performSegueWithIdentifier:@"AuthenticationSegue" sender:nil];
    }
}

#pragma mark - Properties

-(int)getNextImageIndex
{
    if (_bkImageIndex + 1 > 5)
    {
        _bkImageIndex = 1;
    }
    else
    {
        _bkImageIndex++;
    }
    
    return _bkImageIndex;
}

-(void)setProfileName
{
    IBService *service = [IBService sharedInstance];
    IBProfile * profile = service.currentUser;
    [self.userNameLabel setText: [NSString stringWithFormat:@"%@ %@", profile.firstName,profile.lastName]];
    
    [self getProfileImage];
}


-(UIImage *)imageWithImage:(UIImage*)image
              scaledToSize:(CGSize)newSize;
{
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}


#pragma mark - Segues

-(void)unwindToProfile:(UIStoryboard *)seguew
{
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"AuthenticationSegue"]) {
        
        IBLoginViewController *authVC = (IBLoginViewController *)segue.destinationViewController;
        authVC.delegate = self;
        
    } else if ([segue.identifier isEqualToString:@"MeetingSegue"]) {
        IBMeetingViewController *vc = (IBMeetingViewController *)segue.destinationViewController;
        
        vc.company = _companies[_selectedIndex];
    }
    
}

#pragma mark - View Helpers

-(void)showSpinner {
    _spinnerCount ++;
    [self.waitContainer setHidden:NO];
}

-(void)hideSpinner {
    _spinnerCount--;
    
    if (_spinnerCount < 1)
        [self.waitContainer setHidden:YES];
}

-(void)reloadGrid
{
    _bkImageIndex = 0;
    [_gmGridView reloadData];
}

-(void)clearGrid
{
    //clear grid
    [self loadDefaultItems:0];
    self.companies = [[NSArray alloc] init];
    [_gmGridView reloadData];
}

-(void)signOut
{
    __weak typeof(self) weakSelf = self;
    
    //[[DataStore sharedDataStore] dataSoreClear];
    
    //clear grid
    [self clearGrid];
    
    IBService *service = [IBService sharedInstance];
    [service signoutUserWithSuccess:^{
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        IBLoginViewController *authVC = (IBLoginViewController *)[storyboard instantiateViewControllerWithIdentifier:@"AuthenticationScene"];
        authVC.delegate = weakSelf;
        [weakSelf presentViewController:authVC animated:YES completion:NULL];
        
    } failure:^(NSError * error, NSInteger statusCode){}];
    
}

- (IBAction)buttonLogout:(id)sender {
    
    [self signOut];
    
}

- (IBAction)buttonSupport:(id)sender {
    
    
}

#pragma mark - API Methods

- (void)getCompanies
{
    __weak typeof(self) weakSelf = self;
    
    
    IBService *service = [IBService sharedInstance];
    
    if (service.serverRoot) {
        
        [self showSpinner];
        [self setProfileName];
        [self loadDefaultItems:NUMBER_ITEMS_ON_LOAD];
        
        [service getCompaniesSuccess:^(NSArray *companies) {
        
            //update the database
            [DBContext addCompanies:companies];
            
            //set the companies array
            weakSelf.companies = [DBContext getCompanies];
            
            if (weakSelf.companies.count == 1)
            {
                [weakSelf showMeetingScreen:0];
            }
            
            [weakSelf reloadGrid];
            
            [weakSelf hideSpinner];
            
        } failure:^(NSError *error, NSInteger statusCode) {
            
            //load from database
            
            //set the companies array
            weakSelf.companies = [DBContext getCompanies];
            
            if (weakSelf.companies.count == 1)
            {
                [weakSelf showMeetingScreen:0];
            }
            
            [weakSelf reloadGrid];
            
            [weakSelf hideSpinner];
        }];
    }
    else {
        [self performSegueWithIdentifier:@"AuthenticationSegue" sender:nil];
    }
}

// Check if the "thing" pass'd is empty
- (BOOL) isEmpty:(id)thing
{
    return thing == nil
    || [thing isKindOfClass:[NSNull class]]
    || ([thing respondsToSelector:@selector(length)]
        && [(NSData *)thing length] == 0)
    || ([thing respondsToSelector:@selector(count)]
        && [(NSArray *)thing count] == 0);
}


- (void)getProfileImage
{
    
    UIImage *placeholder = [UIImage imageNamed:@"userprofile"];
    
    __weak typeof(self) weakSelf = self;
    
    IBService *service = [IBService sharedInstance];
    
    [self showSpinner];
    
    [service getImageWithID:[service.currentUser imageIDThumbnail]
                    success:^(UIImage *image) {
                        //NSLog(@"getProfileImage");
                        
                        weakSelf.profileImage.image =  [weakSelf imageWithImage:image scaledToSize:CGSizeMake(238, 271)];
                        
                        [weakSelf hideSpinner];
                        
                    }
                    failure:^(NSError *error, NSInteger statusCode) {
                        NSLog(@"Failed to get image");
                        
                        [weakSelf hideSpinner];
                    }];
    
    self.profileImage.image = placeholder;
}

- (void)getTableCellImage:(NSInteger)cellIndex imageID:(NSString*)imageID
{
    
    __weak typeof(self) weakSelf = self;
    
    IBService *service = [IBService sharedInstance];
    
    [self showSpinner];
    
    [service getImageWithID:imageID
                    success:^(UIImage *image) {
                        GMGridViewCell *cell = [_gmGridView cellForItemAtIndex:cellIndex];
                        
                        UIImageView *background = [[UIImageView alloc] initWithImage:[weakSelf imageWithImage:image scaledToSize:CGSizeMake(218, 218)]];
                        [cell.contentView addSubview:background];
                        
                        cell.contentView.contentMode = UIViewContentModeCenter;
                        
                        [weakSelf hideSpinner];
                    }
                    failure:^(NSError *error, NSInteger statusCode) {
                        NSLog(@"Failed to get image");
                        
                        [weakSelf hideSpinner];
                    }];
    
    
}


//////////////////////////////////////////////////////////////
#pragma mark GMGridViewDataSource
//////////////////////////////////////////////////////////////

- (NSInteger)numberOfItemsInGMGridView:(GMGridView *)gridView
{
    return [_currentData count];
}

- (CGSize)GMGridView:(GMGridView *)gridView sizeForItemsInInterfaceOrientation:(UIInterfaceOrientation)orientation
{
    return CGSizeMake(218,218);
}

- (GMGridViewCell *)GMGridView:(GMGridView *)gridView cellForItemAtIndex:(NSInteger)index
{
    //NSLog(@"Creating view indx %d", index);
    
    CGSize size = [self GMGridView:gridView sizeForItemsInInterfaceOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
    
    GMGridViewCell *cell = [gridView dequeueReusableCell];
    
    if (!cell)
    {
        // create new cell
        
        cell = [[GMGridViewCell alloc] init];
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
        view.backgroundColor = [UIColor clearColor];
        view.layer.masksToBounds = NO;
        view.layer.cornerRadius = 8;
        
        cell.contentView = view;
    }
    
    [[cell.contentView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    
    // set content cell
    
    
    
    if (self.companies.count > index){
        
        Company * company = _companies[index];
        
        NSString * imageName = [NSString stringWithFormat:@"bk_profile_color_%d", [self getNextImageIndex]];
        
        //get Company Image
        
        if ([company.imageIDLogo intValue] > 0) {
            
            // Get logo image
            
            //add background
            int offset = 0;
            int xoffset = 0;//offset * 0.5;
            UIImageView *background = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
            background.frame = CGRectMake(0 + xoffset, 0 + xoffset, size.width-offset, size.height-offset);
            [cell.contentView addSubview:background];
            
            [self getTableCellImage:index imageID:[company.imageIDLogo stringValue]];
            
        } else {
            
            
            //No logo ID found
            
            //add background
            int offset = 0;
            int xoffset = 0;//offset * 0.5;
            UIImageView *background = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
            background.frame = CGRectMake(0 + xoffset, 0 + xoffset, size.width-offset, size.height-offset);
            [cell.contentView addSubview:background];
            
            //[cell.contentView sendSubviewToBack:background];
            cell.contentView.contentMode = UIViewContentModeCenter;
            
            //add label
            UILabel *label = [[UILabel alloc] initWithFrame:cell.contentView.bounds];
            label.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            label.text = company.companyName;
            label.textAlignment = NSTextAlignmentCenter;
            label.backgroundColor = [UIColor clearColor];
            label.textColor = [UIColor whiteColor];
            label.highlightedTextColor = [UIColor whiteColor];
            label.font = [UIFont boldSystemFontOfSize:20];
            [cell.contentView addSubview:label];
        }
        
        
    } else {
        
        //add background
        UIImageView *background = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"blockimage.png"]];
        background.frame = CGRectMake(0, 0, size.width, size.height);
        [cell.contentView addSubview:background];
        [cell.contentView sendSubviewToBack:background];
        cell.contentView.contentMode = UIViewContentModeScaleAspectFit;
        
    }
    
    return cell;
}



//////////////////////////////////////////////////////////////
#pragma mark GMGridViewActionDelegate
//////////////////////////////////////////////////////////////

-(void)showMeetingScreen:(int)position
{
    _selectedIndex = (int)position;
    
    //if (self.presentedViewController == nil) {
    [self performSegueWithIdentifier:@"MeetingSegue" sender:nil];
    //}
}

- (void)GMGridView:(GMGridView *)gridView didTapOnItemAtIndex:(NSInteger)position
{
    NSLog(@"Did tap at index %d", (int)position);
    
    if (self.companies.count > (int)position){
        _selectedIndex = (int)position;
        
        if (self.presentedViewController == nil) {
            [self performSegueWithIdentifier:@"MeetingSegue" sender:nil];
        }
    }
}

- (void)GMGridViewDidTapOnEmptySpace:(GMGridView *)gridView
{
    NSLog(@"Tap on empty space");
}


@end
