//
//  IBAnnotationComment.h
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/04/28.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "IBSerializable.h"


@interface IBAnnotationComment : NSObject <IBSerializable>

@property (nonatomic, copy, readonly) NSString *pointX;
@property (nonatomic, copy, readonly) NSString *pointY;
@property (nonatomic, copy, readonly) NSString *text;

/**
 * Initialize a new instance
 */
- (instancetype)initWithPointX:(NSString *)pointX
                        pointY:(NSString *)pointY
                          text:(NSString *)text;


/**
 * Create a new instance from a dictionary
 */
- (instancetype)initWithDictionary:(NSDictionary *)dict;

/**
 * Returns a dictionary representation of the model suitable
 * for JSON serialization
 */
- (NSDictionary *)dictionaryRepresentation;


@end