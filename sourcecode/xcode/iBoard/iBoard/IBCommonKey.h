//
//  IBCommonKey.h
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/04/28.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>


extern NSString * const IBProfileFirstNameKey;
extern NSString * const IBProfileLastNameKey;
extern NSString * const IBProfileServerExpiryDateKey;
extern NSString * const IBProfileUserIDKey;
extern NSString * const IBProfileAPIKeyKey;
extern NSString * const IBImageIDThumbnailKey;

extern NSString * const IBCompanyIDKey;
extern NSString * const IBCompanyNameKey;
extern NSString * const IBRequireOnlineSignInEveryXDaysKey;
extern NSString * const IBReLoginInactivityMinutesKey;
extern NSString * const IBCompanyUserLevelIDKey;
extern NSString * const IBImageIDLogoKey;


extern NSString * const IBIsDeletedKey;
extern NSString * const IBAllowEmailKey;
extern NSString * const IBAllowPrintKey;

extern NSString * const IBDocumentPackDocumentIDKey;
extern NSString * const IBDocumentIDKey;
extern NSString * const IBDocumentOrderKey;
extern NSString * const IBDocumentNameKey;
extern NSString * const IBCanEmailKey;
extern NSString * const IBCanPrintKey;
extern NSString * const IBFileNameKey;

extern NSString * const IBMeetingAgendaItemIDKey;
extern NSString * const IBItemNumberKey;
extern NSString * const IBItemNameKey;
extern NSString * const IBItemOrderKey;
extern NSString * const IBDocumentListKey;

extern NSString * const IBCompanyMeetingIDKey;
extern NSString * const IBStartDateTimeKey;
extern NSString * const IBEndDateTimeKey;
extern NSString * const IBLocationKey;
extern NSString * const IBApologiesKey;
extern NSString * const IBShowAgendaItemNumberIndKey;
extern NSString * const IBPackTypeKey;
extern NSString * const IBMeetingAgendaItemListKey;
extern NSString * const IBDocumentPackIDKey;
extern NSString * const IBPackNameKey;
extern NSString * const IBPackDescriptionKey;
extern NSString * const IBCoverNoteKey;
extern NSString * const IBCoverStyleIDKey;


extern NSString * const IBPackTypeKey;
extern NSString * const IBDocumentListKey;
extern NSString * const IBDocumentPackIDKey;
extern NSString * const IBPackNameKey;
extern NSString * const IBPackDescriptionKey;
extern NSString * const IBCoverNoteKey;
extern NSString * const IBCoverStyleIDKey;

extern NSString * const IBDocumentPackListKey;
extern NSString * const IBMeetingListKey;
extern NSString * const IBServerTimestampKey;

extern NSString * const IBPointXKey;
extern NSString * const IBPointYKey;
extern NSString * const IBTextKey;


extern NSString * const IBDocumentAnnotationIDKey;
extern NSString * const IBPageNoKey;
extern NSString * const IBJSONDataKey;


extern NSString * const IBGraphicsKey;
extern NSString * const IBCommentsKey;

@interface IBCommonKey : NSObject

@end
