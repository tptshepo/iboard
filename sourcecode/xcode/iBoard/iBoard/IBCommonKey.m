//
//  IBCommonKey.m
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/04/28.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import "IBCommonKey.h"


NSString * const IBProfileFirstNameKey = @"FirstName";
NSString * const IBProfileLastNameKey = @"LastName";
NSString * const IBProfileServerExpiryDateKey = @"ServerExpiryDate";
NSString * const IBProfileUserIDKey = @"UserID";
NSString * const IBProfileAPIKeyKey = @"AuthToken";
NSString * const IBImageIDThumbnailKey = @"ImageIDThumbnail";

NSString * const IBCompanyIDKey = @"CompanyID";
NSString * const IBCompanyNameKey = @"CompanyName";
NSString * const IBRequireOnlineSignInEveryXDaysKey = @"RequireOnlineSignInEveryXDays";
NSString * const IBReLoginInactivityMinutesKey = @"ReLoginInactivityMinutes";
NSString * const IBCompanyUserLevelIDKey = @"CompanyUserLevelID";
NSString * const IBImageIDLogoKey = @"ImageIDLogo";

NSString * const IBIsDeletedKey = @"IsDeleted";
NSString * const IBAllowEmailKey = @"AllowEmail";
NSString * const IBAllowPrintKey = @"AllowPrint";

NSString * const IBDocumentPackDocumentIDKey = @"DocumentPackDocumentID";
NSString * const IBDocumentIDKey = @"DocumentID";
NSString * const IBDocumentOrderKey = @"DocumentOrder";
NSString * const IBDocumentNameKey = @"DocumentName";
NSString * const IBCanEmailKey = @"CanEmail";
NSString * const IBCanPrintKey = @"CanPrint";
NSString * const IBFileNameKey = @"FileName";


NSString * const IBMeetingAgendaItemIDKey = @"MeetingAgendaItemID";
NSString * const IBItemNumberKey = @"ItemNumber";
NSString * const IBItemNameKey = @"ItemName";
NSString * const IBItemOrderKey = @"ItemOrder";
NSString * const IBDocumentListKey = @"DocumentList";


NSString * const IBCompanyMeetingIDKey = @"CompanyMeetingID";
NSString * const IBStartDateTimeKey = @"StartDateTime";
NSString * const IBEndDateTimeKey = @"EndDateTime";
NSString * const IBLocationKey = @"Location";
NSString * const IBApologiesKey = @"Apologies";
NSString * const IBShowAgendaItemNumberIndKey = @"ShowAgendaItemNumberInd";
NSString * const IBPackTypeKey = @"PackType";
NSString * const IBMeetingAgendaItemListKey = @"MeetingAgendaItemList";
NSString * const IBDocumentPackIDKey = @"DocumentPackID";
NSString * const IBPackNameKey = @"PackName";
NSString * const IBPackDescriptionKey = @"PackDescription";
NSString * const IBCoverNoteKey = @"CoverNote";
NSString * const IBCoverStyleIDKey = @"CoverStyleID";


NSString * const IBDocumentPackListKey = @"DocumentPackList";
NSString * const IBMeetingListKey = @"MeetingList";
NSString * const IBServerTimestampKey = @"ServerTimestamp";

NSString * const IBPointXKey = @"PointX";
NSString * const IBPointYKey = @"PointY";
NSString * const IBTextKey = @"Text";

NSString * const IBDocumentAnnotationIDKey = @"DocumentAnnotationID";
NSString * const IBPageNoKey = @"PageNo";
NSString * const IBJSONDataKey = @"JSONData";

NSString * const IBGraphicsKey = @"Graphics";
NSString * const IBCommentsKey = @"Comments";

@implementation IBCommonKey

@end
