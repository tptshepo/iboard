//
//  IBDocumentViewController.m
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/05/17.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import "IBDocumentViewController.h"
#import "IBFileManager.h"
#import "DocumentPackDocument.h"
#import "IBService.h"
#import "IBWaitView.h"
#import "IBDocumentViewerView.h"



@interface IBDocumentViewController ()
{
    int _spinnerCount;
    IBDocumentViewerView * _documentView;
}


@property (nonatomic, strong) IBWaitView *waitView;

@end

@implementation IBDocumentViewController
{
    ReaderDocument *document;
    
}

#pragma mark - Constants


#pragma mark - Properties

@synthesize documentPackDocument = _documentPackDocument;


-(void)documentSelected:(DocumentPackDocument *)documentPackDocument
{
    [self showSpinner];
    
    __weak typeof(self) weakSelf = self;
    
    IBService *service = [IBService sharedInstance];
    
    [service getDocumentWithID:[documentPackDocument.documentID stringValue]
                       success:^(NSData *data) {
                           NSLog(@"document downloaded");
                           
                           [weakSelf loadDocumentInContainer];
                           [weakSelf hideSpinner];
                       }
                       failure:^(NSError *error, NSInteger statusCode) {
                           NSLog(@"Failed to get image");
                           
                           [weakSelf hideSpinner];
                           
                       }];
}


- (IBAction)backToDocumentList:(id)sender {
    
    if (_documentView != nil)
        [_documentView cleanUp];
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Helpers

-(void)showSpinner {
    _spinnerCount ++;
    [self.waitContainer setHidden:NO];
}

-(void)hideSpinner {
    _spinnerCount--;
    
    if (_spinnerCount < 1)
        [self.waitContainer setHidden:YES];
}

#pragma mark - Reader View UI Methods


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //NSLog(@"viewDidLoad");
    
    self.waitView = [[IBWaitView alloc] initWithFrame:CGRectZero];
    [self.waitView setBounds:[self.waitContainer bounds]];
    [self.waitContainer setBackgroundColor:[UIColor clearColor]];
    [self.waitContainer addSubview:self.waitView];
    [self.waitView showWithText:@""];
    [self hideSpinner];
    
    
    [self.titleView setText:_documentPackDocument.documentName];
    [self documentSelected:_documentPackDocument];
}


- (void)loadDocumentInContainer
{
    self.documentContainer.bounds = CGRectMake(0, 0, 768, 871);
    
    IBDocumentViewerView * documentView = [[IBDocumentViewerView alloc] initWithFrame:self.documentContainer.bounds];
    _documentView = documentView;
    [self.documentContainer addSubview:documentView];
    [_documentView loadDocument:[_documentPackDocument.documentID stringValue] canCallServer:YES];
}






@end
