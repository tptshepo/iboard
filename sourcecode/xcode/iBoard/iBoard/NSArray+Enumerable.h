//
//  NSArray+Enumerable.h
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/04/11.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (Enumerable)

- (NSArray *)mappedArrayWithBlock:(id(^)(id obj))block;

@end
