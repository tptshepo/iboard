//
//  IBAnnotationJSONData.m
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/04/28.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import "IBAnnotationJSONData.h"
#import "IBCommonKey.h"
#import "NSArray+Enumerable.h"

NSString * const IBGraphicsKey = @"Graphics";
NSString * const IBCommentsKey = @"Comments";

@interface IBAnnotationJSONData ()

@property (nonatomic, copy, readwrite) NSString *graphics;
@property (nonatomic, copy, readwrite) NSString *comments;

@end


@implementation IBAnnotationJSONData


-(instancetype)initWith
graphics:(NSString *)graphics
comments:(NSString *)comments
{
    
    
    if ((self = [super init])) {

    self.graphics = graphics;
    self.comments = comments;

    }
    
    return self;
    
    
}


- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    return [self initWith
Graphics:dict[IBGraphicsKey]
Comments:dict[IBCommentsKey]
];

}

- (NSDictionary *)dictionaryRepresentation
{
    return @{
 IBGraphicsKey: self.Graphics,
 IBCommentsKey: self.Comments,
             };
}

- (NSString *)description
{

    return [NSString stringWithFormat:@"<%@: 0x%x \
        Graphics=%@ \
        Comments=%@ \
            ",
            NSStringFromClass([self class]),
            (unsigned int)self,
                self.graphics,
                self.comments,
            ];
}


@end
