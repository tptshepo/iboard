//
//  IBService_NSURLConnection.m
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/04/11.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import "IBService_NSURLConnection.h"


#import "IBService_SubclassMethods.h"
#import "IBProfile.h"
#import "IBService_NSURLConnectionRequest.h"
#import "NSArray+Enumerable.h"

@interface IBService_NSURLConnection () <IBService_NSURLConnectionRequestDelegate>

@property (nonatomic, strong) NSMutableArray *requestsPendingAuthentication;

@end

@implementation IBService_NSURLConnection

- (id)init
{
    if ((self = [super init]))
    {
        self.requestsPendingAuthentication = [NSMutableArray array];
    }
    
    return self;
}

#pragma mark - Subclass methods

- (NSString *)submitRequestWithURL:(NSURL *)URL
                            method:(NSString *)httpMethod
                              body:(NSDictionary *)bodyDict
                    expectedStatus:(NSInteger)expectedStatus
                           success:(IBServiceSuccess)success
                           failure:(IBServiceFailure)failure
{
    NSMutableURLRequest *request = [self requestForURL:URL method:httpMethod bodyDict:bodyDict];
    
    IBService_NSURLConnectionRequest *connectionRequest;
    connectionRequest = [[IBService_NSURLConnectionRequest alloc] initWithRequest:request
                                                                        expectedStatusCode:expectedStatus
                                                                                   success:success
                                                                                   failure:failure
                                                                                  delegate:self];
    
    NSString *connectionID = [connectionRequest uniqueIdentifier];
    [self.requests setObject:connectionRequest forKey:connectionID];
    return connectionID;
}

- (void)resendRequestsPendingAuthentication
{
    for (IBService_NSURLConnectionRequest *request in self.requestsPendingAuthentication) {
        [request restart];
    }
    
    [self.requestsPendingAuthentication removeAllObjects];
}

- (void)cancelRequestWithIdentifier:(NSString *)identifier
{
    IBService_NSURLConnectionRequest *request = [self.requests objectForKey:identifier];
    if (request) {
        [request cancel];
        [self.requests removeObjectForKey:identifier];
    }
}

#pragma mark - IBService_NSURLConnectionRequestDelegate

- (void)requestDidComplete:(IBService_NSURLConnectionRequest *)request
{
    [self.requests removeObjectForKey:[request uniqueIdentifier]];
    [self.requestsPendingAuthentication removeObject:request];
}

- (void)requestRequiresAuthentication:(IBService_NSURLConnectionRequest *)request
{
    [self.requestsPendingAuthentication addObject:request];
    [[NSNotificationCenter defaultCenter] postNotificationName:IBServiceAuthRequiredNotification object:nil];
}

@end
