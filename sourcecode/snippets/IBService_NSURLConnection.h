//
//  IBService_NSURLConnection.h
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/04/11.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "IBService.h"

/**
 * An implementation of CCVChatcaveService using NSURLConnection
 */
@interface IBService_NSURLConnection : IBService

@end
