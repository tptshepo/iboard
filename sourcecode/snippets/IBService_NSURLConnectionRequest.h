//
//  IBService_NSURLConnectionRequest.h
//  iBoard
//
//  Created by Tshepo Mgaga on 2015/04/11.
//  Copyright (c) 2015 Qualip Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "IBService.h"

@protocol IBService_NSURLConnectionRequestDelegate;

/**
 * A class that wraps NSURLConnection for easier use and state-tracking
 */
@interface IBService_NSURLConnectionRequest : NSObject
<NSURLConnectionDelegate, NSURLConnectionDataDelegate>


/**
 * Initialize a new instance
 * @param request A NSURLRequest for the underlying connection to execute
 * @param statusCode The expected HTTP status code signaling successful execution
 * @param success The callback block to execute upon successful completion
 * @param failure The failure block to execute if the connection fails for any reason
 */
- (instancetype)initWithRequest:(NSURLRequest *)request
             expectedStatusCode:(NSInteger)statusCode
                        success:(IBServiceSuccess)success
                        failure:(IBServiceFailure)failure
                       delegate:(id<IBService_NSURLConnectionRequestDelegate>)delegate;

/**
 * Cancel the underlying connection
 */
- (void)cancel;

/**
 * Restarts the request
 */
- (void)restart;

/**
 * The unique identifier for the request, used to track instances separately
 */
- (NSString *)uniqueIdentifier;

@end

@protocol IBService_NSURLConnectionRequestDelegate <NSObject>

- (void)requestDidComplete:(IBService_NSURLConnectionRequest *)request;

- (void)requestRequiresAuthentication:(IBService_NSURLConnectionRequest *)request;

@end


